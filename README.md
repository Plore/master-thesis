## Master's thesis "Correlations in the central spin model with homogeneous couplings"

final PDF: [thesis](https://bitbucket.org/plore/master-thesis/raw/master/thesis/build/thesis.pdf)

###Required programs (tested versions):
 - GNU make 4.0
 - TeXLive 2014
 - Python 3.4.3
 - g++ 4.9.2

###Required Python 3 libraries:
 - matplotlib 1.4.2
 - numpy 1.8.2
 - scipy 0.14.1

###Required C++ libraries:
 - Eigen 3.2.2
