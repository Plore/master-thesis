\input{header_physics.tex}

\begin{document}

\input{front.tex}

\tableofcontents
\newpage

\section{The central spin model with uniform couplings}
The central spin model describes a system of bath spins coupled to a distinguished central spin via Heisenberg interactions.
The central spin may also be subject to a magnetic field of strength $h$ applied in the $z$-direction:
\begin{eqn}
  H = \v{S}_0 \sum_{i=1}^N A_i \v{S}_i + h S_0^z
\end{eqn}
This model is of interest for example in the description of quantum dots in semiconductors on a timescale where the dynamics due to the interaction of bath spins can be neglected.

Though not particularly realistic, it can be illuminating to study the model with uniform couplings $A_i \equiv 2$ as well as a vanishing external magnetic field $h = 0$.
In this case, the Hamiltonian can be rewritten as
\begin{eqn}
  H = \v{S}^2 - \v{S}_b^2 - \v{S}_0^2
  \label{eq:hamiltonian}
\end{eqn}
with $\v{S}_b = \sum_{n=1}^N \v{S}_i$, $\v{S} = \v{S}_0 + \v{S}_b$.
The spectrum is given by
\begin{eqn}
  E = \begin{cases}
    S_b\:,       & S = S_b + \frac{1}{2} \\
    -S_b - 1\:,  & S = S_b - \frac{1}{2}
    \label{eq:spectrum}
  \end{cases}
\end{eqn}
with eigenstates $\Ket{S = S_b \pm \frac{1}{2}, S_b}$.

The main objective is to calculate the expectation value of the central spin polarization $\avg{S_0^z(t)} = \Tr\left[\rho_0(t) S_0^z\right]$ as well as the von Neumann entropy $E = \Tr\left[\rho_0(t) \ln \rho_0(t)\right]$ where $\rho_0(t) = \Tr_\t{Bath}\left[\rho(t)\right]$ denotes the time-dependent reduced density matrix obtained from the general density matrix by performing the trace over all bath states.

\section{Exact solutions for spin $\frac{1}{2}$}
The central spin and the bath are chosen to be initially unentangled, both being in a state of definite polarization.
For the bath, two cases can be distinguished:
\begin{itemize}
  \item[a)] The bath is in an eigenstate of $\v{S}_b^2$ and $S_b^z$. Except for limiting cases of maximal positive or negative polarization, there is entanglement between the individial bath spins.
  \item[b)] The bath is in a product state $\Ket{\uparrow \downarrow \downarrow \uparrow \downarrow \ldots}$ with definite $S_b^z$ but of indeterminate $S_b$.
\end{itemize}
To determine the time evolution of both kinds of initial states, their decompositions into states of total angular momentum $\Ket{S, S^z, S_b}$ are needed.
In the case of adding one spin-$\frac{1}{2}$ particle to a bigger spin $\Ket{S_b, S_b^z}$ this can be done via the explicit formulas \cite{bortz_stolze_2007} \cite{schwabl}
\begin{eqns*}
  \Ket{\Downarrow}\Ket{S_b,S_b^z} &=& \frac{1}{\sqrt{2 S_b + 1}} \left(\sqrt{S_b - S_b^z + 1} \Ket{S_b + \frac{1}{2}, S_b^z - \frac{1}{2}, S_b}\right. \\
  && - \left.\sqrt{S_b + S_b^z} \Ket{S_b - \frac{1}{2}, S_b^z - \frac{1}{2}, S_b}\right) \IEEEyesnumber \IEEEyessubnumber \label{eq:clebschgordan_spin_one_half_down}\\
  \Ket{\Uparrow}\Ket{S_b,S_b^z-1} &=& \frac{1}{\sqrt{2 S_b + 1}} \left(\sqrt{S_b + S_b^z} \Ket{S_b + \frac{1}{2}, S_b^z - \frac{1}{2}, S_b}\right. \\
  && + \left.\sqrt{S_b - S_b^z + 1} \Ket{S_b - \frac{1}{2}, S_b^z - \frac{1}{2}, S_b}\right)\:. \IEEEyessubnumber
\end{eqns*}
Inverting these equations yields
\begin{eqns*}
  \Ket{S_b + \frac{1}{2}, S_b^z - \frac{1}{2}, S_b} &=& \frac{1}{\sqrt{2 S_b + 1}} \left(\sqrt{S_b + S_b^z} \Ket{\Uparrow}\Ket{S_b, S_b^z - 1}\right. \\
  && + \left.\sqrt{S_b - S_b^z + 1} \Ket{\Downarrow}\Ket{S_b, S_b^z}\right) \IEEEyesnumber \IEEEyessubnumber \\
  \Ket{S_b - \frac{1}{2}, S_b^z - \frac{1}{2}, S_b} &=& \frac{1}{\sqrt{2 S_b + 1}} \left(\sqrt{S_b - S_b^z + 1} \Ket{\Uparrow}\Ket{S_b, S_b^z - 1}\right. \\
  && - \left.\sqrt{S_b + S_b^z} \Ket{\Downarrow}\Ket{S_b, S_b^z}\right) \:. \IEEEyessubnumber
\end{eqns*}
After expressing the initial state of the system in terms of total angular momentum states and performing time development, the trace over bath states can be performed.
If the bath is in an eigenstate of $\v{S}_b^2$ and $S_b^z$ and the central spin is in the down state $\Ket{\Downarrow}$ initially, this yields \cite{bortz_stolze_2007}
\begin{eqns}
  \rho_0(t) &=& \alpha(t) \Ket{\Uparrow}\Bra{\Uparrow} + (1 - \alpha(t)) \Ket{\Downarrow}\Bra{\Downarrow}\:, \\
  \alpha(t) &=& \frac{2 (S_b - S_b^z + 1)(S_b + S_b^z)}{(2 S_b + 1)^2} \left(1 - \cos\left( (2 S_b + 1) t\right) \right)\:.
  \label{eq:dynamics_simple}
\end{eqns}
In the case of an inital bath product state
\begin{eqn}
  \Ket{\t{Bath}} = |\underbrace{\downarrow\ldots\downarrow}_{M-1},\underbrace{\uparrow\ldots\uparrow}_{N-M}\rangle
\end{eqn}
one has to start from the $N-M$-particle state $\Ket{\uparrow \ldots \uparrow}$ and add a total of $M$ particles in the down state $\Ket{\downarrow}$ iteratively according to rule~\ref{eq:clebschgordan_spin_one_half_down}, the last of which is chosen to represent the central spin.
This results in \cite{bortz_stolze_2007}
\begin{eqns*}
  \Ket{\Downarrow} |\underbrace{\downarrow \ldots \downarrow}_{M-1},\underbrace{\uparrow \ldots \uparrow}_{N-M}\rangle &=& \sum_{k=0}^{M-1} \frac{1}{\sqrt{N - 2k}} \sum_{j=1}^{\binom{M-1}{k}} c_{j,k}^{N,M}\\
  && \left(\sqrt{M - k}\Ket{\frac{N}{2} - k, \frac{N - 2M}{2}, \frac{N - 1}{2} - k} \right. \\
  && \left. - \sqrt{N - M - k}\Ket{\frac{N}{2} - k - 1, \frac{N - 2M}{2}, \frac{N - 1}{2} - k} \right) \IEEEyesnumber
  \label{eq:monster}
\end{eqns*}
Time evolution and tracing off the bath states yields \cite{bortz_stolze_2007}
\begin{eqns}
  \rho_0(t) &=& \beta(t) \Ket{\Uparrow}\Bra{\Uparrow} + (1 - \beta(t)) \Ket{\Downarrow}\Bra{\Downarrow} \:, \\
  \beta(t) &=& 2 \sum_{k=0}^{M-1} \frac{d_{k,N,M}}{(N - 2 k)^2} (M - k) (N - M - k) \left(1 - \cos( (N - 2 k) t)\right)\:,
  \label{eq:bs_product_dynamics}
\end{eqns}
where $d_{k,N,M} := \sum_{j=1}^{\binom{M-1}{k}} \left(c_{j,k}^{N,M}\right)^2$ can be used for convenience since all $2^M$ states in \eqref{eq:monster} are orthogonal.
This quantity is subject to recursion relation in $N$ and $M$ which is solved by
\begin{eqn}
  d_{k,N,M} = \frac{(M-1)!(N-M)!(N-2k)}{(N-k)!k!}\:.
\end{eqn}
With this, the central spin polarization and the von Neumann entropy can be determined.

\section{Extension to arbitrary bath spin}
In passing from bath particles of spin-$\frac{1}{2}$ to particles of arbitrary spin $I$, the general strategy for calculating the time-dependent reduced density matrix as outlined above can be maintained.
In fact, as long as the initial bath state is an eigenstate of $\v{S}_b^2$, nothing needs to be done since the Hamiltonian \eqref{eq:hamiltonian} doesn't distinguish between internal compositions of the bath.
In the case of a initial product state, however, a few generalizations need to be made.

First of all, since the iterative scheme mentioned above now relies on addition of arbitrary angular momentum $I$ to the angular momentum of the bath in each step, all Clebsch-Gordan coefficients $\Braket{J, m_J, I, L | I, m_I, L, m_L = m_J - m_I}$ have to be determined.
Since for a single bath $I$ is fixed among all spins, these will be abbreviated as $\alpha_{J, m_J, L}^{m_I}$.
With these coefficients, bath product states can be constructed iteratively again, starting from a state with $N-M$ spins of maximal polarization.
In contrast to the case $I=\frac{1}{2}$, now there are more possibilities of individual spin polarization to choose from.
For example, a bath state with $M$ spins of polarization $m_I = -I$ and $N-M$ spins with $m_I = I$ may be written as
\begin{eqns}
  |\underbrace{-I, \ldots, -I}_{M},\underbrace{I, \ldots, I}_{N-M}\rangle &=& \sum_{k=0}^{2 I (M-1)} \sum_{j=1}^{G_{k}^{M-1}} c_{j,k}^{N,M} \sum_{i=0}^{2I} \alpha_{NI-k-i, (N-2M)I, (N-1)I-k}^{-I} \IEEEnonumber\\[5pt]
  && \Ket{NI-k-i, (N-2M)I, (N-1)I-k}\:.
  \label{eq:general_decomposition}
\end{eqns}
in analogy to \eqref{eq:monster}, where $G_k^M$ denote the multinomial coefficients which are appropriate for spin $I$ (for example, binomial coefficients for $I=\frac{1}{2}$, trinomial coefficients \cite{andrews_1990} for $I=1$ and so on).
Again the resulting $(2I+1)^N$ states are mutually orthogonal and it is convenient to use the quantities
\begin{eqn}
  d_{k,N,M} := \sum_{j=1}^{G_{k}^{M-1}}\left(c_{j,k}^{N,M}\right)^2
\end{eqn}
which are related by a recursion in $N$ and $M$:
\begin{eqns}
  d_{k,N,M} &=& \sum_{i=0}^{\min(k, 2I)} d_{k,N-1,M-1} \left(\alpha_{NI-k, (N-2M+1)I, (N-1)I-k+i}^{-I}\right)^2
\end{eqns}
While there is no simple explicit solution to this recursion since it depends on various Clebsch-Gordan coefficients, the $d_{k,N,M}$ can be calculated by a computer in a numerically stable way.

The central spin-$\frac{1}{2}$ particle can be appended according to \eqref{eq:clebschgordan_spin_one_half_down}, for example, to receive the complete initial state of the system:
\begin{eqns*}
  \Ket{\Downarrow}\Ket{\t{Bath}} &=& \sum_{k=0}^{2MI} \sum_{j=1}^{G_{k}^{M}} c_{j,k}^{N,M} \frac{1}{\sqrt{2(NI-k) + 1}} \\
  && \left(\sqrt{2MI - k + 1} \Ket{NI - k + \frac{1}{2}, (N-2M)I - \frac{1}{2}, NI - k}\right. \\
  &&\left. - \sqrt{2NI - 2MI - k}\Ket{NI - k -\frac{1}{2}, (N-2M)I-\frac{1}{2}, NI-k}\right) \IEEEyesnumber
\end{eqns*}
The time-dependent reduced density matrix then reads
\begin{eqn}
  \rho_0(t) = \alpha(t) \Ket{\Uparrow}\Bra{\Uparrow} + (1-\alpha(t)) \Ket{\Downarrow}\Bra{\Downarrow}
\end{eqn}
with
\begin{eqn}
  \alpha(t) = 2 \sum_{k=0}^{2IM} \frac{d_{k,N,M}(2MI - k + 1)}{(2(NI - k) + 1)^2} (2I(N - M) - k) \left(1-\cos\left( (2 (NI - k) + 1) t \right)\right). \IEEEeqnarraynumspace
\end{eqn}
The result is analogous to that for a bath product state of spin-$\frac{1}{2}$ particles \eqref{eq:bs_product_dynamics}.
However, instead of $M$ frequencies there now are $2MI$ frequencies involved.

If a different configuration of individual spin polarizations $\{m_I\}$ is chosen, the bath state can in principle be built in an analogous fashion using corresponding Clebsch-Gordan coefficients.
However, while for $I=\frac{1}{2}$ it is always possible to express the bath product state in the form~\eqref{eq:monster} (interchanging the role of up- and down-spins if $M > N-M$ \cite{bortz_stolze_2007}), equation~\eqref{eq:general_decomposition} is not the most general expression for $I > \frac{1}{2}$.
For example, if there are more than two types of polarization $m_I$ involved, keeping track of the coefficients becomes more cumbersome and consequently quantities like the $d_{k,N,M}$ are harder to determine.
Thus, while by superposition it is possible to examine the dynamics of more general initial product states of the form
\begin{eqn}
  \bigotimes_{i=1}^N \left(\sum_{m_I = -I}^I a_{i,m_I} \Ket{m_I}\right)\:, \qquad \sum_{m_I} a_{i,m_I}^2 \equiv 1
%  \left(\alpha_1\Ket{\Uparrow} + \beta_1\Ket{\Downarrow}\right)\left(\alpha_2\Ket{\Uparrow} + \beta_2\Ket{\Downarrow}\right)\ldots\left(\alpha_N\Ket{\Uparrow} + \beta_N\Ket{\Downarrow}\right)\:, \quad \alpha_i^2 + \beta_i^2 \equiv 1
\end{eqn}
as well as the dynamics of mixed states, this can usually get quite tedious.

In the special case of a maximally mixed state, which is a good approximation of the system at temperatures of a few \si{\kelvin} \cite{hackmann_anders_2014}, dynamics are determined more easily.
The central spin is chosen to be initially unentangled with the bath and aligned along the $z$-axis in positive direction, so the initial density matrix reads
\begin{eqn}
  \rho = \begin{pmatrix}
    1 & 0 \\
    0 & 0
  \end{pmatrix}
  \otimes \frac{1}{(2I+1)^N} \mathbb{1}\:.
\end{eqn}
Since each product bath state occurs with equal probability, the time-dependent reduced density matrix is characterized by a weighted average of expressions of the type \eqref{eq:dynamics_simple}.
The weights are determined by counting the degeneracy $D_{S_b, S_b^z}$ of a bath eigenstate $\Ket{S_b, S_b^z}$ with respect to the product basis.
There are $G_{NI-S_b^z}^N$ different product states with the same $S_b^z$-value; therefore
\begin{eqn}
  D_{S_b, S_b^z} = G_{NI-S_b}^N - G_{NI-(S_b+1)}^N
\end{eqn}
where $G_M^N$ are again the multinomial coefficients.
This is illustrated in figure~\ref{fig:degeneracy_illustration}; see also \cite{arecchi_1972}.

Results for $I=\frac{3}{2}$ are in accordance with the calculations of Daniel Meissner \cite{meissner_2014}, as can be seen in figure~\ref{fig:comparison_meissner}.
There an approximate method using Chebyshev polynomials was used to determine central spin dynamics in an initially maximally mixed bath of spin-$\frac{3}{2}$ particles.
Fluctuations in the central spin polarization are suppressed upon increasing the number of bath spins.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{../Code/plot/degeneracy_illustration}
  \caption{Illustration of the scheme for counting the degeneracy $D_{S_b, S_b^z}$. In this example, $I=\frac{1}{2}$, $N = 5$. Each line segment corresponds to a distinct configuration of individual spin polarizations. The outlined state $\Ket{\frac{3}{2}, \frac{1}{2}}$ is associated with the weight $\displaystyle\binom{5}{5/2 - 3/2} - \binom{5}{5/2 - 5/2} = 4$.}
  \label{fig:degeneracy_illustration}
\end{figure}
\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{../Code/build/comparison_meissner.pdf}
  \caption{Comparison with the results of Daniel Meißner \cite{meissner_2014}. Development of the central spin polarization in a maximally mixed bath of $N=3$, $5$ and $7$ spins, respectively.}
  \label{fig:comparison_meissner}
\end{figure}

\section{Investigating temperature dependence}
While arbitrary pure states of definite polarization as well as the maximally mixed state can be investigated using the methods outlined above, it would also be interesting to have a look at the intermediate regime of sufficiently small but nonzero temperatures.
Since the energy of the entire system is known to be dependent only on $S_b$ and the orientation of the central spin with respect to $\v{S}_b$ \eqref{eq:spectrum}, again the strategy of superposing terms of the kind \eqref{eq:dynamics_simple} can be used.
Apart from the combinatorial weight $D_{S_b, S_b^z}$ now the sum of two Boltzmann weights $\exp\left(-\frac{S_b}{k_\t{B} T}\right) + \exp\left(\frac{S_b+1}{k_\t{B}}\right)$ is associated with the dynamics of a bath state $\Ket{S_b, S_b^z}$.

As a result, the dynamics of $\avg{S_0^z(t)}$ now show oscillations that are increasingly dampened with higher temperature, as is demonstrated in figure~\ref{fig:dampening_example} for a system of $N=21$ bath particles of spin $I = \frac{3}{2}$ (here and in the following, $k_\t{B} = 1$).
The middle part of the envelope can be approximated to reasonable accuracy with a function of the form $f(x) = a \cosh(b x + c) + d$.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{../Code/build/dampening_example}
  \caption{Example for dampened oscillations in a bath of $N=21$ particles of spin $\frac{3}{2}$. The central part of the envelope can be approximated by a hyperbolic cosine function.}
  \label{fig:dampening_example}
\end{figure}

\newpage
In examining the minimal height $h$ of the envelope as a function of the temperature, one can roughly distinguish three regimes.
For sufficiently small temperatures ($T \lesssim 0.3$), there is an Arrhenius-like relation $h(T) = 0.5 - \exp\left(-\frac{E}{T}\right)$ between $T$ and $h$, as can be seen in figure~\ref{fig:small_T_arrhenius}.
The characteristic energy $E = 1$ is found to be independent of the system size.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{../Code/build/small_T_arrhenius}
  \caption{Arrhenius plot for the temperature depence of the minimal height $h$ of the envelope of $\avg{S_0^z(t)}$. Linear regression reveals $E=1$ independently of the system size.}
  \label{fig:small_T_arrhenius}
\end{figure}

For intermediate temperatures ($0.3 \lesssim T \lesssim 1$ for the system sizes investigated here) $h$ falls off exponentially with $h(T) = A \exp(-kT)$, as is evident from figure~\ref{fig:mid_T_exponential}.
The exponent is found to be proportional to $N$ with $k(N) = N - 1.2$ (figure~\ref{fig:mid_T_slopes}).

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{../Code/build/mid_T_exponential}
  \caption{Exponential regime of the temperature dependence of the envelope minimum $h$. The strength of the exponential decay is proportional to the system size $N$.}
  \label{fig:mid_T_exponential}
\end{figure}
\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{../Code/build/mid_T_slopes}
  \caption{Dependence of the strength $k$ of exponential decay on the system size $N$.}
  \label{fig:mid_T_slopes}
\end{figure}

$h$ is known to reach the limiting value $\frac{1}{6}$ for maximally mixed states and $N \to \infty$ \cite{hackmann_anders_2014}.
At the end of the exponential regime, curiously $h$ is closer to $\frac{1}{6}$ than in the limit of high temperatures.
This is depicted in figure~\ref{fig:all_T_overview_log}.

For $T \gg 1$, asymptotic values of $h$ are reached.
Once the number of bath particles is sufficiently high to dampen out oscillations (compare figure~\ref{fig:comparison_meissner}), the asymptotic values $h_\infty$ are related to $N$ via $h_\t{\infty}(N) = \frac{1}{6} + \frac{1}{N}$, as can be seen in figure~\ref{fig:asymptotic_T_power}.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{../Code/build/all_T_overview_log}
  \caption{Logarithmic overview of the temperature dependence of the envelope minimum $h$.}
  \label{fig:all_T_overview_log}
\end{figure}
\begin{figure}[H]
  \centering
  \includegraphics[scale=0.58]{../Code/build/asymptotic_T_power}
\caption{Asymptotic values for $h$ at $T = 100$. The limiting case is reached with $h_\infty(N) = \frac{1}{6} + \frac{1}{N}$.}
  \label{fig:asymptotic_T_power}
\end{figure}

\section{A remark on central spin coherences}
At $T=0$, for both types of initial bath states mentioned above the reduced density matrix of the central spin is strictly diagonal.
This is due to the fact that all intital states considered so far exhibit a definite polarization $S^z = S_b^z + S_0^z$ which is conserved by the Hamiltonian.
A flipping of the central spin is accompanied by a corresponding change in the bath polarization, and since all bath states are mutually orthogonal no coherences occur in the reduced density matrix of the central spin.

On the other hand, if the system is initially in a more general state
\begin{eqn}
  \Ket{\Psi_0} = \left(c_1\Ket{\Downarrow} + c_2\Ket{\Uparrow}\right) \Ket{\text{Bath}} \quad c_1, c_2 \neq 0\:,
\end{eqn}
it is well known that $\rho_0(t)$ exhibits coherences irrespectively of the bath state.
For example, if $\Ket{\text{Bath}} = \Ket{S_b, S_b^z}$, $\rho_0^{12}(t)$ is given by
\begin{eqns*}
  &\frac{c_1c_2^*}{(2 S_b + 1)^2} \left((S_b - S_b^z + 1)(S_b + S_b^z + 1) + (S_b - S_b^z)(S_b + S_b^z) \right.& \\
  &\left. + (S_b - S_b^z + 1)(S_b - S_b^z) \E^{-\I (2 S_b + 1) t} + (S_b + S_b^z + 1)(S_b + S_b^z) \E^{\I (2 S_b + 1) t}\right)\:.&\IEEEyesnumber
  \label{eq:coherences_product_general}
\end{eqns*}

If instead of the central spin only the bath is initially put in a superposition of different polarizations $S_b^z$, say
\begin{eqn}
  \Ket{\Psi_0} = \Ket{\Downarrow} \left(\alpha \Ket{S_b, S_b^z} + \beta\Ket{S_b, S_b^z + 1}\right)
\end{eqn}
again $\rho_0(t=0)$ has diagonal form. 
However, the state $\Ket{\Psi(t)}$ develops contributions $\left(c_1(t) \Ket{\Downarrow} + c_2(t) \Ket{\Uparrow}\right) \otimes \Ket{S_b, S_b^z}$ and thus coherences appear with dynamics determined by $c_1(t)$ and $c_2(t)$.
In this example, 
\begin{eqns}
  c_1(t) &=& \left((S_b-S_b^z+1) \E^{-\I S_b t} + (S_b + S_b^z) \E^{\I (S_b + 1) t}\right) \\
  c_2(t) &=& \sqrt{S_b-S_b^z}\sqrt{S_b + S_b^z + 1} \left(\E^{-\I S_b t} - \E^{\I (S_b + 1) t}\right)\:.
\end{eqns}
Note that since the central spin is a spin-$\frac{1}{2}$, no such contributions arise if the bath state is initially a superposition $\alpha \Ket{S_b, S_b^z} + \beta \Ket{S_b, S_b^z + k}, k \ge 2$ since the initial state would then be located in two subspaces of the Hilbert space that are ``sufficiently separated''.

So far, only initial product states of central spin and bath have been considered.
In a recent paper \cite{yu_2015}, Yu et al. investigated the influence of different initial bath states as well as different kinds of initial correlations between bath and central spin on the off-diagonal elements of $\rho_0(t)$.
In particular, apart from initial product states they considered the case of initial classical correlations
\begin{eqn}
  \rho = \abs{\alpha}^2 \Ket{\mu}\Bra{\mu} \otimes \Ket{S_b, S_b^z}\Bra{S_b, S_b^z} + \abs{\beta}^2 \Ket{\nu}\Bra{\nu} \otimes \Ket{S_b, S_b^z + 1}\Bra{S_b, S_b^z + 1}
  \label{eq:yu_classical_corr}
\end{eqn}
and initial entanglement
\begin{eqn}
  \rho = \left(\alpha \Ket{\mu} \Ket{S_b, S_b^z} + \beta \Ket{\nu} \Ket{S_b, S_b^z + 1}\right)\left(\alpha^* \Bra{\mu} \Bra{S_b, S_b^z} + \beta^* \Bra{\nu} \Bra{S_b, S_b^z + 1}\right)
  \label{eq:yu_entangled}
\end{eqn}
with orthogonal central spin states $\Ket{\mu}$ and $\Ket{\nu}$.
They found that while behaviour varies for different values of $\alpha$ and $\beta$ and different choices of $\Ket{\mu}$ and $\Ket{\nu}$, the amplitude of off-diagonal element oscillations is generally increased in the presence of initial correlations, especially for initial entanglement.

The occurence of coherences in these correlated states can be understood by focusing the individual terms in \eqref{eq:yu_classical_corr} and \eqref{eq:yu_entangled}.
In the cases of $\Ket{\mu} = \Ket{\Uparrow}$, $\Ket{\nu} = \Ket{\Downarrow}$ or vice versa, both terms in \eqref{eq:yu_classical_corr} exhibit a definite polarization $S^z$, respectively, and therefore no coherences are possible. 
In analogy to the different types of product states mentioned above, this changes if $\Ket{\mu}$ and $\Ket{\nu}$ are instead chosen to be superpositions of the basis states.

For initial entanglement, if $\Ket{\mu}$ and $\Ket{\nu}$ are chosen to be basis states, each term in \eqref{eq:yu_entangled} either has a definite $S^z$ or Ket and Bra differ in their $S^z$ value by two units.
Therefore no coherences occur unless $\Ket{\mu}$ and $\Ket{\nu}$ are chosen to be superpositions of basis states.
In this case there are multiple contributions of the form \eqref{eq:coherences_product_general}.

\printbibliography

\end{document}

