\chapter{Introduction}
\section{Motivation}
The field of quantum computing offers exciting prospects for the performance of tasks that are impossible to do efficiently on a classical computer.
A quantum computer exploits the fact that its information carriers, so-called qubits, are quantum mechanical two-state systems which can be in states $\Ket{0}$ and $\Ket{1}$ as well as in a superposition $\alpha \Ket{0} + \beta \Ket{1}$ with $\abs{\alpha}^2 + \abs{\beta}^2 = 1$.
This allows for an amount of parallelization which is not achievable by classical computers.

If a certain set of universal unitary operations can be performed on the ensemble of qubits, quantum computation is Turing-complete \cite{stolze_suter_2008} and thus every task that can be accomplished by a classical computer can also be done with a quantum computer.
Yet the main focus of quantum computing lies in the solution of a couple of specific problems for which quantum algorithms have been established, promising a significant qualitative speedup compared to classical algorithms.
For example, Shor's algorithm \cite{shor_1997} can be used to factorize a composite number $N$ in polynomial time in $\log(N)$ while the fastest classical algorithms need super-polynomial time.
As another example, Grover's algorithm \cite{grover_1996} is designed to search for a key in an unordered database.
The runtime of this algorithm is $\mathcal{O}(N^{1/2})$ as opposed to the worst-case runtime $\mathcal{O}(N)$ for any classical search algorithm.

One of the main issues quantum information processing faces today is system scalability.
Successful physical implementations of quantum computers such as systems using liquid state NMR \cite{stolze_suter_2008} suffer from the fact that the number of available qubits is severely limited, thereby restricting the applicability of quantum algorithms to trivial cases.
For instance, in 2001 a group at IBM managed to factorize the number \num{15} using Shor's algorithm on a liquid state NMR computer with \num{7} qubits \cite{vandersypen_2001}.
This record for largest number factored by quantum computers was broken eleven years later, in 2012, by the factorization of the number \num{21} via an optical setup \cite{martin_2012}.

Promising candidates for scalable quantum computing systems can be found in the domain of solid state physics.
Among many different schemes proposed, the use of electron spins confined to semiconductor quantum dots is of special interest because there exist various chemical methods for large-scale fabrication \cite{urbaszek_2013}.
Moreover, since the electron is strongly localized and only its spin but not its charge is used as the information carrier, long decoherence times are feasible in principle.
However, most semiconductor materials exhibit isotopes with nonvanishing magnetic momentum (e.g.\@\ce{^{29}Si} for Silicon and \ce{^{69}Ga} and \ce{^{71}Ga} for Gallium). Consequently, the hyperfine interaction of the electron spin with surrounding nuclear spins proves to be the limiting factor in achieving long decoherence times \cite{schliemann_khaetskii_loss_2003}.

The dynamics of a single spin-$\nicefrac{1}{2}$ in an ensemble of nuclear spins can be described by the central spin model.
It captures the interaction between the central spin-$\nicefrac{1}{2}$ and individual nuclear spins and is therefore suitable on a timescale where direct interactions between the nuclei can be neglected.
Additionally, a field may be applied to the central spin, expressing the fact that the qubit has to be controllable by external means for quantum computing.
In this thesis a special variant of the central spin model with homogeneous couplings to all nuclear spins is investigated.
While not particularly realistic, this choice of couplings allows for analytical treatment of the system in full generality.

This thesis is organized as follows: In chapter one, the central spin model is described in detail.
In chapter two, known results from \cite{bortz_stolze_2007} for different kinds of initial pure states of the system are discussed.
These results are built upon and generalized to different nuclear spin as well as initial mixed states in chapter three.
In chapter four the influence of temperature on the environment of the central spin is examined.
In chapter five an additional central field is applied and its effect on central spin dynamics is investigated for different initial temperatures of the bath.
Chapter six discusses different roles of entanglement: On the one hand, the loss of bipartite entanglement between the central spin and different spin partners, and on the other hand the influence of initial entanglement between the nuclear spins on the dynamics of the central spin.
Finally, the results are summarized in chapter seven and further possible directions of research are outlined.

\section{The central spin model}
The Hamiltonian of the general central spin model reads
\begin{eqn}
  H = \sum_{i=1}^N A_i \v{S}_0 \v{S}_i + \omega S_0^z\:,
  \label{eq:hamiltonian_central_field}
\end{eqn}
where $\v{S}_0$ is the spin operator of the central spin and $\v{S}_i$ the spin operator of the $i$th bath spin.
The coupling strength depends on the spin $I$ of a single nucleus, its magnetic moment $\mu_I$ and the amplitude of the electronic wave function $\Psi(r_i)$ at the site of the $i$th nucleus \cite{schliemann_khaetskii_loss_2003}:
\begin{eqn}
  A_i=\frac{4}{3 I} \mu_0 \mu_\t{B} \mu_I \abs{\Psi(\v{r}_i)}^2\:, \quad \mu_\t{B} = \frac{e\hbar}{2m_\t{e}}\:.
\end{eqn}

A typical quantum dot consists of about \SIrange{1e4}{1e6}{} nuclei \cite{urbaszek_2013}; for a \ce{GaAs} quantum dot, this yields an energy scale of the order of \SIrange{1e-5}{1e-4}{\electronvolt} for the combined hyperfine fields of the nuclei \cite{schliemann_khaetskii_loss_2003}.
On the other hand, dipolar interactions between neighboring nuclei, which are absent in \eqref{eq:hamiltonian_central_field}, are of the order of \SI{1e-12}{\electronvolt} energetically, corresponding to a characteristic timescale of \SIrange{1e-5}{1e-4}{\second} \cite{schliemann_khaetskii_loss_2003}.
This defines the timespan for which the central spin model is applicable.

The implementation of an external field can be realized by a magnetic field or by optical means, using the spin--orbit coupling and optical selection rules to transfer photonic angular momentum to the electron \cite{urbaszek_2013}.
The magnetic momenta of electronic and nuclear spins differ by about three orders of magnitude since the characteristic scale of the nuclear dipole moment, the nuclear magneton $\nicefrac{e\hbar}{2 m_\t{p}}$, is inversely proportional to the proton mass $m_\t{p}$, whereas the characteristic scale of the electronic dipole moment is given by the Bohr magneton $\nicefrac{e\hbar}{2 m_\t{e}}$.
This justifies the absence of a term coupling the external field to the nuclear spins in \eqref{eq:hamiltonian_central_field}.

For arbitrary couplings $A_i$ the Hamiltonian is solvable by the Bethe ansatz \cite{gaudin_1983, bortz_stolze_2007_inhomogeneous}.
However, this gets tedious for large systems with an arbitrary number of flipped spins and thus practically limits investigations to initial states with high degrees of polarization.
In the thermodynamic limit exact results can be obtained via a mean field solution \cite{bortz_stolze_2007}, yet this fails to reproduce dynamics for most initial states of the finite system since quantum correlations are neglected.

The central spin model becomes analytically solvable in the case of homogeneous couplings,
\begin{eqn}
  H = A \sum_{i=1}^N \v{S}_0 \v{S}_i + \omega S_0^z\:.
  \label{eq:hamiltonian}
\end{eqn}
As mentioned above, this choice of couplings is not realistic.
However, it serves in investigating the characteristics of system dynamics for arbitrary initial states instead of just the limiting cases mentioned above.

If $A=2$ is chosen and additionally the external field is neglected ($\omega = 0$) the Hamiltonian can be rewritten as
\begin{eqn}
  H = \v{S}^2 - \v{S}_b^2 - \v{S}_0^2\:,
  \label{eq:hamiltonian_simple}
\end{eqn}
where the total bath spin operator $\v{S}_b = \sum_i \v{S}_i$ and the total spin operator $\v{S} = \v{S}_b + \v{S}_0$ have been introduced.
From this form the energy eigenvalues can be determined:
\begin{eqn}
  E = \begin{cases}
    S_b\:, & S = S_b + \frac{1}{2} \\
    -S_b -1\:, & S = S_b - \frac{1}{2}
  \end{cases}\:.
  \label{eq:spectrum}
\end{eqn}
The spectrum of the system depends only on the total bath spin $S_b$ and the total spin $S$, that is, the orientation of the central spin with respect to the bath spin.
Since the values of $S_b$ are evenly spaced, the eigenenergies are commensurate and thus periodic dynamics are expected.
For an odd number of bath spins the recurrence time is $\tau = \pi$ and for an even number of bath spins it is $\tau = 2 \pi$ \cite{bortz_stolze_2007}.

By introducing the ladder operators $S_0^\pm = S_0^x \pm \I S_0^y$ and $S_b^\pm = S_b^x \pm \I S_b^y$ the Hamiltonian can also be rewritten as
\begin{eqn}
  H = A S_0^z S_b^z + \frac{A}{2} \left(S_0^+S_b^- + S_0^-S_b^+\right)\:.
\end{eqn}
In this form it becomes apparent that the projection $S^z$ of the total spin onto the $z$-axis is conserved. 

To span the full $2 \cdot 2^N$-dimensional Hilbert space (in the case of a bath of spins-$\nicefrac{1}{2}$) an additional quantum number $\alpha$ has to be introduced, capturing the fact that a certain total bath spin $S_b$ may be constructed from spin-$\nicefrac{1}{2}$ constituents in multiple ways.
This is further illustrated in figure \ref{fig:spinaddition}.
Thus a pure state of the system is fully specified by four quantum numbers
\begin{eqn}
  \Ket{\Psi_\t{Bath}} = \Ket{S, S^z, S_b, \alpha}\:.
\end{eqn}
\sidecaptionvpos{figure}{b}
\begin{SCfigure}[][h]
  \centering
  \includegraphics[width=0.5\textwidth]{../../figures/Spinaddition}
  \caption{By adding several spins-$\nicefrac{1}{2}$ different total spin values $S_b$ can be reached in various ways. For example, a total bath spin ${S_b = 1}$ can be reached with four spins-$\nicefrac{1}{2}$ in three different ways, yielding three different values of the additional quantum number $\alpha$.}
  \label{fig:spinaddition}
\end{SCfigure}
\FloatBarrier
The system may be prepared in one of several different kinds of initial states.
For the moment, only pure states will be considered.
Since the qubit is typically prepared independently of the bath, the central spin is initially in a product state with the bath, $\Ket{\Psi} = \Ket{\Psi_0}\Ket{\Psi_\t{Bath}}$.
Unless stated otherwise, this is true for all states considered in this thesis.

The nuclear spins may be prepared in a simple product state, for example $\Ket{\Psi_\t{Bath}} = \Ket{\downarrow \uparrow \uparrow \downarrow \cdots \downarrow}$, which is realizable experimentally by cooling down the bath in an external magnetic field \cite{schliemann_khaetskii_loss_2003}. 
They may also be prepared in a more general product state of the form
\begin{eqn}
  \Ket{\Psi_\t{Bath}} = \bigotimes_{i=1}^N a_i \Ket{\downarrow} + b_i \Ket{\uparrow}\:, \qquad \abs{a_i}^2 + \abs{b_i}^2 = 1 \quad \forall i \:.
\end{eqn}
The most general form of bath state is a superposition of product states
\begin{eqn}
  \Ket{\Psi_\t{Bath}} = \sum_{i=0}^{2^N} \alpha_i \Ket{\Psi_\t{Bath, prod.}}\:, \qquad \sum_i \abs{\alpha_i}^2 = 1
\end{eqn}
which usually exhibits some amount of entanglement between the bath spins.

Since for a single value of $S_b$ only two different eigenenergies determine the dynamics of the system, initial states 
\begin{eqn}
  \Ket{\Psi} = \Ket{\Psi_0}\Ket{S_b, S_b^z}
  \label{eq:bath_eigenstate}
\end{eqn}
that are eigenstates of the bath operators $\v{S}_b^2$ and $S_b^z$ are of particular interest.
In the following, the behavior for these types of states will be contrasted with the dynamics for initial simple product states with $M$ flipped spins, 
\begin{eqn}
  \Ket{\Psi} = \Ket{\Psi_0}|\underbrace{\downarrow\downarrow \ldots \downarrow}_{M} \underbrace{\uparrow\uparrow \ldots \uparrow}_{N-M} \rangle\:.
  \label{eq:bath_product}
\end{eqn}
