\chapter{Temperature dependence of the central spin model}
\label{sec:temperature}
\section{Analytical treatment}
While arbitrary pure bath states of definite polarization as well as the maximally mixed bath state can be investigated using the methods outlined above, it would also be interesting to have a look at the intermediate regime of sufficiently small but nonzero temperatures.
More specifically, an initial state of the form
\begin{eqn}
  \rho = \Ket{\Downarrow}\Bra{\Downarrow} \otimes \rho_\t{Bath, th.} = \sum_{\mathclap{S_b, S_b^z, \alpha}} \: p(\Downarrow, S_b, S_b^z, \alpha) \Ket{\Downarrow,S_b, S_b^z, \alpha}\Bra{\Downarrow, S_b, S_b^z, \alpha}
  \label{eq:thermal_bath}
\end{eqn}
is investigated where the probabilities $p(\Downarrow, S_b, S_b^z, \alpha)$ exhibit a temperature dependence.
For explicitness, the extra quantum number $\alpha$ is also taken into consideration even though it has no influence on the energies and thus cannot influence the form of the state \eqref{eq:thermal_bath}.

The probability $p(\Downarrow, S_b, S_b^z, \alpha)$ can be calculated as the thermodynamic expectation value of the corresponding projector.
For this the partition function of the system needs to be determined.
Since the central spin is fixed, $\Ket{S_0^z} = \Ket{-\nicefrac{1}{2}} \equiv \Ket{\Downarrow}$, and it is shielded from thermal influence, the partition function of the system is determined only by the bath degrees of freedom:
\begin{eqns*}
  Z &=& \sum_{S_b S_b^z \alpha} \Bra{\Downarrow, S_b, S_b^z, \alpha} \E^{-\beta H} \Ket{\Downarrow, S_b, S_b^z, \alpha} \IEEEyesnumber\IEEEyessubnumber*\\
  &=& \sum_{S_b S_b^z \alpha} \: \sum_{S' S^{z\prime} S'_b  \alpha'} \sum_{S'' S^{z\dprime} S''_b \alpha''} \Braket{\Downarrow, S_b, S_b^z, \alpha | S', S^{z\prime}, S'_b, \alpha'} \IEEEnonumber\\
  && {} \cdot \Bra{S', S^{z\prime}, S'_b, \alpha'} \E^{-\beta H} \Ket{S'', S^{z\dprime}, S''_b, \alpha''} \Braket{S'', S^{z\dprime}, S''_b, \alpha'' | \Downarrow, S_b, S_b^z, \alpha} \\
  &=& \sum_{S_b S_b^z \alpha} \abs{\Braket{\Downarrow, S_b, S_b^z, \alpha | S_b+\nicefrac{1}{2}, S_b^z - \nicefrac{1}{2}, S_b, \alpha}}^2 \exp\left(-\beta E_{S_b+\nicefrac{1}{2}, S_b}\right) \IEEEnonumber \\
  && +                        \abs{\Braket{\Downarrow, S_b, S_b^z, \alpha | S_b-\nicefrac{1}{2}, S_b^z - \nicefrac{1}{2}, S_b, \alpha}}^2 \exp\left(-\beta E_{S_b-\nicefrac{1}{2}, S_b}\right)\:,
\end{eqns*}
where the quantities $\Braket{\Downarrow, S_b, S_b^z, \alpha | S_b\pm\nicefrac{1}{2}, S_b^z - \nicefrac{1}{2}, S_b, \alpha}$ are known form formulas \eqref{eq:cg_spin_one_half_down} and \eqref{eq:cg_spin_one_half_up}.

The probability of the system being in the state $\Ket{\Downarrow, S_b, S_b^z, \alpha}$ can then be calculated as follows:
{\allowdisplaybreaks
\begin{eqns*}[Cl]
  & \hspace{-0.4cm}p(\Downarrow, S_b, S_b^z, \alpha) = \Tr\left[\rho \Ket{\Downarrow, S_b, S_b^z, \alpha}\Bra{\Downarrow, S_b, S_b^z, \alpha}\right] \IEEEyesnumber\IEEEyessubnumber*\\
  =& \Tr \left[\sum_{S' S^{z\prime} S'_b \alpha'} \Ket{S', S^{z\prime}, S'_b, \alpha}\Bra{S', S^{z\prime}, S'_b, \alpha'} \frac{\E^{-\beta E_{S', S'_b}}}{Z} \Ket{\Downarrow, S_b, S_b^z, \alpha}\Bra{\Downarrow, S_b, S_b^z, \alpha}\!\right] \\
  =& \sum_{S'} \Braket{S', S_b^z-\nicefrac{1}{2}, S_b, \alpha | \Downarrow, S_b, S_b^z, \alpha} \Braket{\Downarrow, S_b, S_b^z, \alpha | S', S_b^z-\nicefrac{1}{2}, S_b, \alpha} \frac{\E^{-\beta E_{S', S_b}}}{Z}\\
  =& \frac{1}{Z}\exp\left(-\beta E_{S_b+\nicefrac{1}{2}, S_b}\right) \abs{\Braket{S_b + \nicefrac{1}{2}, S_b^z - \nicefrac{1}{2}, S_b, \alpha | \Downarrow, S_b, S_b^z, \alpha}}^2 \IEEEnonumber\\
  & {}+ \frac{1}{Z} \exp\left(-\beta E_{S_b-\nicefrac{1}{2}, S_b}\right) \abs{\Braket{S_b - \nicefrac{1}{2}, S_b^z - \nicefrac{1}{2}, S_b, \alpha | \Downarrow, S_b, S_b^z, \alpha}}^2\:.
\end{eqns*}}
With this, the dynamics of \eqref{eq:thermal_bath} can be determined. 

\section{Results}
Here and in the following, $k_\t{B} = 1$. 
Together with the energy scale of the hyperfine coupling ($A=2$ for a single hyperfine coupling) this fixes the temperature scale. 
For purely numerical reasons, the rest of this section focuses on bath spin $I = \nicefrac{3}{2}$.
Results are qualitatively the same for a different choice of $I$.

The dynamics of $\avg{S_0^z(t)}$ show oscillations that are increasingly damped with higher temperature, as is demonstrated in figure~\ref{fig:dampening_example} for a system of $N=29$ bath spins.
The middle part of the envelope can be approximated to reasonable accuracy by a function of the form $f(x) = a \cosh(b x + c) + d$.
This means that the decay of the spin signal is of exponential nature from a certain point on.

In examining the minimal height $h$ of the oscillation envelope as a function of the temperature, a rapid decay to values close to the asymptotic limit of $\nicefrac{1}{6}$ is found (figure \ref{fig:all_T_overview_log}, compare equation~\eqref{eq:merkulov}).
The larger the system size the steeper the decay, that is, the more sensitively the system reacts even to small variations of the temperature.

More precisely, numerical analysis reveals three regimes of temperature dependence:
For sufficiently small temperatures ($T \lesssim 0.2$), there is an Arrhenius-like relation 
\begin{eqn}
  h(T) = 0.5 - f(N) \exp\left(-\frac{E}{T}\right)
\end{eqn}
between $T$ and $h$ with a monotonically increasing function $f(N)$, as can be seen in figure~\ref{fig:small_T_arrhenius}.
The characteristic energy $E = 1$ is found to be independent of the system size.
This signifies that at very small temperatures the system shows some amount of resilience to thermal influence, with only little variation for different bath sizes.

For intermediate temperatures ($0.2 \lesssim T \lesssim 0.6$ for the system sizes investigated here) $h$ falls off exponentially as is evident from figure~\ref{fig:mid_T_exponential}.
It is found numerically (figure~\ref{fig:mid_T_slopes}) that
\begin{eqn}
  h(N, T) = (0.03 N - 0.3) \exp(-0.18 N + 1.7)\:.
  \label{eq:exponential_regime}
\end{eqn}
This is the regime in which the signal decays most rapidly with increasing temperature, even more so with higher $N$.

For high temperatures and large numbers of bath spins the asymptotic value $h_\infty = \nicefrac{1}{6}$ is expected from the result \eqref{eq:merkulov} of Merkulov et al..
This is indeed observed for $T \gg 1$ -- once the number of bath particles is sufficiently high to dampen out oscillations (compare figure~\ref{fig:comparison_meissner}), the asymptotic values $h_\infty$ are related to $N$ via 
\begin{eqn}
h_\t{\infty}(N) = \frac{1}{6} + \frac{1}{N}
\end{eqn}
as can be seen in figure~\ref{fig:asymptotic_T_power}.

Before this asymptotic regime is reached, however, $h$ takes on a global minimum visibly lower than $\nicefrac{1}{6}$ at roughly $1 \lesssim T \lesssim 10$ for the system sizes considered here.
The exact temperature at which this mininum occurs depends on the bath size $N$, as depicted in figure~\ref{fig:all_T_overview_log}.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{temperature/1.5/damping_example}
  \caption{Example for damped oscillations in a bath of $N=29$ particles of spin $\nicefrac{3}{2}$. The central part of the envelope can be approximated by a hyperbolic cosine function, signifying exponential decay of the signal.}
  \label{fig:dampening_example}
\end{figure}

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{temperature/1.5/all_T_overview_log}
  \caption{Temperature dependence of the minimum $h$ of the envelope of $\avg{S_0^z(t)}$. At very small temperatures only little variation is seen. After an exponentially fast decay a global minimum is reached at finite temperatures. In the high temperature limit the result of Merkulov et al.\@ is recovered for big system sizes $N$.}
  \label{fig:all_T_overview_log}
\end{figure}
\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{temperature/1.5/small_T_arrhenius}
  \caption{Arrhenius plot for the temperature dependence of the minimal height $h$ of the envelope of $\avg{S_0^z(t)}$ at small values of $T$. Here the system is somewhat resilient to thermal influence. Linear regression reveals a characteristic energy $E=1$ independent of the system size. The deviations at very small $T$ are of a numerical nature.}
  \label{fig:small_T_arrhenius}
\end{figure}
\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{temperature/1.5/mid_T_exponential}
  \caption{Logarithmic scaling of the $y$-axis reveals an exponential regime of the temperature dependence of the envelope minimum $h$. The strength of the exponential decay is proportional to the system size except for very small $N$. This means that for big system sizes the central spin signal responds very sensitively to a raise in the temperature beyond a certain level.}
  \label{fig:mid_T_exponential}
\end{figure}
\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{temperature/1.5/mid_T_slopes}
  \caption{Dependence of the exponent $k$ in equation \eqref{eq:exponential_regime} on the system size $N$. For the linear regression only those points with $N>5$ have been considered.}
  \label{fig:mid_T_slopes}
\end{figure}
\begin{figure}[ht]
  \centering
  \includegraphics[scale=0.58]{temperature/1.5/asymptotic_T_power}
  \caption{Asymptotic values of the spin signal envelope $h$ at $T = 10000$. The limiting case is reached with $h_\infty(N) = \nicefrac{1}{6} + \nicefrac{1}{N}$, recovering the result \eqref{eq:merkulov} of Merkulov et al.\@ for big system sizes $N$. For the linear regression only those data points with $N > 5$ have been considered.}
  \label{fig:asymptotic_T_power}
\end{figure}
