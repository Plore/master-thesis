\chapter*{Appendices}
\addcontentsline{toc}{chapter}{Appendices}
\addtocontents{toc}{\protect\setcounter{tocdepth}{0}}
\renewcommand{\theequation}{\Alph{section}.\arabic{equation}}
\renewcommand{\thesection}{\Alph{section}}
%\phantomsection
\label{sec:appendix}
\setlength{\IEEEnormaljot}{10pt}
\section{Time evolution of bath eigenstates}
\setcounter{equation}{0}
In some cases it is convenient to rewrite the time evolution of a system with the bath in a $\v{S}_b^2$, $S_b^z$ eigenstate explicitly as
\begin{eqns*}
  U(t) \Ket{\Downarrow}\Ket{S_b, S_b^z} &=& \frac{1}{2 S_b + 1} \left[ \sqrt{S_b - S_b + 1} \sqrt{S_b + S_b^z}\left( \E^{-\I S_b t} - \E^{\I (S_b + 1) t} \right) \Ket{\Uparrow}\Ket{S_b, S_b^z - 1} \right. \\
  && \left. + \left(\left(S_b - S_b^z + 1 \right)\E^{-\I S_b t} + \left(S_b + S_b^z\right) \E^{\I (S_b + 1) t}\right) \Ket{\Downarrow}\Ket{S_b, S_b^z}\right]
  \IEEEyesnumber \IEEEyessubnumber
  \label{eq:time_evolution_down} \\
  U(t) \Ket{\Uparrow}\Ket{S_b, S_b^z} &=& \frac{1}{2 S_b + 1} \left[ \left( \left(S_b + S_b^z + 1\right) \E^{-\I S_b t} + \left(S_b - S_b^z\right) \E^{\I(S_b + 1) t}\right) \Ket{\Uparrow}\Ket{S_b, S_b^z} \right. \\
  && \left. + \sqrt{S_b + S_b^z + 1} \sqrt{S_b - S_b^z} \left( \E^{-\I S_b t} - \E^{\I(S_b + 1) t}\right) \Ket{\Downarrow}\Ket{S_b, S_b^z + 1}\right]
  \IEEEyessubnumber
  \label{eq:time_evolution_up}
\end{eqns*}
using equations \eqref{eq:cg_spin_one_half_up}, \eqref{eq:cg_spin_one_half_down} and their inversions \eqref{eq:cg_spin_one_half_inverted_a}, \eqref{eq:cg_spin_one_half_inverted_b} as well as the eigenspectrum \eqref{eq:spectrum} of the Hamiltonian.
This may be abbreviated as
\begin{eqns*}
  U(t) \Ket{\Downarrow}\Ket{S_b, S_b^z} &=& A(t) \Ket{\Uparrow}\Ket{S_b, S_b^z - 1} + B(t) \Ket{\Downarrow}\Ket{S_b, S_b^z}  \IEEEyesnumber\IEEEyessubnumber* \label{eq:evolution_short_down}\\
  U(t) \ket{\Uparrow}\Ket{S_b, S_b^z}   &=& C(t) \Ket{\Uparrow}\Ket{S_b, S_b^z}     + D(t) \Ket{\Downarrow}\Ket{S_b, S_b^z + 1}\:. \label{eq:evolution_short_up}
\end{eqns*}
In this way, it is especially apparent that flipping of the central spin is associated with a corresponding change in the bath polarization $S_b^z$.

\section{Central spin coherences for different initial states}
\setcounter{equation}{0}
\label{ssec:appendix_coherences}
If the central spin and the bath exhibit definite polarizations $S_0^z$ and $S_b^z$, respectively, the reduced density matrix of the central spin is strictly diagonal.
This fact remains unchanged even if the bath is in a mixed state without any true quantum mechanical superpositions of states of different $S_b^z$.
A general mixed bath state expressed in bath eigenstates
\begin{eqns}
  \rho &=& \sum_{S_b, S_b^z} c_{S_b, S_b^z} \Ket{S_b, S_b^z}\Bra{S_b, S_b^z} \otimes \Ket{\Downarrow}\Bra{\Uparrow}
\end{eqns}
has the following time dependence, expressed by \eqref{eq:evolution_short_down} and \eqref{eq:evolution_short_up}:
\begin{eqns*}
  \rho(t) &=& \sum_{S_b, S_b^z} c_{S_b, S_b^z} \left( \abs{A(t)}^2 \Ket{S_b, S_b^z - 1}\Bra{S_b, S_b^z - 1} \Ket{\Uparrow}\Bra{\Uparrow} \right. \IEEEyesnumber\IEEEyessubnumber*\\
  && \hspace{1.9cm} \left. + A(t) B^*(t) \Ket{S_b, S_b^z - 1}\Bra{S_b, S_b^z} \Ket{\Uparrow}\Bra{\Downarrow}\right. \\
  && \hspace{1.9cm} \left. + B(t) A^*(t) \Ket{S_b, S_b^z}\Bra{S_b, S_b^z + 1} \Ket{\Downarrow}\Bra{\Uparrow}\right. \\
  && \hspace{1.9cm} \left. + \abs{B(t)}^2 \Ket{S_b, S_b^z}\Bra{S_b, S_b^z} \Ket{\Downarrow}\Bra{\Downarrow} \right)\:.
\end{eqns*}

From this it follows that
\begin{eqns}
  \rho_0(t) &=& \sum_{S_b, S_b^z} c_{S_b, S_b^z} \left(\abs{A(t)}^2 \Ket{\Downarrow}\Bra{\Downarrow} + \abs{B(t)}^2 \Ket{\Uparrow}\Bra{\Uparrow}\right)
\end{eqns}
has a diagonal shape.

For an initial state $\Ket{\Psi} = \left(c_1 \Ket{\Downarrow} + c_2 \Ket{\Uparrow}\right) \Ket{S_b, S_b^z}$ the time-dependent reduced density matrix of the central spin is determined by
\begin{eqns*}
  (2 S_b + 1)^2 \rho_0^{11}(t) &=& 2 \abs{c_2}^2 (S_b + S_b^z + 1) (S_b - S_b^z) \left(1 - \cos\left( (2 S_b + 1) t \right)\right) \IEEEyesnumber\IEEEnonumber\\
  && {} + \abs{c_1}^2 \left( (S_b - S_b^z + 1)^2 + (S_b + S_b^z)^2 \right. \\
  && \hspace{1.3cm} \left. {}+ 2 (S_b - S_b^z + 1)(S_b + S_b^z) \cos\left( (2 S_b + 1) t\right)\right) \IEEEyessubnumber\\
  (2 S_b + 1)^2 \rho_0^{12}(t) &=& \frac{c_1c_2^*}{(2 S_b + 1)^2} \left((S_b - S_b^z + 1)(S_b + S_b^z + 1) + (S_b - S_b^z)(S_b + S_b^z) \right. \\
  &&\left.{}+ (S_b - S_b^z + 1)(S_b - S_b^z) \E^{-\I (2 S_b + 1) t} \right. \\
  &&\left.{}+ (S_b + S_b^z + 1)(S_b + S_b^z) \E^{\I (2 S_b + 1) t}\right) \IEEEyessubnumber*\\
  \rho_0^{21}(t) &=& \left(\rho_0^{12}(t)\right)^* \\
  \rho_0^{22}(t) &=& 1 - \rho_0^{11}(t)\:.
\end{eqns*}

For an initial superposition of the bath, $\Ket{\Psi} = \Ket{\Downarrow} \left(\alpha \Ket{S_b, S_b^z} + \beta \Ket{S_b, S_b^z + 1}\right)$, one has
\begin{eqns*}
  (2 Sb + 1) ^2 \rho_0^{11}(t) &=& \abs{\alpha}^2 \left((S_b - S_b^z + 1) \E^{-\I S_b t} + (S_b + S_b^z) \E^{\I (S_b + 1) t}\right) \left(\t{h.c.}\right) \IEEEyesnumber\IEEEnonumber\\%(S_b - S_b^z + 1) \E^{\I S_b t} + (S_b + S_b^z) \E^{-\I (S_b + 1) t}\right) \\
  &&{}+ \abs{\beta}^2 \left((S_b - S_b^z) \E^{-\I S_b t} + (S_b + S_b^z + 1) \E^{\I (S_b + 1) t}\right) \left(\t{h.c.}\right)\IEEEyessubnumber\\%(S_b - S_b^z) \E^{\I S_b t} + (S_b + S_b^z + 1) \E^{-\I (S_b + 1) t}\right) \\
  (2 S_b + 1)^2 \rho_0^{12}(t) &=& \alpha \beta^* \left((S_b - S_b^z + 1) \E^{-\I S_b t} + (S_b + S_b^z) \E^{\I (S_b + 1) t}\right) \\
  &&{}\cdot \sqrt{S_b - S_b^z}\sqrt{S_b + S_b^z + 1} \left(\E^{-\I S_b t} - \E^{\I (S_b + 1) t}\right) \IEEEyessubnumber*\\
  \rho_0^{21}(t) &=& \left(\rho_0^{12}(t)\right)^* \\
  \rho_0^{22}(t) &=& 1 - \rho_0^{11}(t)\:.
\end{eqns*}

\section{Loss of entanglement with a dedicated bath spin}
\setcounter{equation}{0}
The central spin and a dedicated bath spin are prepared in the entangled state $\frac{1}{\sqrt{2}}\left(\Ket{\Uparrow}\Ket{\down} + \Ket{\Downarrow}\Ket{\up}\right)$.
For simplicity, the rest of the bath is assumed to be in an eigenstate of $\v{S}_b^2$ and $S_b^z$.
Repeated use of \eqref{eq:cg_spin_one_half_up}, \eqref{eq:cg_spin_one_half_down}, \eqref{eq:cg_spin_one_half_inverted_a} and \eqref{eq:cg_spin_one_half_inverted_b} yields the following expressions for the dynamics of the spin pair:
\newpage
{\allowdisplaybreaks
\begin{eqns*}
  &U(t) \Ket{\Downarrow}\Ket{\uparrow}\Ket{S_b, S_b^z} = & \\
  & \Ket{\Downarrow}\Ket{\downarrow}\Ket{S_b, S_b^z + 1} & 
     \left\{ \frac{\sqrt{S_b + S_b^z + 1} \sqrt{S_b - S_b^z}}{(2 S_b + 1) (2 S_b + 2)} B\left(S_b + \frac{1}{2}, S_b^z + \frac{1}{2}, t\right) \right. \\
  && \left.{ }- \frac{\sqrt{S_b + S_b^z + 1} \sqrt{S_b - S_b^z}}{(2 S_b + 1) (2 S_b)}     B\left(S_b - \frac{1}{2}, S_b^z + \frac{1}{2}, t\right) \right\} \\
  &{ } + \Ket{\Downarrow}\Ket{\uparrow}\Ket{S_b, S_b^z} & 
     \left\{ \frac{S_b + S_b^z + 1}{(2 S_b + 1) (2 S_b + 2)} B\left(S_b + \frac{1}{2}, S_b^z + \frac{1}{2}, t\right) \right. \\
  && \left.{ } + \frac{S_b - S_b^z}{(2 S_b + 1) (2 S_b)} B\left(S_b - \frac{1}{2}, S_b^z + \frac{1}{2}, t\right)         \right\}\\
  &{ } + \Ket{\Uparrow}\Ket{\downarrow}\Ket{S_b, S_b^z} &
     \left\{ \frac{(S_b - S_b^z + 1) (S_b + S_b^z + 1)}{(2 S_b + 1) (2 S_b + 2)} A\left(S_b + \frac{1}{2}, t\right) \right. \\
  && \left.{ } - \frac{(S_b + S_b^z) (S_b - S_b^z)}{(2 S_b + 1) (2 S_b)} A\left(S_b - \frac{1}{2}, t\right)             \right\}\\
  &{ } + \Ket{\Uparrow}\Ket{\uparrow}\Ket{S_b, S_b^z - 1} &
     \left\{ \frac{\sqrt{S_b - S_b^z + 1} \sqrt{S_b + S_b^z} (S_b + S_b^z + 1)}{(2 S_b + 1) (2 S_b + 2)} A\left(S_b + \frac{1}{2}, t\right) \right. \\
  && \left.{ } + \frac{\sqrt{S_b - S_b^z + 1} \sqrt{S_b + S_b^z} (S_b - S_b^z)}{(2 S_b + 1) (2 S_b)} A\left(S_b - \frac{1}{2}, t\right) \right\}\:. \IEEEyesnumber
  \label{eq:ugly_one}
\end{eqns*}}

{\allowdisplaybreaks
\begin{eqns*}
  & U(t) \Ket{\Uparrow}\Ket{\downarrow}\Ket{S_b, S_b^z} = & \\
  & \Ket{\Downarrow}\Ket{\downarrow}\Ket{S_b, S_b^z + 1} & 
     \left\{ \frac{\sqrt{S_b + S_b^z + 1}\sqrt{S_b - S_b^z}(S_b - S_b^z + 1)}{(2 S_b + 1) (2 S_b + 2)} A\left(S_b + \frac{1}{2}, t\right) \right. \\
  && \left.{ } + \frac{\sqrt{S_b + S_b^z + 1}\sqrt{S_b - S_b^z}(S_b + S_b^z)}{(2 S_b + 1) (2 S_b)} A\left(S_b - \frac{1}{2}, t\right)      \right\} \\
  &{ } + \Ket{\Downarrow}\Ket{\uparrow}\Ket{S_b, S_b^z} &
     \left\{ \frac{(S_b - S_b^z + 1) (S_b + S_b^z + 1)}{(2 S_b + 1) (2 S_b + 2)} A\left(S_b + \frac{1}{2}, t\right) \right. \\
  && \left.{ } - \frac{(S_b + S_b^z) (S_b - S_b^z)}{(2 S_b + 1) (2 S_b)} A\left(S_b - \frac{1}{2}, t\right)             \right\} \\
  &{ }  + \Ket{\Uparrow}\Ket{\downarrow}\Ket{S_b, S_b^z} &
     \left\{ \frac{S_b - S_b^z + 1}{(2 S_b + 1) (2 S_b + 2)} C\left(S_b + \frac{1}{2}, S_b^z - \frac{1}{2}, t\right) \right. \\
  && \left.{ } + \frac{S_b + S_b^z}{(2 S_b + 1) (2 S_b)} C\left(S_b - \frac{1}{2}, S_b^z - \frac{1}{2}, t\right)       \right\} \\
  &{ }  + \Ket{\Uparrow}\Ket{\uparrow}\Ket{S_b, S_b^z - 1} &
     \left\{ \frac{\sqrt{S_b - S_b^z + 1} \sqrt{S_b + S_b^z}}{(2 S_b + 1) (2 S_b + 2)} C\left(S_b + \frac{1}{2}, S_b^z - \frac{1}{2}, t\right) \right. \\
  && \left.{ } - \frac{\sqrt{S_b - S_b^z + 1} \sqrt{S_b + S_b^z}}{(2 S_b + 1) (2 S_b)} C\left(S_b - \frac{1}{2}, S_b^z - \frac{1}{2}, t\right) \right\}\:, \IEEEyesnumber
  \label{eq:ugly_two}
\end{eqns*}}
where the abbreviations
\begin{eqns}
  A(S_b, t) &=& \E^{-\I S_b t} - \E^{\I (S_b + 1) t} \\
  B(S_b, S_b^z, t) &=& \left(S_b - S_b^z + 1\right) \E^{-\I S_b t} + \left(S_b + S_b^z\right) \E^{\I (S_b + 1) t} \\
  C(S_b, S_b^z, t) &=& \left(S_b + S_b^z + 1\right) \E^{-\I S_b t} + \left(S_b - S_b^z\right) \E^{\I (S_b + 1) t}
\end{eqns}
have been used.
