\chapter{Different functions of entanglement}
\section{Loss of entanglement}
To view the role of the central spin in a quantum computational framework, it would be interesting to know how entanglement between the central spin and a partner spin behaves under time evolution of the system.
Since the treatment of multiple electron spins in separate quantum dots would exceed the scope of this thesis (the reader is, for example, referred to \cite{erbe_schliemann_2010} instead), here only two simple cases are investigated: first, the entanglement with an external ``spectator'' spin-$\nicefrac{1}{2}$ that has no dynamics of its own, second, the entanglement with a dedicated spin-$\nicefrac{1}{2}$ from the bath, with the rest of the bath being initially either in a $\v{S}_b^2$, $S_b^z$ eigenstate or in a maximally mixed state.
For simplicity, only the dynamics without the external central field are considered.

\subsection{Entanglement with an external spin}
%For an initial bath state that is a $\v{S}_b^2$, $S_b^z$ eigenstate, the corresponding methods from section \ref{sec:known_results} can be summarized two the equations \eqref{eq:time_evolution_down} and \eqref{eq:time_evolution_up} for convenience.
The central spin and the external spectator spin are prepared in the maximally entangled state
\begin{eqn}
  \frac{1}{\sqrt{2}} \left( \Ket{\downarrow}_\t{\!ex} \Ket{\Uparrow} + \Ket{\uparrow}_\t{\!ex} \Ket{\Downarrow}\right)\:.
  \label{eq:external_entanglement_initial}
\end{eqn}
Since the spectator spin does not appear in the Hamiltonian, the dynamics of the system are determined only by the interaction of the central spin with the bath.

The entanglement of the spin pair is quantified by the concurrence
\begin{eqn}
  \mathcal{C} = \max(0, \lambda_1-\lambda_2-\lambda_3-\lambda_4)\:,
\end{eqn}
where $\lambda_1$, $\lambda_2$, $\lambda_3$ and $\lambda_4$ are the square roots of the eigenvalues of
\begin{eqn}
  \rho (\sigma_y \otimes \sigma_y)\rho^*(\sigma_y\otimes\sigma_y)\:.
\end{eqn}
in decreasing order of magnitude.
Here $\rho$ is the reduced density matrix of the spin pair and $\rho^*$ denotes complex conjugation in the product basis $\left\{ \Ket{\downarrow\downarrow}, \Ket{\downarrow\uparrow}, \Ket{\uparrow\downarrow}, \Ket{\uparrow\uparrow} \right\}$.
The methods from section \ref{sec:known_results} can be used in a straightforward way to determine the time dependent reduced density matrix of the two spins.

From figures \ref{fig:concurrences_external_example_Sb=9_2} and \ref{fig:concurrences_external_example_Sb=14_2} it becomes apparent that for an initial $\v{S}_b^2$, $S_b^z$ eigenstate of the bath the concurrence shows oscillatory behavior with a single frequency $\omega = 2 S_b + 1$, as was expected from the results in section \ref{sec:known_results}.
The amplitude of these oscillations depends on the polarization~$S_b^z$ of the bath, minimal polarization resulting in maximal amplitude and vice versa.
This can be explained qualitatively if the fact is taken into account that spin flips are suppressed for an initially upwards (downwards) pointing central spin and very high positive (negative) bath polarization.
Therefore both parts of \eqref{eq:external_entanglement_initial} contribute to the dynamics in the case of minimal bath polarization.

At small polarizations, entanglement between the two spins is lost entirely for certain time intervals.
However, maximal entanglement is always recovered even before the recurrence time.
The entanglement dynamics are furthermore invariant under reversal of the bath polarization~$S_b^z$.
This is to be expected because the initial state \eqref{eq:external_entanglement_initial} is invariant under $z\rightarrow-z$ and because the Hamiltonian does not distinguish between different orientations along the $z$-axis without an external field.

\begin{figure}[ht!]
  \centering
  \includegraphics[width=\textwidth]{entanglement_loss/concurrences_external_example_Sb=9_2}
  \caption{Concurrence of the central spin and a spectator spin with the bath being initially in a $\v{S}_b^2$, $S_b^z$ eigenstate with $S_b = \nicefrac{9}{2}$. The amplitude depends on the bath polarization, small values of $S_b^z$ corresponding to higher amplitudes. The concurrence is invariant under reversal of the bath polarization.}
  \label{fig:concurrences_external_example_Sb=9_2}
\end{figure}
\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{entanglement_loss/concurrences_external_example_Sb=14_2}
  \caption{Concurrence of the central spin and a spectator spin with the bath being initially in a $\v{S}_b^2$, $S_b^z$ eigenstate with $S_b = \nicefrac{14}{2}$. When compared to figure \ref{fig:concurrences_external_example_Sb=9_2}, it becomes clear that only the oscillation frequency is influenced by the bath spin $S_b$.}
  \label{fig:concurrences_external_example_Sb=14_2}
\end{figure}

\newpage
For a maximally mixed bath, entanglement dynamics are also closely related to the behavior of the central spin polarization already established in section \ref{sec:generalizations}.
After an initial dip, a plateau is reached whose extent and height are dependent on the number $N$ of bath spins.
In the limit of high $N$, entanglement between the two spins is lost almost entirely.
This is seen in figures \ref{fig:concurrences_external_mixed_even} and \ref{fig:concurrences_external_mixed_odd}.

\begin{figure}[ht!]
  \centering
  \includegraphics[width=\textwidth]{entanglement_loss/concurrences_external_mixed_even}
  \caption{Entanglement between the central spin and a spectator spin for a maximally mixed initial bath state of an even number $N$ of nuclear spins. Since only the central spin exhibits any dynamics, the concurrence closely mirrors the central spin polarization.}
  \label{fig:concurrences_external_mixed_even}
\end{figure}
\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{entanglement_loss/concurrences_external_mixed_odd}
  \caption{Entanglement between the central spin and a spectator spin for a maximally mixed initial bath state of an odd number $N$ of nuclear spins. Since the spectator spin does not have any dynamics of its own, the concurrence closely mirrors the central spin polarization.}
  \label{fig:concurrences_external_mixed_odd}
\end{figure}

\FloatBarrier

\subsection{Entanglement with a dedicated bath spin}
Entanglement dynamics of the central spin with a dedicated bath spin that partakes in dynamics shows more interesting behavior.

Again the two relevant spins are prepared in the state $\frac{1}{\sqrt{2}} \left( \Ket{\downarrow}\Ket{\Uparrow} + \Ket{\uparrow}\Ket{\Downarrow}\right)$.
If the rest of the bath is initially in a $\v{S}_b^2$, $S_b^z$ eigenstate, dynamics are now characterized by two frequencies corresponding to $S_b + \nicefrac{1}{2}$ and $S_b - \nicefrac{1}{2}$ since
\begin{eqns*}
  \Ket{\downarrow}\Ket{S_b, S_b^z} &=& \alpha \ \Ket{S_b + \frac{1}{2}, S_b^z - \frac{1}{2}, S_b} + \beta \ \Ket{S_b - \frac{1}{2}, S_b^z - \frac{1}{2}, S_b}\IEEEyesnumber\IEEEyessubnumber*\\
  \Ket{\uparrow}\Ket{S_b, S_b^z} &=& \gamma \ \Ket{S_b + \frac{1}{2}, S_b^z + \frac{1}{2}, S_b} + \delta \ \Ket{S_b - \frac{1}{2}, S_b^z + \frac{1}{2}, S_b}
\end{eqns*}
with coefficients $\alpha$, $\beta$ and $\gamma$, $\delta$ determined formulas \eqref{eq:cg_spin_one_half_down} and \eqref{eq:cg_spin_one_half_up}, respectively.
To determine $\mathcal{C}(t)$, it is easiest to calculate analytically the dynamics of the system using the methods outlined in section \ref{sec:known_results}.
For reference, the results are summarized in \eqref{eq:ugly_one}, \eqref{eq:ugly_two} in the appendix.
Therefrom, $\mathcal{C}(t)$ can be calculated in a straightforward manner.

The results are visualized in figures \ref{fig:concurrences_internal_example_Sb=9_2} and \ref{fig:concurrences_internal_example_Sb=14_2}.
As in the case of entanglement with a spectator spin, the oscillation frequency of the concurrence is determined by $S_b$ and the choice of $S_b^z$ affects the amplitude of the oscillations.
But now maximum entanglement is recovered before the recurrence time only in the case of vanishing bath polarization.
For intermediate bath polarization, entanglement gets lost almost completely after a certain amount of time, signifying decoherence of the central spin.
As before, the dynamics are invariant under ${S_b^z \rightarrow - S_b^z}$ for the same reasons as above.

If the rest of the bath is prepared in a maximally mixed state, the system shows behavior depicted in figure \ref{fig:concurrences_internal_mixed}.
The concurrence diminishes rapidly and is not recovered until the recurrence time, which again can be explained by the decoherence of the central spin.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{entanglement_loss/concurrences_internal_example_Sb=9_2}
  \caption{Entanglement between the central spin and a dedicated bath spin with the rest of the bath prepared in a $\v{S}_b^2$, $S_b^z$ eigenstate with $S_b = \nicefrac{9}{2}$. Maximum entanglement is restored only at the recurrence time. Intermediate values of the bath polarization $S_b^z$ show an almost complete loss of entanglement between the pair of spins.}
  \label{fig:concurrences_internal_example_Sb=9_2}
\end{figure}
\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{entanglement_loss/concurrences_internal_example_Sb=14_2}
  \caption{Entanglement between the central spin and a dedicated bath spin with the rest of the bath prepared in a $\v{S}_b^2$, $S_b^z$ eigenstate with $S_b = \nicefrac{14}{2}$. Maximum entanglement can be recovered before the recurrence time in the case of vanishing bath polarization $S_b^z$.}
  \label{fig:concurrences_internal_example_Sb=14_2}
\end{figure}
\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{entanglement_loss/concurrences_internal_mixed}
  \caption{Concurrence of the central spin and a dedicated bath spin with the rest of the bath initially in a maximally mixed state. Decoherence of the central spin brings about a rapid loss of entanglement}
  \label{fig:concurrences_internal_mixed}
\end{figure}

\FloatBarrier

\section{Initial bath entanglement}

\subsection{Motivation}
For the two different kinds of initial product states discussed in section \ref{sec:known_results}, fundamentally different kinds of dynamics were observed.
Initial $\v{S}_b^2$, $S_b^z$ eigenstates yield monochromatic oscillations, corresponding to solutions obtained via a mean field approach.
On the other hand, initial product states yield dynamics which show decoherence of the central spin on a timescale $\tau_\t{D} = O(1/\sqrt{N})$ \cite{bortz_stolze_2007}.

With the exception of fully polarized cases, bath eigenstates exhibit a certain amount of entanglement -- the most simple example of this being the states $\frac{1}{\sqrt{2}}\left(\Ket{\down\up} \pm \Ket{\up\down}\right)$ for the case of two spins-$\nicefrac{1}{2}$.
From this arises the question if it is the eigenstate property or rather the entanglement property that is responsible for causing the different types of dynamics. 
More specifically, to what extent can different forms of initial entanglement in the bath prevent or mitigate decoherence of the central spin?

Decoherence of a system is a result of its entanglement with the environment.
Thus if system--bath entanglement can be limited, possibly the effects of decoherence are preventable.
A simple example of how internal bath entanglement can achieve this is the following \cite{coffman_kundu_wootters_2000}:
Suppose two bath spins $B_1$ and $B_2$ are prepared in the singlet state, i.e.\@ they are maximally entangled.
A third spin $S$ can not become entangled with either $B_1$ or $B_2$ since then it would also be entangled with its respective partner.
Thus the pair $B_1 B_2$ as a whole would be entangled with $S$, exhibiting a mixed reduced density matrix, yet the singlet state is a pure state.

In extending this principle of limited multipartite entanglement, the natural entanglement measure is the so-called tangle \cite{coffman_kundu_wootters_2000}.
For an arbitrary state $\rho_{AB}$ of two spins $A$ and $B$, the tangle is defined as
\begin{eqn}
  \tau_{A|B} = \left(\max(0, \lambda_1-\lambda_2-\lambda_3-\lambda_4)\right)^2\:,
\end{eqn}
where $\lambda_i$ are, in decreasing order of magnitude, the square roots of the eigenvalues of
\begin{eqn}
  \rho_{AB} (\sigma_y \otimes \sigma_y)\rho_{AB}^*(\sigma_y\otimes\sigma_y)\:,
\end{eqn}
where $\rho^*$ denotes complex conjugation in the product basis $\left\{ \Ket{\down\down}, \Ket{\down\up}, \Ket{\up\down}, \Ket{\up\up} \right\}$.
Thus for two spins $A$ and $B$, $\tau_{A|B}$ is just the square of their concurrence.
For three spins, apart from pairwise entanglement there is also a three-way kind of entanglement quantified by the three-tangle
\begin{eqns}
  \tau_{A|B|C} &=& \tau_{A|BC} - \tau_{A|B} - \tau_{A|C}\:,
\end{eqns}
that is, the residual entanglement of $A$ with the pair $BC$ after the pairwise entanglement contributions have been taken into account.
There exist certain limiting relations for the two-tangle and the three-tangle \cite{dawson_et_al_2005}:
\begin{eqns}
  \tau_{B_1|B_2} + \tau_{S|B_1|B_2} &\le& 1\\
  \tau_{B_1|B_2} + \tau_{S|B_1} + \tau_{S|B_2} &\le& 4/3
\end{eqns}
that stem from the facts that the three-tangle is permutationally invariant and that two- and three-tangle are limited by $0 \le \tau \le 1$.

Dawson et al. \cite{dawson_et_al_2005} investigated the influence of initial bath states of the form
\begin{eqn}
  \Ket{\Psi_\t{Bath}} = \frac{a}{\sqrt{N}}\left(\Ket{\down\cdots\down\up} + \Ket{\down\cdots\down\up\down} + \ldots + \Ket{\up\down\cdots\down\down}\right) + b \Ket{\down \cdots \down}\:, \quad \! \! \abs{a}^2 + \abs{b}^2 = 1
\end{eqn}
on the dynamics of the central spin.
However, they investigated a central spin model with the Hamiltonian
\begin{eqn}
  H = g \sum_{k=1}^N \sigma_0^z \sigma_k^z
\end{eqn}
where no spin flips occur and thus initial bath entanglement stays constant.
It is not clear initially how results translate to the central spin model with isotropic couplings where bath entanglement gets lost at least partially.

To quantify initial bath entanglement, the so-called index of correlation \cite{barnett_phoenix_1991} is used:
\begin{eqn}
  I = -\sum_{i=1}^N \rho_i \log_2 \rho_i + \rho \log_2 \rho\:,
\end{eqn}
where $\rho_i$ denotes the reduced density matrix of the $i$th bath spin and $\rho$ is the density matrix of the bath.
In the present case, the index of correlation is simply the sum of the entanglement entropies of the individual bath spins since only pure initial bath states are investigated.

\subsection{GHZ class states}
The Greenberg\textendash Horne\textendash Zeilinger class of entangled states of the form
\begin{eqn}
  \Ket{\Psi} = a \Ket{\down \down \cdots \down} + b \Ket{\up \up \cdots \up}\:, \quad a^2 + b^2 = 1
  \label{eq:GHZ_class}
\end{eqn}
is investigated.
By the parameters $a$ and $b$ (both chosen as real numbers here), the degree of entanglement can be varied, with maximum entanglement at $a = b = \frac{1}{\sqrt{2}}$ and a product state in the limiting cases $a = 0$, $b = 1$ or vice versa.

Seeing that the two parts of state \eqref{eq:GHZ_class} are bath eigenstates with $S_b = \nicefrac{N}{2}$ and $S_b^z = \pm \nicefrac{N}{2}$, the time-dependent reduced density matrix of the central spin can be calculated analytically using the results from section \ref{sec:known_results}.
For initial polarization of the central spin in positive $z$-direction only diagonal terms appear in its reduced density matrix (compare equations \eqref{eq:dynamics_simple_a}, \eqref{eq:dynamics_simple_b}):
\begin{eqn}
  \rho_0(t) = \beta(t) \Ket{\Downarrow}\Bra{\Downarrow} + (1 - \beta(t)) \Ket{\Uparrow}\Bra{\Uparrow}
\end{eqn}
with
\begin{eqn}
  \beta(t) = \frac{2\abs{a}^2 N}{(N + 1)^2} \left(1 - \cos\left( (N + 1) t\right)\right)\:.
\end{eqn}
The dynamics of the central spin polarization and its entanglement entropy are visualized in figure \ref{fig:dynamics_GHZ_z} for different values of $a$ for a system of $N=9$ bath spins.
The amplitudes of the central spin polarization and the entanglement entropy both depend on $a$.
Plotting the maximal amplitudes as a function of the bath entanglement as quantified by the index of correlation and also as a function of the bath polarization $S_b^z$ (figure \ref{fig:entanglement_correlations_ghz}), it becomes clear that the bath polarization is the more significant physical property.
For a certain amount of initial entanglement $I$ within the bath, two different amplitudes of central spin polarization and entanglement entropy, respectively, can be obtained by interchanging $a$ and $b$.
On the other hand, both amplitudes are unambiguously correlated with the bath polarization $S_b^z$.
This could already be anticipated from the fact that spin flips are suppressed by a bath that is maximally polarized in the same direction as the central spin.

For quantum computing, however, it is desirable to diminish system--bath interaction irrespectively of the initial qubit polarization.
Thus it is necessary to investigate polarization and entanglement dynamics for different starting orientations of the central spin.
For initial polarization of the central spin in the $x$-direction the reduced density matrix reads
\begin{eqns*}
  \rho_0(t) &=& \Ket{\Downarrow}\Bra{\Downarrow} \left( \frac{\abs{a}^2}{2} + \frac{1}{2(N+1)^2}\left(\abs{b}^2(N^2 + 1) + 2 N \abs{a}^2 + 2 N \cos\left( (N + 1) t\right) \left(\abs{b}^2 - \abs{a}^2\right)\! \right)\! \right)\\
  &&{ } + \Ket{\Uparrow}\Bra{\Uparrow} \left(\frac{\abs{b}^2}{2} + \frac{1}{2(N+1)^2}\left(\abs{a}^2(N^2 + 1) + 2 N \abs{b}^2 + 2 N \cos\left( (N + 1) t\right) \left(\abs{a}^2 - \abs{b}^2\right)\! \right)\! \right) \\
  &&{ } + \Ket{\Downarrow}\Bra{\Uparrow} \left(\frac{1}{2(N+1)} \left(1 + \abs{b}^2 N \E^{\I(N+1)t} + \abs{a}^2 N \E^{-\I(N+1)t}\right)\!\right) + \Ket{\Uparrow}\Ket{\Downarrow} (\t{h.c.})\:.\IEEEyesnumber
\end{eqns*}
For initial $y$-polarization, only the off-diagonal elements get changed by a phase, $\rho_0^{12} \rightarrow \I \rho_0^{12}$, which yields identical dynamics.
The resulting transverse polarization components as well as the entanglement entropy of the central spin are visualized in figure \ref{fig:dynamics_GHZ_x}.
It can be seen that $\avg{S_0^x}$ shows oscillations in the shape of a cosine which does not depend on $a$ while $\avg{S_0^y}$ shows a sine-like oscillation with an amplitude that depends on $a$. 
This was to be expected since the central spin simply precesses in the magnetic field of the nuclei; the orientiation of precession (i.e.\@ the orientation of the bath field) can only be detected by examining $\avg{S_0^y}$.
The entanglement entropy gets diminished to some extent for bath states that are closer to product states with a maximal degree of positive or negative polarization.

For the maximally entangled case $a = b = \frac{1}{\sqrt{2}}$, dynamics are plotted as a function of the number $N$ of bath particles in figures \ref{fig:dynamics_GHZ_f=25_x} and \ref{fig:dynamics_GHZ_f=25_z}.
While for an initially $z$-polarized central spin the amplitudes of $\avg{S_0^z}$ and $E$ get diminished with increasing $N$, for initial $x$- or $y$-polarization the amplitudes are maximized.

In conclusion, behavior is qualitatively different for distinct initial polarizations of the central spin parallel or perpendicular to the $z$-axis.
For initial $z$-polarization, the bath can be tuned to minimize oscillations by increasing $N$, yet with initial transverse polarizations this is not possible.
For the maximal amplitude of the entanglement entropy no clear trend can be established.
Thus it seems that for entangled bath states of the GHZ class, it is rather the $\v{S}_b^2$, $S_b^z$ eigenstate property that produces characteristic behavior.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{bath_entanglement/GHZ/dynamics_GHZ_z}
  \caption{Central spin polarization and entanglement entropy for different initial bath states of the form \eqref{eq:GHZ_class}. The central spin is initially pointing in positive $z$-direction, the bath consists of $N=9$ spins-$\nicefrac{1}{2}$. Amplitudes get maximized for an initial bath state that has all spins pointing down.}
  \label{fig:dynamics_GHZ_z}
\end{figure}
\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{bath_entanglement/entanglement_correlations_ghz}
  \caption{Amplitudes of oscillations of central spin polarization and entanglement entropy, respectively, at half the recurrence time as a function of the index of correlation (left) and as a function of the bath polarization (right) for a bath of $N=9$ spins-$\nicefrac{1}{2}$. Amplitudes are correlated with $S_b^z$ but not with $I$.}
  \label{fig:entanglement_correlations_ghz}
\end{figure}
\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{bath_entanglement/GHZ/dynamics_GHZ_x}
  \caption{Transverse components of the central spin polarization and entanglement entropy for different initial bath states of the form \eqref{eq:GHZ_class}. The central spin is initially pointing in positive $x$-direction. The results for initial $y$-polarization are identical except for an exchange of the roles of $\avg{S_0^x}$ and $\avg{S_0^y}$.}
  \label{fig:dynamics_GHZ_x}
\end{figure}

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{bath_entanglement/GHZ/dynamics_GHZ_sym_x}
  \caption{Dynamics of the central spin polarization $\avg{S_0^x}$ and entanglement entropy for different bath sizes $N$ for the maximally entangled bath ($a = b = \frac{1}{\sqrt{2}}$). The central spin is initially polarized in positive $x$-direction. Increasing the system size results in maximization of the oscillation amplitudes. Results are identical for $\avg{S_0^y}$ with initial $y$-polarization.}
  \label{fig:dynamics_GHZ_f=25_x}
\end{figure}
\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{bath_entanglement/GHZ/dynamics_GHZ_sym_z}
  \caption{Dynamics of polarization $\avg{S_0^z}$ and entanglement entropy of the central spin for different bath sizes $N$ for the maximally entangled bath ($a = b = \frac{1}{\sqrt{2}}$). The central spin is initially polarized in positive $z$-direction. Maximizing the system size results in minimization of the oscillation amplitudes.}
  \label{fig:dynamics_GHZ_f=25_z}
\end{figure}

\FloatBarrier
\subsection{W class states}
The second class of entangled states investigated are the so-called W states
\begin{eqn}
  \Ket{\t{W}_\t{Bath}} = \frac{1}{\sqrt{N}} \left(\Ket{\downarrow \downarrow \cdots \downarrow \uparrow} + \Ket{\downarrow \downarrow \cdots \downarrow \uparrow \downarrow} + \cdots + \Ket{\uparrow \downarrow \downarrow \cdots \downarrow}\right)\:.
  \label{eq:W_class}
\end{eqn}
W class states do not exhibit genuine multipartite entanglement (for example, the three-tangle between any three spins in this state vanishes).
This can be seen by rearranging \eqref{eq:W_class} for even bath sizes $N$ (a similar expression exists for odd $N$):
\begin{eqn}
  \Ket{\t{W}_\t{Bath}} = \frac{1}{\sqrt{N}} \sum_{k=0}^{\frac{N}{2} - 1} \bigotimes_{j=1}^k \Ket{\downarrow \downarrow} \left(\Ket{\downarrow \uparrow} + \Ket{\uparrow\downarrow}\right) \bigotimes_{m=1}^{\frac{N}{2} - 1 - k} \Ket{\downarrow \downarrow}\:.
\end{eqn}
In each term of the sum there is only bipartite entanglement between two bath spins.

It turns out that \eqref{eq:W_class} is also a bath eigenstate with $S_b = \frac{N}{2}$, $S_b^z = \frac{N}{2} - 1$.
This makes it possible to state explicitly the time-dependent reduced density matrix of the central spin.
For initial $x$-polarization it reads
\begin{eqns*}
  \rho_0(t) &=& \Ket{\Downarrow}\Bra{\Downarrow} \left( N \left(1 - \cos\left((N + 1) t\right)\right) + \left(\frac{(N - 1)^2}{2} + 2 + 2 (N - 1) \cos\left((N + 1) t\right)\right)\right) \\
  &&{}+ \Ket{\Uparrow}\Bra{\Uparrow} \left(2 (N-1) \left(1 - \cos\left((N + 1) t\right)\right) + \left(\frac{N^2}{2} + \frac{1}{2} + N \cos\left((N + 1) t\right)\right)\right) \\
  &&{}+ \Ket{\Downarrow}\Bra{\Uparrow} \left(N + \frac{N - 1}{2} + \E^{-\I(N + 1) t} + \frac{N (N - 1)}{2} \E^{-\I (N + 1) t}\right) + \Ket{\Uparrow}\Bra{\Downarrow} (\t{h.c.})\:.\IEEEyesnumber
\end{eqns*}
As with the GHZ class states above, for initial $y$-polarization only $\rho_0^{12} \rightarrow \I \rho_0^{12}$ and the polarization dynamics as well as the dynamics of the entanglement entropy are unchanged.
For initial $z$-polarization $\rho_0(t)$ is given by \eqref{eq:dynamics_simple_a} and \eqref{eq:dynamics_simple_b}.
Therefore, in all cases dynamics are again determined by monochromatic oscillations with frequency $\omega = N + 1$ (compare figures \ref{fig:dynamics_W_x} and \ref{fig:dynamics_W_z}).
As to the influence of the bath size, it is possible to minimize oscillations by increasing $N$ for initial central spin polarization in $x$- or $y$-direction but not for initial $z$-polarization.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{bath_entanglement/W/dynamics_W_x}
  \caption{Dynamics of polarization $\avg{S_0^x}$ and entanglement entropy of the central spin for different bath sizes $N$. The central spin is initially polarized in positive $x$-direction. Increasing $N$ maximizes the amplitude of the spin polarization but minimizes the entanglement entropy. Results for $\avg{S_0^y}$ with initial $y$-polarization are identical.}
  \label{fig:dynamics_W_x}
\end{figure}
\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{bath_entanglement/W/dynamics_W_z}
  \caption{Dynamics of polarization $\avg{S_0^x}$ and entanglement entropy of the central spin for different bath sizes $N$. The central spin is initially polarized in positive $z$-direction. Increasing $N$ minimizes the amplitudes of bot the spin polarization and the entanglement entropy.}
  \label{fig:dynamics_W_z}
\end{figure}

Similarly to the case of entangled bath states of the GHZ class, it is possible to suppress spin flips in a trivial way by introducing a phase between the individual terms in \eqref{eq:W_class}.
The state
\begin{eqn}
  \Ket{\Psi_\t{Bath}} = \frac{1}{\sqrt{N}} \left(\Ket{\downarrow \downarrow \cdots \downarrow \uparrow} - \Ket{\downarrow \downarrow \cdots \downarrow \uparrow \downarrow} + \cdots + (-1)^{N-1} \Ket{\uparrow \downarrow \downarrow \cdots \downarrow}\right)\:,
  \label{eq:W_class_phase}
\end{eqn}
is orthogonal to \eqref{eq:W_class} and is characterized by $S_b = - S_b^z = \frac{N}{2} - 1$.
Therefore, if the central spin is initially polarized in $-z$-direction, any spin flip would send the bath into the $S_b = -S_b^z = \frac{N}{2}$ sector, yet $S_b$ is conserved.
However, as remarked before, limiting the initial polarization of the central spin isn't a viable solution for quantum computing.
Apart from that, preparation of a bath state of the form \eqref{eq:W_class_phase} would require control over individual bath spins which is not realistically feasible.

\FloatBarrier
\subsection{Products of entangled pairs}
To substantiate the results above, a third type of entangled bath state
\begin{eqn}
  \Ket{\Psi_\t{Bath}} = \bigotimes_{i=1}^\frac{N}{2} \left(\Ket{\downarrow\uparrow} - \Ket{\uparrow\downarrow}\right)
  \label{eq:pairwise_singlets}
\end{eqn}
is investigated.
As with W states, this product state of singlet pairs exhibits no multipartite entanglement.

Since a product of spin-zero states has itself vanishing spin, it is immediately clear that $S_b = S_b^z = 0$.
Therefore, spin flips are suppressed for the same fundamental reason as with the modified W states \eqref{eq:W_class_phase}, but now for any initial polarization of the central spin.
It becomes clear that this is a feature of the eigenstate property of the bath and not of its initial entanglement when the singlet pairs are exchanged with triplet pairs of vanishing polarization:
\begin{eqn}
  \Ket{\Psi_\t{Bath}} = \bigotimes_{i=1}^\frac{N}{2} \left(\Ket{\downarrow\uparrow} + \Ket{\uparrow\downarrow}\right)
  \label{eq:pairwise_entangled}
\end{eqn}
does not exhibit a definite $S_b$.
Therefore, spin flips are allowed even though the amount of initial entanglement is the same as in \eqref{eq:pairwise_singlets}
The dynamics of $\avg{S_0^z}$ and the entanglement entropy $E$ are visualized in figure \ref{fig:dynamics_pairwise_z} for the case of three triplets.

These results emphasize the fact that it is the eigenstate property of the bath rather than the entanglement property that distinguishes between different kinds of behavior.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{bath_entanglement/dynamics_pairwise_z}
  \caption{Central spin polarization and entanglement entropy for a bath of $N=6$ spins prepared in the state \eqref{eq:pairwise_entangled}. The central spin shows high amounts of entanglement entropy and thus its polarization exhibits large-amplitude variations.}
  \label{fig:dynamics_pairwise_z}
\end{figure}

\FloatBarrier
