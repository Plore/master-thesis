\chapter{Application of an external central field}
\section{Analytical treatment}
Until now, the dynamics of the central spin have been determined only by the interaction term $A \v{S}_b \v{S}_0$ of the full Hamiltonian \eqref{eq:hamiltonian}.
Applying a magnetic field of strength $\omega$ to the central spin in $z$-direction produces fundamentally different dynamics.

The external field term does not commute with the rest of the Hamiltonian since
\begin{eqns*}
  \left[S_0^z, \v{S}_0 \v{S}_b\right] &=& \left[S_0^z, S_0^x S_b^x\right] + \left[S_0^z, S_0^y S_b^y\right] + \underbrace{\left[S_0^z, S_0^z S_b^z\right]}_{=0} \IEEEyesnumber\IEEEyessubnumber*\\
  &=& \left[S_0^z, S_0^x\right] S_b^x + S_0^x\underbrace{\left[S_0^z, S_b^x\right]}_{=0} + \left[S_0^z, S_0^y\right] S_b^y + S_0^y\underbrace{\left[S_0^z, S_b^y\right]}_{=0} \\
  &=& 2 \I S_0^y S_0^x\:.
\end{eqns*}
Of course $\left[S_0^z, \v{S}_0^2\right] = \left[S_0^z, \v{S}_b^2\right] = 0$ and therefore $\left[S_0^z, \left(\v{S}_0 + \v{S}_b\right)^2\right] \neq 0$.
Thus the total spin $S$ is no longer a good quantum number and the eigenenergies can no longer be determined from~\eqref{eq:hamiltonian_simple}.
Rewriting the full Hamiltonian in terms of raising and lowering operators
\begin{eqn}
  H = A S_0^z S_b^z + \frac{A}{2} \left(S_0^+S_b^- + S_0^-S_b^+\right) + \omega S_0^z
\end{eqn}
makes it clear that $S^z$ is still conserved.
As was already investigated by Kozlov \cite{kozlov_2007}, the $2^{N+1} \times 2^{N+1}$-dimensional Hamiltonian matrix (for spin-$\nicefrac{1}{2}$) therefore decomposes into a structure of nested blocks.

Firstly, in a system of $N$ bath particles of spins-$\nicefrac{1}{2}$ there are $\lfloor N/2 \rfloor + 1$ different values of the conserved quantity $S_b$.
Since the extra quantum numbers $\alpha$ do not enter the Hamiltonian at all, each value of $S_b$ corresponds to $\Gamma_{S_b, S_b^z}$ identical blocks (compare equation~\eqref{eq:Gamma} and preceding remarks).
Each of these blocks has dimension $2 (2 S_b + 1)$ since there are $(2 S_b + 1)$ possible values of $S_b^z$ for a given $S_b$ and two possible values of the central spin polarization~$S_0^z$.
These blocks can be further decomposed into $2\times2$-blocks of definite $S^z$, corresponding to $\Ket{S_b, S_b^z, \Downarrow}$ and $\Ket{S_b, S_b^z-1, \Uparrow}$:
\begin{eqn}
  H_{S_b, S_b^z} = \frac{1}{2}
  \begin{pmatrix}
    - h - A S_b^z                        & A \sqrt{S_b(S_b+1) - S_b^z(S_b^z-1)}\\
    A \sqrt{S_b(S_b+1) - S_b^z(S_b^z-1)} & h + A (S_b^z - 1) \\
  \end{pmatrix}\:.
  \vspace{0.2cm}
\end{eqn}
The eigenenergies corresponding to the eigenvectors 
\begin{eqn}
  \Ket{\pm, S^z = S_b^z - \frac{1}{2}, S_b, \alpha}
\end{eqn}
are
\begin{eqn}
  E_{S_b, S_b^z}^{\pm} = -\frac{A}{4} \pm \frac{1}{2} \sqrt{\left(h + A S_b^z - \frac{S_b^z}{2}\right)^{\!2} + A^2\left(S_b (S_b + 1) - S_b^z (S_b^z - 1)\right)}
  \label{eq:eigenenergies_central_field}
\end{eqn}
for $S_b^z > -S_b$.
Additionally, there are two $1\times1$-blocks corresponding to maximal and minimal~$S^z$, respectively:
\begin{eqns*}
  E_{S_b, S_b, \Uparrow} &=& \frac{h + A S_b}{2} \IEEEyesnumber\IEEEyessubnumber*\\
  E_{S_b, -S_b, \Downarrow} &=&  \frac{-h + A S_b}{2}\:.
  \label{eq:eigenenergies_central_field_lim}
\end{eqns*}
With this, the dynamics of the system can once again be determined analytically.

\section{Results}
The dynamics of the central spin polarization for a maximally mixed initial bath state are visualized in figure \ref{fig:central_field_N=50_maximally_mixed} for different values of $\omega$.
Here $N=50$ and the central spin is initially polarized in positive $z$-direction.

Since in general the eigenenergies \eqref{eq:eigenenergies_central_field} are not commensurate, the dynamics no longer exhibit any periodicity.
Instead, a rich variety of nonperiodic oscillations occur due to the interplay between the external central field and the Overhauser field of the bath.

For small values of $\omega$ and small times the dynamics due to the bath field are dominant: After the initial drop to a local minimum the polarization assumes the constant value of $\avg{S_0^z} = \nicefrac{1}{6} + \nicefrac{1}{N}$, corresponding to maximal decoherence, until $t = \pi$ which for this choice of $N$ coincides with half the recurrence time $\tau$.
Without the external field, the polarization would then periodically re-enter the plateau of maximal decoherence and leave it at multiples of $\nicefrac{\tau}{2}$.
Here, however, the system eventually transitions to oscillatory behavior with frequencies dependent on $\omega$.
From the Fourier spectrum in the right part of figure \ref{fig:central_field_N=50_spectrum_EE} it can be seen that the system features a variety of frequencies, their average following closely the central field strength, $f = \frac{1}{2\pi} \omega$.
By increasing $\omega$, the transition to multi-frequency oscillations occurs earlier.

For $\omega = 50$ and $\omega = 100$ the system shows some initial oscillations driven by the central field which decay rapidly due to interaction with the bath.
The polarization then remains constant for a while since neither the external field nor the bath field induce any spin flips.
At $t = \nicefrac{\tau}{2} = \pi$ however, the bath again evokes a variation of $\avg{S_0^z}$; this brings about some amount of oscillation by the central field which then gets damped by the bath.
Depending on $\omega$, this behavior recurs several times at multiples of $\nicefrac{\tau}{2}$ before eventually the oscillation intervals start to overlap, ensuing in persistent nonperiodic oscillations.
For all values of $\omega$ the system eventually succumbs to this chaotic oscillatory behavior.
Apparently, greater external field strengths can stabilize the system for a longer amount of time, keeping it in a state of periodically recurring oscillations.

The entanglement entropy of the central spin, as visualized in the left side of figure \ref{fig:central_field_N=50_spectrum_EE}, mirrors the polarization dynamics.
High values of $\omega$ correspond to a low entanglement entropy, the external field keeping the central spin in an approximately pure state.
For smaller values of $\omega$, the entropy reaches a local minimum by the time the systems transitions from the regime dominated by the original bath field dynamics to the regime of nonperiodic oscillations.
Even for high values of the central field, $E$ does not get maximized until the system enters the domain of persistent oscillations.

The different effects of interaction with the bath and with the external central field on the transverse components of the central spin polarization are visualized in figure \ref{fig:central_field_polarization_components}.
As with $\avg{S_0^z}$, a recurrence of small oscillation packets is observed which eventually transition into persistent polychromatic fluctuations.
Moreover, a single low-frequency oscillation is visible that gets damped as a consequence of interaction with the bath field.
The higher the external field strength, the longer this large-scale oscillation is maintained.
The periodicity of this low-frequency oscillation does not depend on the value of $\omega$, which indicates it is a feature of the interaction with the bath field.
This is further corroborated by the fact that its amplitude diminished as $\omega$ increases.
In the limit of an infinitely strong central field, transverse components would not arise in the first place since the central spin would stay in its initial $z$-polarized state.

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{central_field/central_field_N=50_T=1000000}
  \caption{Central spin polarization $\avg{S_0^z(t)}$ for different central field strengths $\omega$. The bath is initially in a maximally mixed state with $N=50$. Traces of the periodic dynamics without the central field are still visible for low values of $\omega$ but are eventually superseded by erratic behavior. This transition occurs earlier for higher values of $\omega$, yet at $\omega > 50$ the central field has a stabilizing effect.}
  \label{fig:central_field_N=50_maximally_mixed}
\end{figure}
\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{central_field/central_field_N=50_T=1000000_spectrum_EE}
  \caption{Entanglement entropy of the central spin and Fourier spectrum of $\avg{S_0^z}$ for different central field strengths $\omega$. The bath is initially in a maximally mixed state with $N=50$. The entanglement entropy mirrors the central spin polarization, higher polarization corresponding to lower entropy. The Fourier spectrum shows linear depdendency between the average oscillation frequency and the central field strength.}
  \label{fig:central_field_N=50_spectrum_EE}
\end{figure}
\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{central_field/central_field_N=50_components}
  \caption{Transverse components of the central spin polarization for a maximally mixed bath of $N=50$ spins for $\omega = 20, 50, 100, 200$, respectively from top to bottom. The large-scale oscillation is due to the interaction with the Overhauser field since its frequency does not change with $\omega$. Again the stabilizing nature of large central field strengths becomes apparent.}
  \label{fig:central_field_polarization_components}
\end{figure}
\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{central_field/central_field_average_height}
  \caption{Average value of the central spin polarization parallel to the central field for different values of $N$ and $\omega$. In all cases, the bath is initially in a maximally mixed state. The higher the energy scale of the central field in comparison to that of the combined hyperfine interactions with the bath, the higher the amount of average polarization.}
  \label{fig:central_field_average_height}
\end{figure}

\FloatBarrier
\section{Central field at finite temperatures}
The above results were obtained for a maximally mixed bath state.
It is not too difficult to examine thermal states of finite temperature of the form
\begin{eqn}
  \rho = \Ket{\Uparrow}\Bra{\Uparrow} \otimes \rho_\t{Bath, th.} = \sum_{S_b, S_b^z, \alpha} p(\Uparrow, S_b, S_b^z, \alpha) \Ket{\Uparrow, S_b, S_b^z, \alpha}\Bra{\Uparrow, S_b, S_b^z, \alpha}\:.
\end{eqn}
Analogously to the case without an external central field, the partition function of the system is only determined by the bath degrees of freedom:
\begin{eqns*}
  Z &=& \sum_{S_b, S_b^z, \alpha} \Braket{\Uparrow, S_b, S_b^z, \alpha | \E^{-\beta H} | \Uparrow, S_b, S_b^z, \alpha} \IEEEyesnumber\IEEEyessubnumber*\\
  &=& \sum_{\substack{S_b, \alpha \\ S_b^z>-S_b}} \abs{\Braket{\Uparrow, S_b, S_b^z - 1, \alpha | +, S^z = S_b^z - \nicefrac{1}{2}, S_b, \alpha}}^2 \exp\left(-\beta E^+_{S_b, S_b^z}\right) \IEEEnonumber\\
  &&{ } + \abs{\Braket{\Uparrow, S_b, S_b^z - 1, \alpha | -, S^z = S_b^z - \nicefrac{1}{2}, S_b, \alpha}}^2 \exp\left(-\beta E^-_{S_b, S_b^z}\right) \IEEEnonumber\\
  &&{ } + \sum_{S_b, \alpha} \exp\left(-\beta E_{S_b, S_b, \Uparrow}\right)
\end{eqns*}
with the eigenenergies $E^\pm_{S_b, S_b^z}$ and $E_{S_b, S_b}$ from \eqref{eq:eigenenergies_central_field} and \eqref{eq:eigenenergies_central_field_lim}, respectively.

The thermodynamic probability for a pure state $\Ket{\Uparrow, S_b, S_b^z, \alpha}$ can then be calculated as
\begin{eqns*}
  p(\Uparrow, S_b, S_b^z, \alpha) &=& \Tr \left[ \rho \Ket{\Uparrow, S_b, S_b^z, \alpha}\Bra{\Uparrow, S_b, S_b^z, \alpha} \right] \IEEEyesnumber\IEEEyessubnumber*\\
  &=&\: \abs{\Braket{\Uparrow, S_b, S_b^z, \alpha | +, S_b^z + \nicefrac{1}{2}, S_b, \alpha}}^2 \frac{1}{Z} \exp\left(-\beta E^+_{S_b, S_b^z + 1}\right) \IEEEnonumber\\
  &&+ \abs{\Braket{\Uparrow, S_b, S_b^z, \alpha | -, S_b^z + \nicefrac{1}{2}, S_b, \alpha}}^2 \frac{1}{Z} \exp\left(-\beta E^-_{S_b, S_b^z + 1}\right)
\end{eqns*}
for $S_b^z < S_b$ and
\begin{eqn}
  p(\Uparrow, S_b, S_b, \alpha) = \frac{1}{Z}\exp\left(-\beta E_{S_b, S_b, \Uparrow}\right)
\end{eqn}
for $S_b^z = S_b$.
For initial negative polarization $\Ket{S_0^z} = \Ket{\Downarrow}$ of the central spin results can be obtained in a similar fashion.

The dynamics are visualized in figures \ref{fig:central_field_N=50_T=0.2} through \ref{fig:central_field_N=50_T=5.0} for the case of $N=50$ bath spins.
At $T=0.2$, decaying oscillations are visible for small values of the central field (for example, $\omega = 1$ through $\omega = 10$).
Oscillations recur after a certain period, however, just to decay again.
For higher $\omega$ this behavior occurs in shorter time intervals; for $\omega \ge 20$ oscillations become persistent (while getting diminished in amplitude, though).

At slightly higher temperature ($T=0.5$), initial oscillations die out quickly enough to reveal traces of the periodicity of the original dynamics.
Oscillations appear at multiples of half the original recurrence time $\tau$ of the system without the external field.
For intermediate values of the external central field ($\omega = 10$, $\omega = 20$) it can be seen that oscillations start overlapping, producing less regular oscillations.
This trend is emphasized for even higher temperatures ($T=1.0$).
The onset of chaotic oscillations occurs earlier for higher values of $\omega$ except for very high central field strengths where dynamics are stabilized for a longer amount of time.

At $T=5.0$, the system shows an almost immediate transition to chaotic behavior for nearly all values of $\omega$.
The curves are already nearly identical to those for the maximally mixed case in all but the average value.
The dependence of the average value of $\avg{S_0^z}$ on the temperature, taken over a time interval $\Delta t = 10 \pi$, is visualized in figure \ref{fig:central_field_average_heights_temperature}.
It can be seen that average polarization drops to an absolute minimum at finite temperatures before the limit of the maximally mixed bath is reached.
This is similar to the behavior found in section~\ref{sec:temperature} without the external field, suggesting an effect that is inherent to the interaction of the central spin with the Overhauser field of nuclear spins.

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{central_field/central_field_N=50_T=0.2}
  \caption{Central field polarization for different field strengths $\omega$ at $T=0.2$ and a bath size of $N=50$. Initial oscillations decay but recur in similar shape. For higher values of $\omega$, recurrence happens earlier, with the limiting case of persistent oscillations.}
  \label{fig:central_field_N=50_T=0.2}
\end{figure}
\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{central_field/central_field_N=50_T=0.5}
  \caption{Central field polarization for different field strengths $\omega$ at $T=0.5$ and a bath size of $N=50$. Periodically recurring oscillations are testimony of the interaction with the bath of nuclear spins. For $\omega \ge 20$ the transition to multi-frequency oscillations is already visible.}
  \label{fig:central_field_N=50_T=0.5}
\end{figure}
\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{central_field/central_field_N=50_T=1.0}
  \caption{Central field polarization for different values of $\omega$ at $T=1.0$, $N=50$. Oscillation packets are sufficiently smeared out to overlap and produce nonperiodic oscillations.}
  \label{fig:central_field_N=50_T=1.0}
\end{figure}
\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{central_field/central_field_N=50_T=5.0}
  \caption{Central field polarization for different values $\omega$ of the central field strength at $T=5$, $N=50$. Already the dynamics are nearly indistinguishable from those for a maximally mixed bath state, the only difference being the average value of the central spin polarization after initial relaxation.}
  \label{fig:central_field_N=50_T=5.0}
\end{figure}
\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{central_field/central_field_N=50_average_heights_temperature}
  \caption{Time average over an interval $\Delta t = 10 \pi$ of the central spin polarization $\avg{S_0^z}$ as a function of the temperature for different values of $\omega$ and a bath size of $N=50$. The dip in average value at intermediate temperatures bears resemblance to the findings in section \ref{sec:temperature}, suggesting an effect that originates in the interaction with the Overhauser field.}
  \label{fig:central_field_average_heights_temperature}
\end{figure}
