\chapter{Generalizations of known results}
\label{sec:generalizations}
\section{Generalization to arbitrary bath spin $I$}
In passing from bath particles of spin $\nicefrac{1}{2}$ to particles of arbitrary spin $I$, the general strategy for calculating the time-dependent reduced density matrix as outlined above can be maintained.
In fact, as long as the initial bath state is an eigenstate of $\v{S}_b^2$, nothing changes at all since the Hamiltonian \eqref{eq:hamiltonian} does not distinguish between internal compositions of the bath.
In the case of an initial product state, however, a few generalizations need to be made.

First of all, since the iterative scheme mentioned above now relies on addition of arbitrary angular momentum $I$ to the angular momentum of the bath in each step, all Clebsch--Gordan coefficients $\Braket{J, m_J, I, L | I, m_I, L, m_L = m_J - m_I}$ have to be determined.
Since $I$ is fixed among all bath spins, these will be abbreviated as $\alpha_{J, m_J, L}^{m_I}$.
With these coefficients, bath product states can be constructed iteratively again.
In contrast to the case $I=\nicefrac{1}{2}$, there are now more possibilities of individual spin polarization to choose from.
Starting from a state with $N-M$ spins of maximal positive polarization $m_{I,i} = I$, for example, and then adding $M$ spins with maximal negative polarization $m_{I,i} = -I$, one arrives at a result analogous to~\eqref{eq:monster}:
\begin{eqns}
  |\underbrace{-I, \ldots, -I}_{M},\underbrace{I, \ldots, I}_{N-M}\rangle &=& \sum_{k=0}^{2 I (M-1)} \sum_{j=1}^{\:G_{k}^{M-1}} c_{j,k}^{N,M} \sum_{i=0}^{2I} \alpha_{NI-k-i, (N-2M)I, (N-1)I-k}^{-I} \IEEEnonumber\\[5pt]
  && {}\cdot \Ket{NI-k-i, (N-2M)I, (N-1)I-k}\:.
  \label{eq:general_decomposition}
\end{eqns}
Here $G_k^M$ denote the multinomial coefficients which are appropriate for spin $I$ (for example, binomial coefficients for $I=\nicefrac{1}{2}$, trinomial coefficients \cite{andrews_1990} for $I=1$ and so on).

The central spin-$\nicefrac{1}{2}$ can be appended according to \eqref{eq:cg_spin_one_half_down} to obtain the complete initial state of the system:
\begin{eqns*}
  \Ket{\Downarrow}\Ket{\t{Bath}} &=& \sum_{k=0}^{2MI} \sum_{j=1}^{\:G_{k}^{M}} c_{j,k}^{N,M} \frac{1}{\sqrt{2(NI-k) + 1}} \\
  && {} \cdot \left(\sqrt{2MI - k + 1}\: \Ket{NI - k + \frac{1}{2}, (N-2M)I - \frac{1}{2}, NI - k}\right. \\
  &&\left.{}- \sqrt{2NI - 2MI - k}\: \Ket{NI - k -\frac{1}{2}, (N-2M)I-\frac{1}{2}, NI-k}\right)\:. \IEEEyesnumber
\end{eqns*}

Again the resulting $2 (2I+1)^N$ states are mutually orthogonal and it is convenient to use the quantities
\begin{eqn}
  d_{k,N,M} := \sum_{j=1}^{G_{k}^{M-1}}\left(c_{j,k}^{N,M}\right)^{\!2}
\end{eqn}
which are related by a recursion in $N$ and $M$:
\begin{eqns}
  d_{k,N,M} &=& \ \sum_{i=0}^{\mathclap{\min(k, 2I)}} \ d_{k,N-1,M-1} \left(\alpha_{NI-k, (N-2M+1)I, (N-1)I-k+i}^{-I}\right)^{\!2}\:.
\end{eqns}
While there is no simple explicit solution to this recursion since it depends on various Clebsch--Gordan coefficients, the $d_{k,N,M}$ can be calculated by a computer in a numerically stable way.
Expressed in these quantities the time-dependent reduced density matrix then reads
\begin{eqn}
  \rho_0(t) = \beta(t) \Ket{\Uparrow}\Bra{\Uparrow} + (1-\alpha(t)) \Ket{\Downarrow}\Bra{\Downarrow}
\end{eqn}
with
\begin{eqn}
  \beta(t) = 2 \sum_{k=0}^{2IM} \frac{d_{k,N,M}(2MI - k + 1)}{(2(NI - k) + 1)^2} (2I(N - M) - k) \left(1-\cos\left( (2 (NI - k) + 1) t \right)\right)\:. \IEEEeqnarraynumspace
\end{eqn}
The result is analogous to that for a bath product state of spin-$\nicefrac{1}{2}$ particles \eqref{eq:dynamics_product}.
However, instead of $M$ frequencies there now are $2MI$ frequencies involved.

If a different configuration of individual spin polarizations $\{m_{I,i}\}$ is chosen, the bath state can in principle be built in an analogous fashion using corresponding Clebsch--Gordan coefficients.
However, while for $I=\nicefrac{1}{2}$ it is always possible to express the bath product state in the form~\eqref{eq:monster} (interchanging the role of up- and down-spins if $M > N-M$ \cite{bortz_stolze_2007}), not all results for $I > \nicefrac{1}{2}$ can be expressed as compactly as equation \eqref{eq:general_decomposition}.
For example, if there are more than two types of bath spin polarization $m_I$ involved, keeping track of the coefficients $c_{j,k}^{N,M}$ becomes more cumbersome and consequently quantities like the $d_{k,N,M}$ are harder to determine.
Thus, while by superposition it is possible to examine the dynamics of more general initial product states of the form
\begin{eqn}
  \bigotimes_{i=1}^N \left(\sum_{m_I = -I}^I a_{i,m_I} \Ket{m_{I,i}}\right)\:, \qquad \sum_{m_I} a_{i,m_I}^2 = 1 \quad \forall i
%  \left(\alpha_1\Ket{\Uparrow} + \beta_1\Ket{\Downarrow}\right)\left(\alpha_2\Ket{\Uparrow} + \beta_2\Ket{\Downarrow}\right)\ldots\left(\alpha_N\Ket{\Uparrow} + \beta_N\Ket{\Downarrow}\right)\:, \quad \alpha_i^2 + \beta_i^2 \equiv 1
\end{eqn}
as well as the dynamics of mixed states, this can get quite tedious in practice.

\section{Treatment of mixed states}
The dynamics of systems with mixed bath states are analyzed most easily by expressing the bath in eigenstates of $\v{S}_b^2$ and $S_b^z$:
\begin{eqn}
  \rho = \Ket{\Psi_0}\Bra{\Psi_0} \otimes \sum_{S_b, S_b^z} c_{S_b, S_b^z} \Ket{S_b, S_b^z}\Bra{S_b, S_b^z}\:.
  \label{eq:mixed_state}
\end{eqn}
Here the summation over the additional quantum number $\alpha$ has been shifted into the coefficients $c_{S_b, S_b^z}$.
Once these are known, the dynamics of the central spin reduced density matrix
\begin{eqn}
  \rho_0(t) = \sum_{S_b, S_b^z} \Bra{S_b, S_b^z} \rho(t) \Ket{S_b, S_b^z}
\end{eqn}
can be determined by averaging expressions of type \eqref{eq:dynamics_simple_a}.

To illustrate this further, the case of a maximally mixed initial bath state is considered. 
This is a good approximation of the system at temperatures of a few \si{\kelvin} \cite{hackmann_anders_2014}.
If the central spin is chosen to be initially unentangled with the bath as before and it is aligned along the $z$-axis in positive direction, the initial density matrix of the entire system reads
\begin{eqn}
  \rho = \begin{pmatrix}
    1 & 0 \\
    0 & 0
  \end{pmatrix}
  \otimes \frac{1}{(2I+1)^N} \mathbb{1}
\end{eqn}
for arbitrary bath spin $I$.
Since all $(2 I + 1)$ different product states of the bath occur with equal probability, the coefficients $c_{S_b, S_b^z}$ in \eqref{eq:mixed_state} are determined by calculating combinatorially the dimension $\Gamma_{S_b, S_b^z}$ of the Hilbert subspace associated with a bath eigenstate $\Ket{S_b, S_b^z}$ -- that is, the number of different values the extra quantum number $\alpha$ can take for given $S_b$, $S_b^z$.

There are $G_{NI-S_b^z}^N$ different product states with the same value of $S_b^z$ ($G_M^N$ again being the appropriate multinomial coefficients for spin $I$).
Therefore
\begin{eqn}
  \Gamma_{S_b, S_b^z} = G_{NI-S_b}^N - G_{NI-(S_b+1)}^N\:.
  \label{eq:Gamma}
\end{eqn}
This is illustrated in figure~\ref{fig:degeneracy_illustration} for $I=\nicefrac{1}{2}$; see also \cite{arecchi_1972}.
The coefficients are then given by $c_{S_b, S_b^z} = \Gamma_{S_b, S_b^z} / (2 I + 1)^N$.
\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{../figures/degeneracy_illustration}
  \caption{Illustration of the scheme for counting the dimensionality $\Gamma_{S_b, S_b^z}$ of the Hilbert subspace associated with the quantum numbers $S_b, S_b^z$. In this example, $I=\nicefrac{1}{2}$, ${N = 5}$. Each line segment corresponds to a distinct product state $\Ket{\down\up\down\down\up \cdots}$ (or rather to an appropriate superposition with corresponding $S_b$, $S_b^z$). The subspace associated with the outlined state $\Ket{\nicefrac{3}{2}, \nicefrac{1}{2}}$ has dimensionality $\Gamma_{\nicefrac{3}{2}, \nicefrac{1}{2}} = \dbinom{5}{\nicefrac{5}{2} - \nicefrac{3}{2}} - \dbinom{5}{\nicefrac{5}{2} - \nicefrac{5}{2}} = 4$.}
  \label{fig:degeneracy_illustration}
\end{figure}
In the following, results for the case $I=\nicefrac{3}{2}$ are presented, which is relevant experimentally for baths of \ce{GaAs} nuclear spins. 
The results are in accordance with the calculations of Daniel Meißner \cite{meissner_2014}, as can be seen in figure~\ref{fig:comparison_meissner}.
There an approximate method using Chebyshev polynomials was used to determine central spin dynamics in an initially maximally mixed bath of spin-$\nicefrac{3}{2}$ particles.
Apparently, fluctuations in the central spin polarization are easily suppressed upon increasing the number of bath spins.
\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{comparisons/comparison_meissner.pdf}
  \caption{Comparison of the central spin polarization as obtained by the exact methods mentioned above with the results of Daniel Meißner \cite{meissner_2014}. The bath is initially in a maximally mixed state with system sizes of $N=3$, $N=5$ and $N=7$, respectively from top to bottom. There is good agreement between the two methods.}
  \label{fig:comparison_meissner}
\end{figure}

The short-time dynamics of the central spin for arbitrary hyperfine couplings were determined analytically by Merkulov et al.\@ \cite{merkulov_efros_rosen_2002} in the limit of a large number of bath spins for asymptotic temperature by way of a quasistatic approximation.
Assuming the nuclear spin configuration to be fixed on a sufficiently small timescale, the Bloch equations involving the magnetic field of the electron spin and the Overhauser field of the nuclei can be solved analytically.
Averaging over all possible values of the nuclear magnetic field one arrives at \cite{hackmann_anders_2014}
\begin{eqn}
  \avg{S_0^z(t)} = \frac{S_0^z(0)}{3} \left(1 + 2 \left(1 - \frac{1}{4} \frac{t^2}{(T^*)^2}\right) \E^{-\frac{1}{8}\frac{t^2}{(T^*)^2}}\right)
  \label{eq:merkulov}
\end{eqn}
in the special case of spins-$\nicefrac{1}{2}$, where the characteristic timescale
\begin{eqn}
  T^* = \frac{1}{\sqrt{\sum_i (A_i)^2}}
\end{eqn}
is determined by the hyperfine couplings.

The results obtained with the scheme above manage to reproduce this short-time behavior even for moderately small bath sizes $N$, as can be seen from figure \ref{fig:comparison_merkulov}.
The only difference lies in the periodicity of the system dynamics which is due to the particular choice of the $A_i$.
\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{comparisons/comparison_merkulov}
  \caption{Comparison of the short-time behavior of the central spin polarization as obtained by the exact scheme above for spin-$\nicefrac{1}{2}$ and $N=100$ (blue line) and by formula \eqref{eq:merkulov} (red dashed line). Apart from the periodicity of the system which is not captured by the result from Merkulov et al.\@, agreement between the two curves is excellent.}
  \label{fig:comparison_merkulov}
\end{figure}

\section{Superpositions and coherences}
At $T=0$, for both types of initial bath states mentioned above the reduced density matrix of the central spin is strictly diagonal.
This is due to the fact that all initial pure states considered so far exhibit a definite polarization $S^z = S_b^z + S_0^z$ which is conserved by the Hamiltonian.
A flipping of the central spin is accompanied by a corresponding change in the bath polarization, and since all bath states are mutually orthogonal no coherences occur in the reduced density matrix $\rho_0$ of the central spin.
The density matrix remains diagonal even in the case $T>0$ and for a wide class of general mixed bath states (see appendix \ref{ssec:appendix_coherences}).

On the other hand, if the system is initially in a pure state with the central spin in a superposition like
\begin{eqn}
  \Ket{\Psi} = \left(c_1\Ket{\Downarrow} + c_2\Ket{\Uparrow}\right) \otimes \Ket{\Psi_\t{Bath}}\:, \quad c_1, c_2 \neq 0\:,
  \label{eq:coherences_central_superposition}
\end{eqn}
initial coherences
\begin{eqn}
  \rho_0^{12}(t=0) = c_1 c_2^*
\end{eqn}
occur irrespectively of the bath state.
For the system at hand, those initial coherences never vanish completely.
To illustrate this, suppose that in the state above $\Ket{\Psi_\t{Bath}} = \Ket{S_b, S_b^z}$.
Then the time evolution of the coherences is determined by
\begin{eqns*}
  \hspace{-0.5cm}\rho_0^{12}(t) &=& \frac{c_1c_2^*}{(2 S_b + 1)^2} \left((S_b - S_b^z + 1)(S_b + S_b^z + 1) + (S_b - S_b^z)(S_b + S_b^z) \right. \\
  &&\left.{} + (S_b - S_b^z + 1)(S_b - S_b^z) \E^{-\I (2 S_b + 1) t} + (S_b + S_b^z + 1)(S_b + S_b^z) \E^{\I (2 S_b + 1) t}\right)\:.\IEEEyesnumber
  \label{eq:coherences_product_general}
\end{eqns*}
Note that even for the cases of maximal or minimal polarization $S_b^z = \pm S_b$ a constant contribution remains.

If instead of the central spin only the bath is initially put in a superposition of different polarizations $S_b^z$, say
\begin{eqn}
  \Ket{\Psi} = \Ket{\Downarrow} \otimes \left(\alpha \Ket{S_b, S_b^z} + \beta\Ket{S_b, S_b^z + 1}\right)\:,
  \label{eq:simple_bath_superposition}
\end{eqn}
then $\rho_0(t=0)$ has a diagonal form. 
However, in the time evolution of the state $\Ket{\Psi(t)}$ there appear terms of the form $\left(c_1(t) \Ket{\Downarrow} + c_2(t) \Ket{\Uparrow}\right) \otimes \Ket{S_b, S_b^z}$ -- that is, the bath superposition is transformed into a central spin superposition -- and thus coherences with dynamics determined by $c_1(t)$ and $c_2(t)$ show up (see Appendix \ref{ssec:appendix_coherences}).
In this example, 
\begin{eqns*}
  c_1(t) &\propto& \left((S_b-S_b^z+1) \E^{-\I S_b t} + (S_b + S_b^z) \E^{\I (S_b + 1) t}\right) \IEEEyessubnumber*\\
  c_2(t) &\propto& \sqrt{S_b-S_b^z}\sqrt{S_b + S_b^z + 1} \left(\E^{-\I S_b t} - \E^{\I (S_b + 1) t}\right)\:.
\end{eqns*}

Note that since the central spin is a spin-$\nicefrac{1}{2}$, no coherences can arise for initial bath superpositions
\begin{eqn}
  \Ket{\Psi} = \Ket{\Downarrow} \otimes \left(\alpha \Ket{S_b, S_b^z} + \beta\Ket{S_b, S_b^z + k}\right) \t{ with } k \ge 2
\end{eqn}
since even for $t>0$ any terms proportional to $\alpha$ will differ in their value of $S_b^z$ from those proportional to $\beta$ by at least one unit.
Thus states of the type \eqref{eq:simple_bath_superposition} are the only superpositions of bath eigenstates that produce coherences.
No coherences will arise in general superpositions of states that differ in their $S^z$-value by more than one unit.

\section{Initial correlations between central spin and bath}
So far, only initial product states of central spin and bath have been considered.
In a recent paper \cite{yu_et_al_2015}, Yu et al.\@ investigated the influence of different kinds of initial correlations between bath and central spin on the off-diagonal elements of $\rho_0(t)$.
In particular, apart from initial product states they considered the cases of initial classical correlations
\begin{eqn}
  \rho = \abs{\alpha}^2 \Ket{\mu}\Ket{S_b, S_b^z}\Bra{S_b, S_b^z}\Bra{\mu} + \abs{\beta}^2 \Ket{\nu}\Ket{S_b, S_b^z + 1}\Bra{S_b, S_b^z + 1}\Bra{\nu}
  \label{eq:yu_classical_corr}
\end{eqn}
and initial entanglement
\begin{eqn}
  \rho = \left(\alpha \Ket{\mu} \Ket{S_b, S_b^z} + \beta \Ket{\nu} \Ket{S_b, S_b^z + 1}\right)\left(\alpha^* \Bra{\mu} \Bra{S_b, S_b^z} + \beta^* \Bra{\nu} \Bra{S_b, S_b^z + 1}\right)
  \label{eq:yu_entangled}
\end{eqn}
with orthogonal central spin states $\Ket{\mu}$ and $\Ket{\nu}$.
They found that while behavior varies for different values of $\alpha$ and $\beta$ and different choices of $\Ket{\mu}$ and $\Ket{\nu}$, the amplitude of off-diagonal element oscillations is generally increased in the presence of initial correlations, especially for initial entanglement.

The occurrence of coherences in these correlated states can be understood by focusing on the individual terms in the respective density matrices \eqref{eq:yu_classical_corr} and \eqref{eq:yu_entangled}.

For classical correlations, each term in \eqref{eq:yu_classical_corr} represents a pure state and therefore the situation is analogous to that of \eqref{eq:coherences_central_superposition}.
If $\Ket{\mu} = \Ket{\Uparrow}$, $\Ket{\nu} = \Ket{\Downarrow}$ or vice versa, this corresponds to $c_1 = 0$ or $c_2 = 0$, respectively, and thus there are no coherences arising from the individual terms.
Since correlations in \eqref{eq:yu_classical_corr} are entirely classical, coherences simply add up for the two individual terms and thus vanish for the complete state as well.
Therefore in this case the presence or absence of off-diagonal elements in $\rho_0$ is completely dependent on the choice of the central spin states $\Ket{\mu}$, $\Ket{\nu}$, or, more precisely, on whether or not each term in \eqref{eq:yu_classical_corr} exhibits a definite polarization $S^z$.

For initial entanglement between central spin and bath, a similar reasoning holds.
If the central spin states are chosen as $\Ket{\mu} = \Ket{\Uparrow}$, $\Ket{\nu} = \Ket{\Downarrow}$, all four terms in~\eqref{eq:yu_entangled} have the same value of $S^z$, in which case the two diagonal terms will replicate the behavior of the classically correlated state \eqref{eq:yu_classical_corr} and the other two terms do not contribute to $\rho_0$ at all since the bath states are orthogonal.
If, on the other hand, $\Ket{\mu} = \Ket{\Downarrow}$ and $\Ket{\nu} = \Ket{\Uparrow}$, the two ket parts of~\eqref{eq:yu_entangled} differ in their $S^z$ values by two units and no coherences are possible as outlined in the section above.
So again off-diagonal elements will vanish unless the central spin states $\Ket{\mu}$, $\Ket{\nu}$ are chosen to be superpositions of the basis states, in which case $\rho_0^{12}$ is determined by multiple contributions of the form \eqref{eq:coherences_product_general}.

In conclusion, effects of both types of initial correlations crucially depend on the polarization~$S^z$ of the whole system.
