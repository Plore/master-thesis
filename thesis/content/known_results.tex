\chapter{Known results for the central spin model with homogeneous couplings}
\label{sec:known_results}
\section{Eigenstates of $\v{S}_b^2$ and $S_b^z$}
To determine the time evolution of both kinds \eqref{eq:bath_eigenstate} and \eqref{eq:bath_product} of initial states, their decompositions into states of total angular momentum $\Ket{S, S^z, S_b}$ are needed (the additional quantum number $\alpha$ can be ignored for now).
Since there are only spins-$\nicefrac{1}{2}$ in the system, it is sufficient to know how to add one spin-$\nicefrac{1}{2}$ particle to a bigger spin $\Ket{S_b, S_b^z}$. This can be done via the explicit formulas \cite{bortz_stolze_2007, schwabl}
\begin{eqns*}
  \Ket{\Downarrow}\Ket{S_b,S_b^z} &=& \frac{1}{\sqrt{2 S_b + 1}} \left(\sqrt{S_b - S_b^z + 1}\: \Ket{S_b + \frac{1}{2}, S_b^z - \frac{1}{2}, S_b}\right. \\
  && {}- \left.\sqrt{S_b + S_b^z}\: \Ket{S_b - \frac{1}{2}, S_b^z - \frac{1}{2}, S_b}\right) 
  \IEEEyesnumber \IEEEyessubnumber 
  \label{eq:cg_spin_one_half_down}\\
  \Ket{\Uparrow}\Ket{S_b,S_b^z-1} &=& \frac{1}{\sqrt{2 S_b + 1}} \left(\sqrt{S_b + S_b^z}\: \Ket{S_b + \frac{1}{2}, S_b^z - \frac{1}{2}, S_b}\right. \\
  && {}+ \left.\sqrt{S_b - S_b^z + 1}\: \Ket{S_b - \frac{1}{2}, S_b^z - \frac{1}{2}, S_b}\right)\:. 
  \IEEEyessubnumber 
  \label{eq:cg_spin_one_half_up}
\end{eqns*}
Inverting these equations yields
\begin{eqns*}
  \Ket{S_b + \frac{1}{2}, S_b^z - \frac{1}{2}, S_b} &=& \frac{1}{\sqrt{2 S_b + 1}} \left(\sqrt{S_b + S_b^z} \Ket{\Uparrow}\Ket{S_b, S_b^z - 1}\right. \\
  && + \left.\sqrt{S_b - S_b^z + 1} \Ket{\Downarrow}\Ket{S_b, S_b^z}\right) 
  \IEEEyesnumber \IEEEyessubnumber 
  \label{eq:cg_spin_one_half_inverted_a}\\
  \Ket{S_b - \frac{1}{2}, S_b^z - \frac{1}{2}, S_b} &=& \frac{1}{\sqrt{2 S_b + 1}} \left(\sqrt{S_b - S_b^z + 1} \Ket{\Uparrow}\Ket{S_b, S_b^z - 1}\right. \\
  && - \left.\sqrt{S_b + S_b^z} \Ket{\Downarrow}\Ket{S_b, S_b^z}\right) \:. 
  \IEEEyessubnumber
  \label{eq:cg_spin_one_half_inverted_b}
\end{eqns*}

If the system is initially in a state of type \eqref{eq:bath_eigenstate} with the central spin pointing down, $\Ket{\Psi_0} = \Ket{\Downarrow}$, formula \eqref{eq:cg_spin_one_half_down} provides a decomposition into eigenstates of the Hamiltonian.
Usually one is not interested in the dynamics of the bath degrees of freedom but only in those of the central spin.
Therefore, after applying time evolution it is necessary to employ formulas \eqref{eq:cg_spin_one_half_inverted_a} and \eqref{eq:cg_spin_one_half_inverted_b} to rewrite the system in terms of product states between the central spin and the bath.
The bath degrees of freedom can then be traced out of the corresponding density matrix to obtain the reduced density matrix of the central spin.
This procedure yields \cite{bortz_stolze_2007}
\begin{eqn}
  \rho_0(t) = \beta(t) \Ket{\Uparrow}\Bra{\Uparrow} + (1 - \beta(t)) \Ket{\Downarrow}\Bra{\Downarrow}\:,
  \label{eq:dynamics_simple_a}
\end{eqn}
where
\begin{eqn}
  \beta(t) = \frac{2 (S_b - S_b^z + 1)(S_b + S_b^z)}{(2 S_b + 1)^2} \left(1 - \cos\left( (2 S_b + 1) t\right) \right)\:.
  \label{eq:dynamics_simple_b}
\end{eqn}

\section{Product states}
In the case of an initial bath product state of the form \eqref{eq:bath_product}
one has to start from the $N-M$-particle state $\Ket{\uparrow \ldots \uparrow}$ and add $M$ spins in the down state $\Ket{\downarrow}$ iteratively according to formula \eqref{eq:cg_spin_one_half_down}.
Finally, the central spin is added in the same manner, yielding \cite{bortz_stolze_2007}
\begin{eqns*}
  \Ket{\Downarrow} |\underbrace{\downarrow \cdots \downarrow}_{M}\underbrace{\uparrow \cdots \uparrow}_{N-M}\rangle &=& \sum_{k=0}^{M} \frac{1}{\sqrt{N + 1 - 2k}} \sum_{j=1}^{\binom{M}{k}} c_{j,k}^{N+1,M+1}\\
  && {}\cdot \left(\sqrt{M + 1 - k}\:\Ket{\frac{N}{2} - k + \frac{1}{2}, \frac{N - 2M - 1}{2}, \frac{N}{2} - k} \right. \\
  && \left. {}- \sqrt{N - M - k}\:\Ket{\frac{N}{2} - k - \frac{1}{2}, \frac{N - 2M - 1}{2}, \frac{N}{2} - k} \right)\:. \IEEEyesnumber
  \label{eq:monster}
\end{eqns*}
Time evolution and tracing off the bath states results in 
\begin{eqn}
  \rho_0(t) = \beta(t) \Ket{\Uparrow}\Bra{\Uparrow} + (1 - \beta(t)) \Ket{\Downarrow}\Bra{\Downarrow} \:,
  \label{eq:dynamics_product}
\end{eqn}
where
\begin{eqn}
  \beta(t) = 2 \sum_{k=0}^{M} \frac{d_{k,N+1,M+1}}{(N + 1 - 2 k)^2} (M + 1 - k) (N - M - k) \left(1 - \cos\left( (N + 1 - 2 k) t\right)\right)\:.
\end{eqn}
For convenience, the quantities
\begin{eqn}
d_{k,N+1,M+1} := \sum_{j=1}^{\binom{M}{k}} \left(c_{j,k}^{N+1,M+1}\right)^{\!2}
\end{eqn}
have been used since all $2 \cdot 2^M$ states in~\eqref{eq:monster} are orthogonal (this could be seen more explicitly by keeping track of the additional spin numbers $\alpha$).
The $d_{k,N,M}$ fulfill a recursion relation in $N$ and $M$ \cite{bortz_stolze_2007} which is solved by
\begin{eqn}
  d_{k,N,M} = \frac{(M-1)!(N-M)!(N-2k)}{(N-k)!k!}\:.
\end{eqn}
With this the dynamics of the density matrix of the central spin are fully determined and its polarization and the von Neumann entropy can be calculated.
Instead of a single frequency, there are now $M+1$ frequencies involved.
