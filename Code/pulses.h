#ifndef PULSES_H
#define PULSES_H

typedef std::vector<std::complex<double>> compVec;
typedef std::function<compVec(compVec&)> gate;

std::vector<std::complex<double>> X(std::vector<std::complex<double>> &inputstate);

std::vector<std::complex<double>> Y(std::vector<std::complex<double>> &inputstate);

std::vector<std::complex<double>> Z(std::vector<std::complex<double>> &inputstate);

compVec rot(compVec &inputstate, double angle, compVec (*Axis)(compVec&));

std::vector<std::complex<double>> measure_Z(std::vector<std::complex<double>> &inputstate);

std::vector<compVec> sequence(compVec &initial, std::vector<gate> &actions, std::vector<double> &times, double step_t);

#endif
