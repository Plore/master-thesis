#include <iostream>
#include <cmath>
#include <complex>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

#include "dynamics.h"
#include "information.h"

void dynamics_GHZ(int N, double f, double step, double t_end)
{
	std::string poldown(N, '0');
	std::string polup(N, '1');
	std::vector<std::string> pols = {poldown, polup};

	std::vector<std::complex<double>>  x = {1.0 / sqrt(2), 1.0 / sqrt(2)};
	std::vector<std::complex<double>>  z = {0.0, 1.0};
	std::vector<std::vector<std::complex<double>>> central_orientations = {x, z};

	std::vector<std::string> suffixes = {"_x", "_z"};
	double a = 0 + f / 50.0;
	double b = 1 - f / 50.0;
	std::vector<double> coefs = {a, b};

	std::vector<std::complex<double>> central;
	wholeState GHZ;
	std::ofstream RDMfile;
	std::stringstream RDMfilename;

	for (int i = 0; i < 2; i++)
	{
		GHZ = state_decomposition(N, central_orientations[i], pols, coefs);
		RDMfilename.str("");
		RDMfilename << "build/bath_entanglement/GHZ/RDM_GHZ_N=" << N << "_f=" << f << suffixes[i] << ".txt";
		RDMfile.open(RDMfilename.str());
		if (!RDMfile.is_open())
			std::cout << "could not open file " << RDMfilename.str() << std::endl;
		for (int n = 0; n < t_end / step; n++)
		{
			compVec RDM = reduced_DM_whole(GHZ);
    	    evolve_whole(GHZ, step);
    	    RDMfile << n * step << " ";
    	    for (int i = 0; i < 4; i++)
    	        RDMfile << RDM[i].real() << " " << RDM[i].imag() << " ";
    	    RDMfile << std::endl;
		}
		RDMfile.close();
	}
}

void dynamics_W(int N, double step, double t_end)
{
	std::vector<std::string> pols;
	for (int n = 0; n < N; n++)
	{
		std::stringstream pol;
		for (int i = 0; i < n; i++)
			pol << "0";
		pol << "1";
		for (int i = 0; i < N-n-1; i++)
			pol << "0";
		std::cout << pol.str() << std::endl;
		pols.push_back(pol.str());
	}

	std::vector<std::complex<double>>  x = {1.0 / sqrt(2), 1.0 / sqrt(2)};
	std::vector<std::complex<double>>  z = {0.0, 1.0};
	std::vector<std::vector<std::complex<double>>> central_orientations = {x, z};

	std::vector<std::string> suffixes = {"_x", "_z"};

	std::vector<std::complex<double>> central;
	wholeState GHZ;
	std::ofstream RDMfile;
	std::stringstream RDMfilename;
	std::vector<double> coefs(pols.size(), 1.0 / sqrt(pols.size()));

	for (int i = 0; i < 2; i++)
	{
		GHZ = state_decomposition(N, central_orientations[i], pols, coefs);
		RDMfilename.str("");
		RDMfilename << "build/bath_entanglement/W/RDM_W_N=" << N << suffixes[i] << ".txt";
		RDMfile.open(RDMfilename.str());
		if (!RDMfile.is_open())
			std::cout << "could not open file " << RDMfilename.str() << std::endl;
		for (int n = 0; n < t_end / step; n++)
		{
			compVec RDM = reduced_DM_whole(GHZ);
    	    evolve_whole(GHZ, step);
    	    RDMfile << n * step << " ";
    	    for (int i = 0; i < 4; i++)
    	        RDMfile << RDM[i].real() << " " << RDM[i].imag() << " ";
    	    RDMfile << std::endl;
		}
		RDMfile.close();
	}
}

void dynamics_experimental(double step)
{
	std::vector<std::string> pols;
	pols.push_back("0000000000000");
	pols.push_back("0000000000001");
	pols.push_back("0000000000011");
	pols.push_back("0000000000111");
//	pols.push_back("0000000001111");
//	pols.push_back("0000000011111");
//	pols.push_back("0000000111111");
//	pols.push_back("0000001111111");
//	pols.push_back("0000011111111");
//	pols.push_back("0000111111111");
	pols.push_back("0001111111111");
	pols.push_back("0011111111111");
	pols.push_back("0111111111111");
	pols.push_back("1111111111111");

	std::vector<std::complex<double>>  x = {1.0 / sqrt(2), 1.0 / sqrt(2)};
	std::vector<std::complex<double>> nx = {1.0 / sqrt(2), -1.0 / sqrt(2)};
	std::vector<std::complex<double>>  y = {1.0 / sqrt(2), I * 1.0 / sqrt(2)};
	std::vector<std::complex<double>> ny = {1.0 / sqrt(2), -I * 1.0 / sqrt(2)};
	std::vector<std::complex<double>>  z = {0.0, 1.0};
	std::vector<std::complex<double>> nz = {1.0, 0.0};
	std::vector<std::vector<std::complex<double>>> central_orientations = {x, nx, y, ny, z, nz};

	std::vector<std::string> suffixes = {"_x", "_-x", "_y", "_-y", "_z", "_-z"};

	std::vector<std::complex<double>> central;
	wholeState GHZ;
	std::ofstream RDMfile;
	std::stringstream RDMfilename;
	std::vector<double> coefs(pols.size(), 1.0 / sqrt(pols.size()));

	for (int i = 0; i < 6; i++)
	{
		GHZ = state_decomposition(pols[0].size(), central_orientations[i], pols, coefs);
		RDMfilename.str("");
		RDMfilename << "build/bath_entanglement/RDM_experimental" << suffixes[i] << ".txt";
		RDMfile.open(RDMfilename.str());
		if (!RDMfile.is_open())
			std::cout << "could not open file " << RDMfilename.str() << std::endl;
		for (int n = 0; n < PI / step; n++)
		{
			compVec RDM = reduced_DM_whole(GHZ);
    	    evolve_whole(GHZ, step);
    	    RDMfile << n * step << " ";
    	    for (int i = 0; i < 4; i++)
    	        RDMfile << RDM[i].real() << " " << RDM[i].imag() << " ";
    	    RDMfile << std::endl;
		}
		RDMfile.close();
	}
}

void dynamics_pairwise(double step)
{
    std::vector<std::string> pols;
    pols.push_back("010101");
    pols.push_back("010110");
    pols.push_back("011001");
    pols.push_back("011010");
    pols.push_back("100101");
    pols.push_back("100110");
    pols.push_back("101001");
    pols.push_back("101010");

	std::vector<std::complex<double>>  x = {1.0 / sqrt(2), 1.0 / sqrt(2)};
	std::vector<std::complex<double>> nx = {1.0 / sqrt(2), -1.0 / sqrt(2)};
	std::vector<std::complex<double>>  y = {1.0 / sqrt(2), I * 1.0 / sqrt(2)};
	std::vector<std::complex<double>> ny = {1.0 / sqrt(2), -I * 1.0 / sqrt(2)};
	std::vector<std::complex<double>>  z = {0.0, 1.0};
	std::vector<std::complex<double>> nz = {1.0, 0.0};
	std::vector<std::vector<std::complex<double>>> central_orientations = {x, nx, y, ny, z, nz};

	std::vector<std::string> suffixes = {"_x", "_-x", "_y", "_-y", "_z", "_-z"};

	std::vector<std::complex<double>> central;
	wholeState GHZ;
	std::ofstream RDMfile;
	std::stringstream RDMfilename;
	std::vector<double> coefs = {1,1,1,1,1,1,1,1};

	for (int i = 0; i < 6; i++)
	{
		GHZ = state_decomposition(pols[0].size(), central_orientations[i], pols, coefs);
		RDMfilename.str("");
		RDMfilename << "build/bath_entanglement/RDM_pairwise" << suffixes[i] << ".txt";
		RDMfile.open(RDMfilename.str());
		if (!RDMfile.is_open())
			std::cout << "could not open file " << RDMfilename.str() << std::endl;
		for (int n = 0; n < PI / step; n++)
		{
			compVec RDM = reduced_DM_whole(GHZ);
    	    evolve_whole(GHZ, step);
    	    RDMfile << n * step << " ";
    	    for (int i = 0; i < 4; i++)
    	        RDMfile << RDM[i].real() << " " << RDM[i].imag() << " ";
    	    RDMfile << std::endl;
		}
		RDMfile.close();
	}
}

void dynamics_GHZ_sym_largeN(int N, double step, double t_end)
{
	std::ofstream RDMfile;
	std::stringstream RDMfilename;

	// x direction
	RDMfilename << "build/bath_entanglement/GHZ/RDM_GHZ_N=" << N << "_sym_x.txt";
	RDMfile.open(RDMfilename.str());
	if (!RDMfile.is_open())
		std::cout << "could not open file " << RDMfilename.str() << std::endl;
	compVec RDM(4, std::complex<double>(0, 0));
	RDM[0].real(0.5);
	RDM[3].real(0.5);
	for (int n = 0; n < t_end / step; n++)
	{
		double t = step * n;
		RDM[1].real(1.0 / (2 * (N + 1)) * (1 + N * cos((N + 1) * t)));
		RDM[2].real(1.0 / (2 * (N + 1)) * (1 + N * cos((N + 1) * t)));
	    RDMfile << t << " ";
	    for (int i = 0; i < 4; i++)
	        RDMfile << RDM[i].real() << " " << RDM[i].imag() << " ";
	    RDMfile << std::endl;
	}
	RDMfile.close();

	// z direction
	RDMfilename.str("");
	RDMfilename << "build/bath_entanglement/GHZ/RDM_GHZ_N=" << N << "_sym_z.txt";
	RDMfile.open(RDMfilename.str());
	if (!RDMfile.is_open())
		std::cout << "could not open file " << RDMfilename.str() << std::endl;
	RDM[0].real(0);
	RDM[1].real(0);
	RDM[2].real(0);
	RDM[1].imag(0);
	RDM[2].imag(0);
	RDM[3].real(0);
	for (int n = 0; n < t_end / step; n++)
	{
		double t = step * n;
		RDM[0].real(1.0 / 2 * alpha_pure_entangled(N / 2.0, N / 2.0, t) + 1.0 / 2 * alpha_pure_entangled(N / 2.0, -N / 2.0, t));
		RDM[3].real(1 - (1.0 / 2 * alpha_pure_entangled(N / 2.0, N / 2.0, t) + 1.0 / 2 * alpha_pure_entangled(N / 2.0, -N / 2.0, t)));
	    RDMfile << t << " ";
	    for (int i = 0; i < 4; i++)
	        RDMfile << RDM[i].real() << " " << RDM[i].imag() << " ";
	    RDMfile << std::endl;
	}
	RDMfile.close();
}

void dynamics_W_largeN(int N, double step, double t_end)
{
	std::ofstream RDMfile;
	std::stringstream RDMfilename;

	// x direction
	RDMfilename << "build/bath_entanglement/W/RDM_W_N=" << N << "_x.txt";
	RDMfile.open(RDMfilename.str());
	if (!RDMfile.is_open())
		std::cout << "could not open file " << RDMfilename.str() << std::endl;
	compVec RDM(4, std::complex<double>(0, 0));
	for (int n = 0; n < t_end / step; n++)
	{
		double t = step * n;
		RDM[0].real(1.0 / pow(N + 1, 2) * (N * (1 - cos((N + 1) * t)) + 0.5 * (4 + pow(N - 1, 2) + 4 * (N - 1) * cos((N + 1) * t))));
		RDM[1] = 0.5 / pow(N + 1, 2) * (2.0 * N + (N - 1) + 2.0 * std::exp(-I * double(N + 1) * t) + N * (N - 1) * 1.0 * std::exp(I * double(N + 1) * t));
		RDM[2] = std::conj(RDM[1]);
		RDM[3].real(1.0 / pow(N + 1, 2) * (2 * (N - 1) * (1 - cos((N + 1) * t)) + 0.5 * (pow(N, 2) + 1 + 2 * N * cos((N + 1) * t))));
	    RDMfile << t << " ";
	    for (int i = 0; i < 4; i++)
	        RDMfile << RDM[i].real() << " " << RDM[i].imag() << " ";
	    RDMfile << std::endl;
	}
	RDMfile.close();

	// z direction
	RDMfilename.str("");
	RDMfilename << "build/bath_entanglement/W/RDM_W_N=" << N << "_z.txt";
	RDMfile.open(RDMfilename.str());
	if (!RDMfile.is_open())
		std::cout << "could not open file " << RDMfilename.str() << std::endl;
	RDM[1].real(0);
	RDM[2].real(0);
	RDM[1].imag(0);
	RDM[2].imag(0);
	for (int n = 0; n < t_end / step; n++)
	{
		double t = step * n;
		RDM[0].real(alpha_pure_entangled(N / 2.0, N / 2.0 - 1, t));
		RDM[3].real(1 - alpha_pure_entangled(N / 2.0, N / 2.0 - 1, t));
	    RDMfile << t << " ";
	    for (int i = 0; i < 4; i++)
	        RDMfile << RDM[i].real() << " " << RDM[i].imag() << " ";
	    RDMfile << std::endl;
	}
	RDMfile.close();
}
