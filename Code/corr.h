#ifndef CORR_H
#define CORR_H

std::complex<double> corr_xx(double Sb, double Sbz, double h, double t);
std::complex<double> corr_yx(double Sb, double Sbz, double h, double t);
std::complex<double> corr_zz(double Sb, double Sbz, double h, double t);
std::complex<double> corr(int N, std::complex<double> (*corr_type)(double, double, double, double), std::vector<double> &degs, double a, double h, double t);

#endif
