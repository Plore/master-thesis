import numpy as np
from numpy.linalg import eigh
import matplotlib.pyplot as plt


Sb = 9.0
while Sb >= 0:
    for Sbz in range(Sb, -Sb - 1, -1):
        rdm_data = np.genfromtxt("build/extra_spin/extra_spin_entanglement_Sb={:.1f}_Sbz={:.1f}.txt".format(Sb, Sbz), unpack=True)
        
        # entangled state is |01> + |10>
        s10_real = rdm_data[0] # second spin starts down, stays down
        s10_imag = rdm_data[1]
        s11_real = rdm_data[2] # second spin starts down, gets up
        s11_imag = rdm_data[3]
        s00_real = rdm_data[4] # second spin starts up, gets down
        s00_imag = rdm_data[5]
        s01_real = rdm_data[6] # second spin starts up, stays up
        s01_imag = rdm_data[7]
        
        s00 = s00_real + 1j * s00_imag
        s01 = s01_real + 1j * s01_imag
        s10 = s10_real + 1j * s10_imag
        s11 = s11_real + 1j * s11_imag
        
        two_spin_states = 1 / np.sqrt(2) * np.array([s00, s01, s10, s11]).T
        
        #fig, axes = plt.subplots(1, 2, figsize=(10,6))
        eedata = []
        
        plt.clf()
        
        for i in range(len(rdm_data[0])):
            rdm = np.outer(two_spin_states[i], two_spin_states[i].conj())
            rdm_extra = rdm[::2,::2] + rdm[1::2,1::2]
            evs = eigh(rdm_extra)[0]
            eedata.append(- evs[0] * np.log2(evs[0]) - evs[1] * np.log2(evs[1]))
            #axes[0].imshow(rdm_extra.real, interpolation="nearest", cmap="Blues", vmin=-1, vmax=1, aspect="equal")
            #axes[1].imshow(rdm_extra.imag, interpolation="nearest", cmap="Blues", vmin=-1, vmax=1, aspect="equal")
            #fig.savefig("matrix_{:03}".format(i)) 

        Sb -= 1
plt.plot(range(len(rdm_data[0])), eedata)
plt.savefig("extra_spin_entanglement.pdf")
