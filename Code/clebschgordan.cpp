#include <iostream>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <Eigen/Dense>
#include <algorithm>

using namespace Eigen;

VectorXd clebsch(double J, double mj, double J1, double J2)
{
    double m1_lowest = (mj - J1 - J2 + abs(J1 - J2 + mj)) / 2.0;
    double m1_highest = (mj + J1 + J2 - abs(J1 - J2 - mj)) / 2.0;
    int n_ms = floor(m1_highest - m1_lowest + 1);
    auto a_diag = [J, mj, J1, J2, m1_lowest] (int k) {return J1 * (J1 + 1) + J2 * (J2 + 1) + 2 * (m1_lowest + k) * (mj - (m1_lowest + k)) - J * (J + 1);};
    auto a_offdiag = [mj, J1, J2, m1_lowest] (int k) {return sqrt(J1 * (J1 + 1) - (m1_lowest + k) * (m1_lowest + k + 1)) * sqrt(J2 * (J2 + 1) - (mj - (m1_lowest + k)) * (mj - (m1_lowest + k + 1)));};
    VectorXd diag(n_ms);
    VectorXd offdiag(n_ms - 1);
    for (int k = 0; k < n_ms - 1; k++)
    {
        diag[k] = a_diag(k);
        offdiag[k] = a_offdiag(k);
    }
    diag[n_ms - 1] = a_diag(n_ms - 1);
    MatrixXd A = MatrixXd::Zero(n_ms, n_ms);
    A.diagonal() = diag;
    A.diagonal(+1) = offdiag;
    A.diagonal(-1) = offdiag;
    VectorXd C = A.fullPivLu().kernel();
    return C.normalized();
}

std::vector<double> alphas(double J1, double J2, double mj1)
{
    double mj = mj1 - J2;
    int len = (int)(J1 + J2 - std::max(J1 - J2, mj) + 1);
    std::vector<double> res(len);
    for (int i = 0; i < len; i++)
    {
        VectorXd c = clebsch(J1 + J2 - i, mj, J1, J2);
        res[i] = c[len-1];
    }
    return res;
}

std::vector<double> ds(int N, int M, double I)
{
    // step one: calculate all alphas
    std::vector<std::vector<std::vector<double>>> all_alphas;
    for (int i = 0; i < M; i++)
    {
        std::vector<std::vector<double>> row;
        double Sbz = (N - M - i) * I;
        for (int j = 0; j < floor(2 * i * I + 1); j++)
        {
            std::vector<double> col;
            double Sb = (N - M + i) * I - j;
            std::vector<double> some_alphas = alphas(Sb, I, Sbz);
            for (unsigned int k = 0; k < some_alphas.size(); k++)
            {
                col.push_back(some_alphas[k]);
            }
            row.push_back(col);
        }
        all_alphas.push_back(row);
    }

    // step two: create ds using the alphas
    std::vector<std::vector<double>> all_ds;
    all_ds.push_back(std::vector<double>(1,1));
    for (int i = 1; i <= M; i++)
    {
        std::vector<double> row_ds(floor(2 * i * I + 1), 0);
        for (int j = 0; j < floor(2 * i * I + 1) ; j++)
        {
            for (int k = 0; k < 2 * I + 1; k++)
            {
                if (j - k >= 0 && j - k <= 2 * (i - 1) * I)
                {
                    row_ds[j] += all_ds[i - 1][j - k] * pow(all_alphas[i - 1][j - k][k], 2);
                }
            }
        }
        all_ds.push_back(row_ds);
    }
    return all_ds[M];
}
