#include <iostream>
#include <iomanip>
#include <cmath>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <numeric>

#include "clebschgordan.h"
#include "dynamics.h"
#include "information.h"

double factorial(int n)
{
    double res = 1.0;
    for (int k = 2; k <= n; k++)
    { res *= k;
    }
    return res;
}

double binom(int n, int m)
{
    if (n < m)
        std::cerr << "binomial coefficient: n should be bigger than m" << std::endl;
    return factorial(n) / (factorial(n - m) * factorial(m));
}

std::vector<std::vector<double>> multinomials(double I, int limit)
{
    std::vector<std::vector<double>> res;
    res.push_back(std::vector<double>(1,1));
    for (int n = 1; n <= limit; n++)
    {
        std::vector<double> row(2 * n * I + 1);
        for (unsigned int m = 0; m < row.size(); m++)
        {
            double c = 0;
            for (int k = 0; k < 2 * I + 1; k++)
            {
                if (m - k >= 0 && m - k < 2 * (n - 1) * I + 1)
                {
                    c += res[n-1][m-k];
                }
            }
            row[m] = c;
        }
        res.push_back(row);
    }
//	for (unsigned int i = 0; i < 6; i++)
//	{
//		for (auto d : res[i])
//			std::cout << d << " ";
//		std::cout << std::endl;
//	}
    return res;
}

std::vector<int> polarization(int code, int length)
{
    std::vector<int> res(length);
    int i = res.size() - 1;
    while (i >= 0)
    {
        res[i] = code % 2;
        code /= 2;
        i--;
    }
    return res;
}

std::vector<std::vector<double>> buildstate(std::string polarizations)
{
    auto downPlus  = [](double Sb, double Sbz){return 1.0 / sqrt(2 * Sb + 1) * sqrt(Sb - Sbz + 1);};
    auto downMinus = [](double Sb, double Sbz){return 1.0 / sqrt(2 * Sb + 1) * (-sqrt(Sb + Sbz));};
    auto upPlus    = [](double Sb, double Sbz){return 1.0 / sqrt(2 * Sb + 1) * sqrt(Sb + Sbz + 1);};
    auto upMinus   = [](double Sb, double Sbz){return 1.0 / sqrt(2 * Sb + 1) * sqrt(Sb - Sbz);};

    std::vector<std::vector<double>> res = std::vector<std::vector<double>>(polarizations.size() + 1);
    res[1].push_back(1);
    double Sbz = (polarizations[0] == '0') ? -0.5 : 0.5;
    for (unsigned int i = 1; i < polarizations.size(); i++)
    {
        // update only odd(even) Sb-sublattice in every step -> not possible for integer I!
        for (unsigned int j = i % 2; j <= i; j+=2)
        {
            double Sb = j / 2.0;
            for (unsigned int k = 0; k < res[j].size(); k++)
               res[j+1].push_back((Sb >= abs(Sbz)) ? res[j][k] * ((polarizations[i] == '0') ? downPlus(Sb, Sbz) : upPlus(Sb, Sbz)) : 0);
            if (j > 0)
            {
               for (unsigned int k = 0; k < res[j].size(); k++)
                   res[j-1].push_back((Sb >= abs(Sbz)) ? res[j][k] * ((polarizations[i] == '0') ? downMinus(Sb, Sbz) : upMinus(Sb, Sbz)) : 0);
            }
            res[j].clear();
        }
        Sbz += (polarizations[i] == '0') ? -0.5 : 0.5;
    }
    return res;
}

wholeState state_decomposition(int N, std::vector<std::complex<double>> &central, std::vector<std::string> &polarizations, std::vector<double> &bath_coefs)
{
    if (polarizations.size() != bath_coefs.size())
        std::cerr << "unequal lengths!" << std::endl;

    wholeState res(N + 1);

	// normalize coefficients
	double sum = 0;
    for (auto d : bath_coefs)
    {
        sum += pow(d, 2);
    }
    for (unsigned int i = 0; i < bath_coefs.size(); i++)
        bath_coefs[i] /= sqrt(sum);

    // determine dimensions for each Sb by using first state
    std::vector<std::vector<double>> helper = buildstate(polarizations[0]);
    for (unsigned int i = 0; i < helper.size(); i++)
    {
        res[i].resize(helper[i].size());
        for(unsigned int j = 0; j < res[i].size(); j++)
        {
            res[i][j].resize(2 * (2 * i / 2.0 + 1));
        }
    }

    // fill relevant vector entries
    for (unsigned int h = 0; h < polarizations.size(); h++)
    {
        std::string binary = polarizations[h];
        int M = std::count(binary.begin(), binary.end(), '0');
        double Sbz = N / 2.0 - M;
        std::vector<std::vector<double>> aux = buildstate(binary);

        for (unsigned int i = 0; i < res.size(); i++)
        {
            double Sb = i / 2.0;
            if (Sb < Sbz)
                continue;
            for(unsigned int j = 0; j < res[i].size(); j++)
            {
                res[i][j][int(Sb - Sbz + 0.1)]              += central[0] * aux[i][j] * bath_coefs[h];
                res[i][j][int(Sb - Sbz + 0.1 + 2 * Sb + 1)] += central[1] * aux[i][j] * bath_coefs[h];
            }
        }
    }
    //std::cout << "finished construction" << std::endl;
    return res;
}

double norm(wholeState &system)
{
    double sum = 0;
    for (unsigned int i = 0; i < system.size(); i++)
    {
        for (unsigned int j = 0; j < system[i].size(); j++)
        {
            for(unsigned int k = 0; k < system[i][j].size(); k++)
            {
                sum += std::norm(system[i][j][k]);
            }
        }
    }
    return sum;
}

void print_system(wholeState &system)
{
    for (auto a: system)
    {
        if (a.empty())
            std::cout << "o" << std::endl;
        else
        {
            for (auto b: a)
            {
                for (auto c : b)
                {
                    std::cout << c << " ";
                }
                std::cout << std::endl;
            }
        }
        std::cout << "--" << std::endl;
    }
    return;
}

void evolve_whole(wholeState &system, double t)
{
    double Sb, Sbz;
    for (unsigned int i = 0; i < system.size(); i++)
    {
        Sb = i / 2.0;
        for (unsigned int j = 0; j < system[i].size(); j++)
        {
            compVec res(2 * (2 * Sb + 1));
            for (unsigned int k = 0; k < system[i][j].size() / 2; k++)
            {
                Sbz = Sb - k;
                res[k] += 1.0 / (2 * Sb + 1) * ((Sb - Sbz + 1) * std::exp(-I * Sb * t) + (Sb + Sbz) * std::exp(I * (Sb + 1) * t)) * system[i][j][k];
                if (Sbz > -Sb)
                    res[k + 2 * Sb + 2] += 1.0 / (2 * Sb + 1) * sqrt(Sb + Sbz) * sqrt(Sb - Sbz + 1) * (std::exp(-I * Sb * t) - std::exp(I * (Sb + 1) * t)) * system[i][j][k];
            }

            for (unsigned int k = system[i][j].size() / 2; k < system[i][j].size(); k++)
            {
                Sbz = Sb - (k - (2 * Sb + 1));
                res[k] += 1.0 / (2 * Sb + 1) * ((Sb + Sbz + 1) * std::exp(-I * Sb * t) + (Sb - Sbz) * std::exp(I * (Sb + 1) * t)) * system[i][j][k];
                if (Sbz < Sb)
                    res[k - (2 * Sb + 2)] += 1.0 / (2 * Sb + 1) * sqrt(Sb + Sbz + 1) * sqrt(Sb - Sbz) * (std::exp(-I * Sb * t) - std::exp(I * (Sb + 1) * t)) * system[i][j][k];
            }
            system[i][j] = res;
        }
    }
    return;
}

std::vector<compVec> evolve(std::vector<compVec> &states, double t)
{
    std::vector<compVec> res(states.size());
    for (unsigned int n = 0; n < states.size(); n++)
    {
        compVec state = states[n];
        compVec res_single = compVec(state.size(), std::complex<double>(0,0));
        double Sb = ((state.size() / 2.0) - 1) / 2.0;
        double Sbz;

        for (unsigned int i = 0; i < state.size() / 2; i++)
        {
            Sbz = Sb - i;
            res_single[i] += 1.0 / (2 * Sb + 1) * ((Sb - Sbz + 1) * std::exp(-I * Sb * t) + (Sb + Sbz) * std::exp(I * (Sb + 1) * t)) * state[i];
            if (Sbz > -Sb + 0.1)
                res_single[i + 2 * Sb + 2] += 1.0 / (2 * Sb + 1) * sqrt(Sb + Sbz) * sqrt(Sb - Sbz + 1) * (std::exp(-I * Sb * t) - std::exp(I * (Sb + 1) * t)) * state[i];
        }

        for (unsigned int i = state.size() / 2; i < state.size(); i++)
        {
            Sbz = Sb - (i - (2 * Sb + 1));
            res_single[i] += 1.0 / (2 * Sb + 1) * ((Sb + Sbz + 1) * std::exp(-I * Sb * t) + (Sb - Sbz) * std::exp(I * (Sb + 1) * t)) * state[i];
            if (Sbz < Sb - 0.1)
                res_single[i - (2 * Sb + 2)] += 1.0 / (2 * Sb + 1) * sqrt(Sb + Sbz + 1) * sqrt(Sb - Sbz) * (std::exp(-I * Sb * t) - std::exp(I * (Sb + 1) * t)) * state[i];
        }
        res[n] = res_single;
    }

    return res;
}

compVec evolve_single(compVec &state, double t)
{
    compVec res = compVec(state.size(), std::complex<double>(0,0));
    double Sb = ((state.size() / 2.0) - 1) / 2.0;
    double Sbz;

    for (unsigned int i = 0; i < state.size() / 2; i++)
    {
        Sbz = Sb - i;
        res[i] += 1.0 / (2 * Sb + 1) * ((Sb - Sbz + 1) * std::exp(-I * Sb * t) + (Sb + Sbz) * std::exp(I * (Sb + 1) * t)) * state[i];
        if (Sbz > -Sb)
            res[i + 2 * Sb + 2] += 1.0 / (2 * Sb + 1) * sqrt(Sb + Sbz) * sqrt(Sb - Sbz + 1) * (std::exp(-I * Sb * t) - std::exp(I * (Sb + 1) * t)) * state[i];
    }

    for (unsigned int i = state.size() / 2; i < state.size(); i++)
    {
        Sbz = Sb - (i - (2 * Sb + 1));
        res[i] += 1.0 / (2 * Sb + 1) * ((Sb + Sbz + 1) * std::exp(-I * Sb * t) + (Sb - Sbz) * std::exp(I * (Sb + 1) * t)) * state[i];
        if (Sbz < Sb)
            res[i - (2 * Sb + 2)] += 1.0 / (2 * Sb + 1) * sqrt(Sb + Sbz + 1) * sqrt(Sb - Sbz) * (std::exp(-I * Sb * t) - std::exp(I * (Sb + 1) * t)) * state[i];
    }

    return res;
}

compVec reduced_DM_whole(wholeState &system)
{
    compVec res(4); //0:downdown 1:downup 2:updown 3:upup

    for (unsigned int i = 0; i < system.size(); i++)
    {
        for (unsigned int j = 0; j < system[i].size(); j++)
        {
            for(unsigned int k = 0; k < system[i][j].size() / 2.0; k++)
            {
                res[0] += system[i][j][k] * std::conj(system[i][j][k]);
                res[1] += system[i][j][k] * std::conj(system[i][j][system[i][j].size() / 2.0 + k]);
                res[2] += system[i][j][system[i][j].size() / 2.0 + k] * std::conj(system[i][j][k]);
                res[3] += system[i][j][system[i][j].size() / 2.0 + k] * std::conj(system[i][j][system[i][j].size() / 2.0 + k]);
            }
        }
    }
    return res;
}

compVec reduced_DM(std::vector<compVec> &pure_state)
{
    compVec res;
    std::complex<double> downdown;
    std::complex<double> downup;
    std::complex<double> updown;
    std::complex<double> upup;
    for (unsigned int n = 0; n < pure_state.size(); n++)
    {
        compVec single_state = pure_state[n];
        double Sb = ((single_state.size() / 2.0) - 1) / 2.0;
        for (unsigned int i = 0; i < 2 * Sb + 1; i++)
        {
            upup     += single_state[2 * Sb + 1 + i] * std::conj(single_state[2 * Sb + 1 + i]);
            updown   += single_state[2 * Sb + 1 + i] * std::conj(single_state[i]);
            downup   += single_state[i] * std::conj(single_state[2 * Sb + 1 + i]);
            downdown += single_state[i] * std::conj(single_state[i]);
        }
    }
    res.push_back(downdown);
    res.push_back(downup);
    res.push_back(updown);
    res.push_back(upup);
    return res;
}

compVec reduced_DM_single(compVec &pure_state)
{
    compVec res;
    std::complex<double> downdown;
    std::complex<double> downup;
    std::complex<double> updown;
    std::complex<double> upup;
    double Sb = ((pure_state.size() / 2.0) - 1) / 2.0;
    for (unsigned int i = 0; i < 2 * Sb + 1; i++)
    {
        upup     += pure_state[2 * Sb + 1 + i] * std::conj(pure_state[2 * Sb + 1 + i]);
        updown   += pure_state[2 * Sb + 1 + i] * std::conj(pure_state[i]);
        downup   += pure_state[i] * std::conj(pure_state[2 * Sb + 1 + i]);
        downdown += pure_state[i] * std::conj(pure_state[i]);
    }
    res.push_back(downdown);
    res.push_back(downup);
    res.push_back(updown);
    res.push_back(upup);
    return res;
}

double alpha_pure_entangled(double Sb, double Sbz, double t)
{
    return 2 * (Sb - Sbz + 1) * (Sb + Sbz) / pow((2 * Sb + 1), 2) * (1 - cos((2 * Sb + 1) * t));
}

double alpha_pure_product(int N, int M, double I, std::vector<double> &ds, double t)
{
    double res = 0;
    for (int k = 0; k < (int)(2 * M * I + 1); k++)
    {
        res += ds[k] / pow((2 * (N * I - k) + 1), 2) * 2 * (2 * M * I - k + 1) * (2 * N * I - 2 * M * I - k) * (1 - cos((2 * (N * I - k) + 1) * t));
    }
    return res;
}

double alpha_mixed_maximal(int N, std::vector<double> &degs, double I, double t)
{
    double res = 0;
    double min_J = I * N - (int)(I * N);
    double J = N * I;
    for (int k = 0; J >= min_J; k++)
    {
        J = N * I - k;
        double mj = -J;
        for (int l = 0; mj <= J; l++)
        {
            mj = -J + l;
            res += alpha_pure_entangled(J, mj, t) * degs[k];
        }
    }
    res /= pow((2 * I + 1), N);
    return res;
}

double alpha_punishment(int N, std::vector<double> &degs, double I, double a, double t)
{
    double gamma = a / N;
    double res = 0;
    double min_J = I * N - (int)(I * N);
    double J = N * I;
    for (int k = 0; J >= min_J; k++)
    {
        J = N * I - k;
        double mj = -J;
        for (int l = 0; l <= (int)(J * 2); l++)
        {
            mj = -J + l;
            res += alpha_pure_entangled(J, mj, t) * degs[k] * exp(-gamma * pow(mj, 2));
        }
    }
    double Z = 0;
    for (int j = 0; j <= N; j++)
        Z += binom(N, j) * exp(-gamma / 4 * pow((N - 2 * j), 2));
    return res / Z;
}

double alpha_mixed(int N, std::vector<double> &degs, double I, double T, double t)
{
    long double res = 0;
    long double Z = 0;
    double min_Sb = I * N - (int)(I * N);
    double Sb = N * I;
    for (int k = 0; Sb >= min_Sb; k++)
    {
        Sb = N * I - k;
        double Sbz = -Sb;
        for (int l = 0; l <= (int)(Sb * 2); l++)
        {
            Sbz = -Sb + l;
            res += alpha_pure_entangled(Sb, Sbz, t) * degs[k] * ((Sb + Sbz) / (2 * Sb + 1) * exp(-1.0 / T * (-Sb - 1)) + (Sb - Sbz + 1) / (2 * Sb + 1) * exp(-1.0 / T * Sb));
			Z   +=                                    degs[k] * ((Sb + Sbz) / (2 * Sb + 1) * exp(-1.0 / T * (-Sb - 1)) + (Sb - Sbz + 1) / (2 * Sb + 1) * exp(-1.0 / T * Sb));
        }
    }
    return res / Z;
}

void save_data(std::vector<double> &t_data, std::vector<double> &alpha_data, std::string filename)
{
    std::ofstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open())
    {
        std::cerr << "could not open file!" << std::endl;
    }
    else
    {
        for (unsigned int i = 0; i < t_data.size(); i++)
        {
            myfile << t_data[i] << " " << std::setprecision(15) << alpha_data[i] << std::endl;
        }
        myfile.close();
    }
}

void dynamics_pure(int N, int M, double I, double t_end, double stepsize)
{
    std::vector<double> myds = ds(N, M, I);

    std::vector<double> t_data;
    std::vector<double> alpha_data;
    for (int n = 0; n < t_end / stepsize; n++)
    {
       t_data.push_back(n * stepsize);
       alpha_data.push_back(alpha_pure_product(N, M, I, myds, n * stepsize));
    }

    std::stringstream outfile;
    outfile << "build/temperature/alpha_pure_product_N=" << N << "_M=" << M << ".txt";
    save_data(t_data, alpha_data, outfile.str());
}

void dynamics_mixed(int N, double I, double T, double t_start, double t_end, double stepsize)
{
    std::vector<double> degeneracies;
    degeneracies.push_back(1);
    std::vector<std::vector<double>> multis = multinomials(I, N);
    for (int j = 1; j <= floor(N * I); j++)
    {
        degeneracies.push_back(multis[N][j] - multis[N][j-1]);
    }

    std::vector<double> t_data;
    std::vector<double> alpha_data;
    for (int n = t_start / stepsize; n < t_end / stepsize; n++)
    {
        t_data.push_back(n * stepsize);
        alpha_data.push_back(alpha_mixed(N, degeneracies, I, T, n * stepsize));
    }

    char outfile[80];
    sprintf(outfile, "build/temperature/%.1f/alpha_mixed/alpha_mixed_N=%d_T=%.3f.txt", I, N, T);
    save_data(t_data, alpha_data, std::string(outfile));
}

void dynamics_central_field_temperature(int N, double h, double T, double step, double t_end, std::string filename)
{
	double Sbmax = N / 2.0;
	std::vector<compVec> state(N / 2 + 1);
	std::vector<double> gamma(N / 2 + 1);
	gamma[0] = 1.0;
	for (unsigned int k = 1; k < gamma.size(); k++)
		gamma[k] = (binom(N, k) - binom(N, k - 1));

	std::vector<std::vector<std::vector<std::vector<std::complex<double>>>>> eigenvects(N / 2 + 1);
	std::vector<std::vector<std::vector<double>>> eigenvals(N / 2 + 1);
	double Z = 0;

	for (unsigned int i = 0; i < state.size(); i++)
	{
		double Sb = Sbmax - i;
		state[i].resize(2 * (2 * Sb + 1));
		eigenvects[i].resize(2 * Sb + 1);
		eigenvals[i].resize(2 * Sb + 1);

		state[i][2 * Sb + 1] = gamma[i] * std::exp(-1.0 / T * (2 * Sb + h) / 2);
		Z += gamma[i] * std::exp(-1.0 / T * (2 * Sb + h) / 2);
		for (unsigned int j = 0; j < state[i].size() / 2 - 1; j++)
		{
			eigenvects[i][j].resize(2);
			eigenvals[i][j].resize(2);

			double Sbz = Sb - j;
			double a = -0.5 * (h + 2 * Sbz);
			double b = sqrt(Sb * (Sb + 1) - Sbz * (Sbz - 1));
			double c = 0.5 * (h + 2 * (Sbz - 1));
			// eigenvalues
			double ev1 = 0.5 * (a + c) + sqrt(pow((a - c) / 2.0, 2) + pow(b, 2));
			double ev2 = 0.5 * (a + c) - sqrt(pow((a - c) / 2.0, 2) + pow(b, 2));
			// eigenvectors in down, up basis
			std::vector<std::complex<double>> evec1(2);
			std::vector<std::complex<double>> evec2(2);
			evec1[0] = 1.0 / sqrt(1 + pow(b, 2) / pow(a - ev1, 2)) * (-b) / (a - ev1);
			evec1[1] = 1.0 / sqrt(1 + pow(b, 2) / pow(a - ev1, 2)) * 1;
			evec2[0] = 1.0 / sqrt(1 + pow(b, 2) / pow(a - ev2, 2)) * (-b) / (a - ev2);
			evec2[1] = 1.0 / sqrt(1 + pow(b, 2) / pow(a - ev2, 2)) * 1;
			eigenvects[i][j][0] = evec1;
			eigenvects[i][j][1] = evec2;
			eigenvals[i][j][0] = ev1;
			eigenvals[i][j][1] = ev2;

			double c1 = std::norm(evec1[1]); // |<+ Sb Sz=Sbz-0.5 alpha | up Sb Sbz-1 alpha>|^2
			double c2 = std::norm(evec2[1]); // |<- Sb Sz=Sbz-0.5 alpha | up Sb Sbz-1 alpha>|^2
			state[i][2 * Sb + 1 + j + 1] = gamma[i] * (c1 * std::exp(-1.0 / T * ev1) + c2 * std::exp(-1.0 / T * ev2));
			Z += gamma[i] * (c1 * std::exp(-1.0 / T * ev1) + c2 * std::exp(-1.0 / T * ev2));
		}
	}
	for (unsigned int i = 0; i < state.size(); i++)
	{
		for (unsigned int j = 0; j < state[i].size(); j++)
		{
			state[i][j] /= Z;
			state[i][j] = sqrt(state[i][j]); // the usual trick to avoid use of density matrices
		}
	}
	dynamics_central_field(state, eigenvects, eigenvals, h, step, t_end, filename);
}

void dynamics_central_field(std::vector<compVec> &state, std::vector<std::vector<std::vector<std::vector<std::complex<double>>>>> &eigenvects, std::vector<std::vector<std::vector<double>>> &eigenvals, double h, double step, double t_end, std::string filename)
{
	std::ofstream myfile;
	myfile.open(filename);
	if (!myfile.is_open())
		std::cerr << "could not open file " << filename << std::endl;

	std::vector<compVec> system(state.size());
	for (unsigned int i = 0; i < state.size(); i++)
		system[i] = compVec(state[i].size(), 0);

	for (int n = 0; n < t_end / step; n++)
	{
		for (unsigned int i = 0; i < system.size(); i++)
		{
			double Sb = (system[i].size() / 2.0 - 1) / 2;
			for (unsigned int j = 0; j < 2 * Sb; j++)
			{
				double Sbz = Sb - j;
				std::vector<std::complex<double>> subsys = {state[i][j], state[i][2 * Sb + 1 + j + 1]};
				// u[0] = <+ Sb Sz=Sbz-0.5 | up Sb Sbz-1> + <+ Sb Sz=Sbz-0.5 | down Sb Sbz>
				// u[1] = <- Sb Sz=Sbz-0.5 | up Sb Sbz-1> + <- Sb Sz=Sbz-0.5 | down Sb Sbz>
				std::vector<std::complex<double>> u(2);
				u[0] = std::conj(eigenvects[i][j][0][0]) * subsys[0] + std::conj(eigenvects[i][j][0][1]) * subsys[1];
				u[1] = std::conj(eigenvects[i][j][1][0]) * subsys[0] + std::conj(eigenvects[i][j][1][1]) * subsys[1];
				u[0] *= std::exp(-I * (eigenvals[i][j][0] * n * step));
				u[1] *= std::exp(-I * (eigenvals[i][j][1] * n * step));
				system[i][j]              = u[0] * eigenvects[i][j][0][0] + u[1] * eigenvects[i][j][1][0];
				system[i][j + 2 * Sb + 2] = u[0] * eigenvects[i][j][0][1] + u[1] * eigenvects[i][j][1][1];
				//std::cout << Sb << " " << Sbz << std::endl;
			}
			system[i][2 * Sb + 1] = state[i][2 * Sb + 1] * std::exp(-I * ((2 * Sb + h) / 2 * n * step));
			system[i][2 * Sb]     = state[i][2 * Sb] * std::exp(-I * ((2 * Sb - h) / 2 * n * step));
		}
		compVec RDM = reduced_DM(system);
		myfile << n * step << " ";
		myfile << RDM[0].real() << " " << RDM[0].imag() << " ";
		myfile << RDM[1].real() << " " << RDM[1].imag() << " ";
		myfile << RDM[2].real() << " " << RDM[2].imag() << " ";
		myfile << RDM[3].real() << " " << RDM[3].imag() << std::endl;
	}
	myfile.close();
}

//void dynamics_central_field_maximally_mixed(int N, double h, double step, double t_end, std::string filename)
//{
//	double Sbmax = N / 2.0;
//
//	std::vector<double> gamma(N / 2 + 1);
//	gamma[0] = 1.0;
//	for (unsigned int k = 1; k < gamma.size(); k++)
//		gamma[k] = (binom(N, k) - binom(N, k - 1));
//
//	std::vector<compVec> state_initial(N / 2 + 1);
//	std::vector<compVec> system(N / 2 + 1);
//	for (unsigned int i = 0; i < state_initial.size(); i++)
//	{
//		double Sb = Sbmax - i;
//		state_initial[i] = compVec(2 * (2 * Sb + 1), 0);
//		system[i] = compVec(2 * (2 * Sb + 1), 0);
//		for (int j = 0; j < 2 * Sb + 1; j++) 
//			state_initial[i][2 * Sb + 1 + j] = sqrt(gamma[i] / pow(2, N));
//	}
//
//	std::ofstream myfile;
//	myfile.open(filename);
//	if (!myfile.is_open())
//		std::cerr << "could not open file" << std::endl;
//
//	for (int n = 0; n < t_end / step; n++)
//	{
//		for (unsigned int i = 0; i < system.size(); i++)
//		{
//			double Sb = (system[i].size() / 2.0 - 1) / 2;
//			for (unsigned int j = 0; j < 2 * Sb; j++)
//			{
//				double Sbz = Sb - j;
//				std::vector<std::complex<double>> subsys = {state_initial[i][j], state_initial[i][2 * Sb + 1 + j + 1]};
//				double a = -0.5 * (h + 2 * Sbz);
//				double b = sqrt(Sb * (Sb + 1) - Sbz * (Sbz - 1));
//				double c = 0.5 * (h + 2 * (Sbz - 1));
//				// eigenvalues
//				double ev1 = 0.5 * (a + c) + sqrt(pow((a - c) / 2.0, 2) + pow(b, 2));
//				double ev2 = 0.5 * (a + c) - sqrt(pow((a - c) / 2.0, 2) + pow(b, 2));
//				// eigenvectors in down, up basis
//				std::vector<std::complex<double>> evec1(2);
//				std::vector<std::complex<double>> evec2(2);
//				evec1[0] = 1.0 / sqrt(1 + pow(b, 2) / pow(a - ev1, 2)) * (-b) / (a - ev1);
//				evec1[1] = 1.0 / sqrt(1 + pow(b, 2) / pow(a - ev1, 2)) * 1;
//				evec2[0] = 1.0 / sqrt(1 + pow(b, 2) / pow(a - ev2, 2)) * (-b) / (a - ev2);
//				evec2[1] = 1.0 / sqrt(1 + pow(b, 2) / pow(a - ev2, 2)) * 1;
//				// u[0] = <+ Sb Sz=Sbz-0.5 | up Sb Sbz-1> + <+ Sb Sz=Sbz-0.5 | down Sb Sbz>
//				// u[1] = <- Sb Sz=Sbz-0.5 | up Sb Sbz-1> + <- Sb Sz=Sbz-0.5 | down Sb Sbz>
//				std::vector<std::complex<double>> u(2);
//				u[0] = std::conj(evec1[0]) * subsys[0] + std::conj(evec1[1]) * subsys[1];
//				u[1] = std::conj(evec2[0]) * subsys[0] + std::conj(evec2[1]) * subsys[1];
//				u[0] *= std::exp(-I * (ev1 * n * step));
//				u[1] *= std::exp(-I * (ev2 * n * step));
//				system[i][j]              = u[0] * evec1[0] + u[1] * evec2[0];
//				system[i][j + 2 * Sb + 2] = u[0] * evec1[1] + u[1] * evec2[1];
//			}
//			system[i][2 * Sb + 1] = state_initial[i][2 * Sb + 1] * std::exp(-I * ((2 * Sb + h) / 2 * n * step));
//			system[i][2 * Sb]     = state_initial[i][2 * Sb] * std::exp(-I * ((2 * Sb - h) / 2 * n * step));
//		}
//		compVec RDM = reduced_DM(system);
//		myfile << n * step << " ";
//		myfile << RDM[0].real() << " " << RDM[0].imag() << " ";
//		myfile << RDM[1].real() << " " << RDM[1].imag() << " ";
//		myfile << RDM[2].real() << " " << RDM[2].imag() << " ";
//		myfile << RDM[3].real() << " " << RDM[3].imag() << std::endl;
//		double sum = 0;
//		for (auto a : system) 
//		{
//			for (auto b : a)
//				sum += std::norm(b);
//		}
//		std::cout << sum << std::endl;
//	}
//	myfile.close();
//}

void save_RDM(std::vector<std::vector<std::vector<std::complex<double>>>> &RDMs, std::string filename)
{
    std::ofstream myfile;
    myfile.open(filename);
    for (unsigned int i = 0; i < RDMs.size(); i++)
    {
        for (unsigned int j = 0; j < RDMs[i].size(); j++)
        {
            for (unsigned int k = 0; k < RDMs[i][j].size(); k++)
            {
                myfile << RDMs[i][j][k].real() << " " << RDMs[i][j][k].imag() << " ";
            }
        }
        myfile << std::endl;
    }
    myfile.close();
}

void extra_spin_entanglement_dynamics_mixed(int N, double step)
{
    double Sbmax = N / 2.0;
    std::vector<compVec> state_down(floor(Sbmax + 0.1) + 1);
    std::vector<compVec> state_up(floor(Sbmax + 0.1) + 1);

    // emulate mixed state bath density matrix by ket vector with appropriate coefficients a_{Sb, Sbz}
    std::vector<double> coefs(state_down.size());
    coefs[0] = sqrt(1.0 / pow(2, N));
    for (unsigned int k = 1; k < coefs.size(); k++)
        coefs[k] = sqrt((binom(N, k) - binom(N, k - 1)) / pow(2, N));

    for (unsigned int i = 0; i < state_down.size(); i++)
    {
        double Sb = Sbmax - i;
        state_down[i] = compVec(2 * (2 * Sb + 1));
        state_up[i]   = compVec(2 * (2 * Sb + 1));
        for (unsigned int j = 0; j < state_down[i].size() / 2; j++)
        {
            state_down[i][j]            = 1 / sqrt(2) * coefs[i] * std::complex<double>(1,0);
            state_up[i][j + 2 * Sb + 1] = 1 / sqrt(2) * coefs[i] * std::complex<double>(1,0);
        }
    }
     
    std::ofstream concurrencefile;
    std::stringstream RDMfilename;
    std::stringstream concurrencefilename;
    RDMfilename << "build/entanglement_loss/RDM/RDM_external_mixed_N=" << N << ".txt";
    concurrencefilename << "build/entanglement_loss/concurrence/concurrence_external_mixed_N=" << N << ".txt";
    concurrencefile.open(concurrencefilename.str());
    if (!concurrencefile.is_open())
        std::cerr << "could not open file" << std::endl;

    std::vector<std::vector<std::vector<std::complex<double>>>> RDMs(int(2 * PI / step)+1);

    for (unsigned int n = 0; n < 2 * PI / step; n++)
    {
        std::vector<std::vector<std::complex<double>>> RDM(4);
        for (int k = 0; k < 4; k++)
            RDM[k] = std::vector<std::complex<double>>(4, std::complex<double>(0,0));

        // tracing out bath states ignores all off-diagonal elements that would 
        // be there had we built a real density matrix from the ket vector above
        // --> we can use the easier ket vector time evolution
        for (unsigned int i = 0; i < state_down.size(); i++)
        {
            double Sb = Sbmax - i;
            for (unsigned int j = 0; j < state_down[i].size() / 2; j++)
            {
                // this sequence for |01> + |10>
                RDM[0][0] += state_up[i][j] * std::conj(state_up[i][j]);
                RDM[0][1] += state_up[i][j] * std::conj(state_up[i][j + 2 * Sb + 1]);
                RDM[0][2] += state_up[i][j] * std::conj(state_down[i][j]);
                RDM[0][3] += state_up[i][j] * std::conj(state_down[i][j + 2 * Sb + 1]);

                RDM[1][0] += state_up[i][j + 2 * Sb +1] * std::conj(state_up[i][j]);
                RDM[1][1] += state_up[i][j + 2 * Sb +1] * std::conj(state_up[i][j + 2 * Sb + 1]);
                RDM[1][2] += state_up[i][j + 2 * Sb +1] * std::conj(state_down[i][j]);
                RDM[1][3] += state_up[i][j + 2 * Sb +1] * std::conj(state_down[i][j + 2 * Sb + 1]);

                RDM[2][0] += state_down[i][j] * std::conj(state_up[i][j]);
                RDM[2][1] += state_down[i][j] * std::conj(state_up[i][j + 2 * Sb + 1]);
                RDM[2][2] += state_down[i][j] * std::conj(state_down[i][j]);
                RDM[2][3] += state_down[i][j] * std::conj(state_down[i][j + 2 * Sb + 1]);

                RDM[3][0] += state_down[i][j + 2 * Sb + 1] * std::conj(state_up[i][j]);
                RDM[3][1] += state_down[i][j + 2 * Sb + 1] * std::conj(state_up[i][j + 2 * Sb + 1]);
                RDM[3][2] += state_down[i][j + 2 * Sb + 1] * std::conj(state_down[i][j]);
                RDM[3][3] += state_down[i][j + 2 * Sb + 1] * std::conj(state_down[i][j + 2 * Sb + 1]);

                // this sequence for |00> + |11>
                //RDM[0][0] += state_down[i][j] * std::conj(state_down[i][j]);
                //RDM[0][1] += state_down[i][j] * std::conj(state_down[i][j + 2 * Sb + 1]);
                //RDM[0][2] += state_down[i][j] * std::conj(state_up[i][j]);
                //RDM[0][3] += state_down[i][j] * std::conj(state_up[i][j + 2 * Sb + 1]);

                //RDM[1][0] += state_down[i][j + 2 * Sb +1] * std::conj(state_down[i][j]);
                //RDM[1][1] += state_down[i][j + 2 * Sb +1] * std::conj(state_down[i][j + 2 * Sb + 1]);
                //RDM[1][2] += state_down[i][j + 2 * Sb +1] * std::conj(state_up[i][j]);
                //RDM[1][3] += state_down[i][j + 2 * Sb +1] * std::conj(state_up[i][j + 2 * Sb + 1]);

                //RDM[2][0] += state_up[i][j] * std::conj(state_down[i][j]);
                //RDM[2][1] += state_up[i][j] * std::conj(state_down[i][j + 2 * Sb + 1]);
                //RDM[2][2] += state_up[i][j] * std::conj(state_up[i][j]);
                //RDM[2][3] += state_up[i][j] * std::conj(state_up[i][j + 2 * Sb + 1]);

                //RDM[3][0] += state_up[i][j + 2 * Sb + 1] * std::conj(state_down[i][j]);
                //RDM[3][1] += state_up[i][j + 2 * Sb + 1] * std::conj(state_down[i][j + 2 * Sb + 1]);
                //RDM[3][2] += state_up[i][j + 2 * Sb + 1] * std::conj(state_up[i][j]);
                //RDM[3][3] += state_up[i][j + 2 * Sb + 1] * std::conj(state_up[i][j + 2 * Sb + 1]);
            }
        }

        RDMs[n] = RDM;

        concurrencefile << n * step << " " << concurrence(RDM) << std::endl;

        state_down = evolve(state_down, step);
        state_up   = evolve(state_up, step);
    }
    concurrencefile.close();
    save_RDM(RDMs, RDMfilename.str());
}

void extra_spin_entanglement_dynamics_eigenstate(double Sb, double Sbz, double step)
{
    compVec state_down(2 * (2 * Sb + 1), 0);
    compVec state_up(2 * (2 * Sb + 1), 0);

    state_down[Sb - Sbz]            = 1 / sqrt(2) * std::complex<double>(1,0);
    state_up[Sb - Sbz + 2 * Sb + 1] = 1 / sqrt(2) * std::complex<double>(1,0);
     
    std::ofstream concurrencefile;
    std::stringstream RDMfilename;
    std::stringstream concurrencefilename;
    RDMfilename << "build/entanglement_loss/RDM/RDM_external_Sb=" << round(Sb * 2 + 0.1) << "\\2_Sbz=" << round(Sbz * 2 + 0.1) << "\\2.txt";
    concurrencefilename << "build/entanglement_loss/concurrence/concurrence_external_Sb=" << round(Sb * 2 + 0.1) << "\\2_Sbz=" << round(Sbz * 2 + 0.1) << "\\2.txt";
    concurrencefile.open(concurrencefilename.str());
    if (!concurrencefile.is_open())
        std::cerr << "could not open file" << std::endl;

    std::vector<std::vector<std::vector<std::complex<double>>>> RDMs(int(2 * PI / step)+1);

    for (unsigned int n = 0; n < 2 * PI / step; n++)
    {
        std::vector<std::vector<std::complex<double>>> RDM(4);
        for (int k = 0; k < 4; k++)
            RDM[k] = std::vector<std::complex<double>>(4, std::complex<double>(0,0));

        for (unsigned int j = 0; j < state_down.size() / 2; j++)
        {
            // this sequence for |01> - |10>
            RDM[0][0] += state_up[j] * std::conj(state_up[j]);
            RDM[0][1] += state_up[j] * std::conj(state_up[j + 2 * Sb + 1]);
            RDM[0][2] += state_up[j] * std::conj(state_down[j]);
            RDM[0][3] += state_up[j] * std::conj(state_down[j + 2 * Sb + 1]);

            RDM[1][0] += state_up[j + 2 * Sb +1] * std::conj(state_up[j]);
            RDM[1][1] += state_up[j + 2 * Sb +1] * std::conj(state_up[j + 2 * Sb + 1]);
            RDM[1][2] += state_up[j + 2 * Sb +1] * std::conj(state_down[j]);
            RDM[1][3] += state_up[j + 2 * Sb +1] * std::conj(state_down[j + 2 * Sb + 1]);

            RDM[2][0] += state_down[j] * std::conj(state_up[j]);
            RDM[2][1] += state_down[j] * std::conj(state_up[j + 2 * Sb + 1]);
            RDM[2][2] += state_down[j] * std::conj(state_down[j]);
            RDM[2][3] += state_down[j] * std::conj(state_down[j + 2 * Sb + 1]);

            RDM[3][0] += state_down[j + 2 * Sb + 1] * std::conj(state_up[j]);
            RDM[3][1] += state_down[j + 2 * Sb + 1] * std::conj(state_up[j + 2 * Sb + 1]);
            RDM[3][2] += state_down[j + 2 * Sb + 1] * std::conj(state_down[j]);
            RDM[3][3] += state_down[j + 2 * Sb + 1] * std::conj(state_down[j + 2 * Sb + 1]);
        }

        RDMs[n] = RDM;

        concurrencefile << n * step << " " << concurrence(RDM) << std::endl;

        state_down = evolve_single(state_down, step);
        state_up   = evolve_single(state_up, step);
    }
    concurrencefile.close();
    save_RDM(RDMs, RDMfilename.str());
}

//void intra_spin_entanglement_dynamics_eigenstate(double Sb, double Sbz, double step)
//{
//    compVec state_a(2 * (2 * (Sb + 0.5) + 1));
//    compVec state_b(2 * (2 * (Sb - 0.5) + 1));
//    state_a[2 * (Sb + 0.5) + 1 + Sb - Sbz + 1] = 1.0 / sqrt(2) * 1.0 / sqrt(2 * Sb + 1) * sqrt(Sb - Sbz + 1);
//    state_b[2 * (Sb - 0.5) + 1 + Sb - Sbz]     = 1.0 / sqrt(2) * (-1.0) / sqrt(2 * Sb + 1) * sqrt(Sb + Sbz);
//    state_a[Sb - Sbz]                          = 1.0 / sqrt(2) * 1.0 / sqrt(2 * Sb + 1) * sqrt(Sb + Sbz + 1);
//    state_b[Sb - Sbz - 1]                      = 1.0 / sqrt(2) * 1.0 / sqrt(2 * Sb + 1) * sqrt(Sb - Sbz);
//
//    std::vector<compVec> RDM(4);
//    std::vector<std::vector<std::vector<std::complex<double>>>> testRDMs(2 * PI / step + 1);
//
//    std::stringstream RDMfilename;
//    std::stringstream concurrencefilename;
//    RDMfilename << "build/entanglement_loss/RDM/RDM_internal_Sb=" << round(2 * Sb) << "\\2_Sbz=" << round(2 * Sbz) << "\\2.txt";
//    concurrencefilename << "build/entanglement_loss/concurrence/concurrence_internal_Sb=" << round(2 * Sb) << "\\2_Sbz=" << round(2 * Sbz) << "\\2.txt";
//    std::ofstream concurrencefile;
//    concurrencefile.open(concurrencefilename.str());
//
//    for (unsigned int n = 0; n < 2 * PI / step; n++)
//    {
//        RDM = central_bath_RDM(state_a, state_b, Sb);
//        testRDMs[n] = RDM;
//        concurrencefile << n * step << " " << concurrence(RDM) << std::endl;
//        state_a = evolve_single(state_a, step);
//        state_b = evolve_single(state_b, step);
//    }
//    save_RDM(testRDMs, RDMfilename.str());
//    concurrencefile.close();
//}

void intra_spin_entanglement_dynamics_eigenstate(double Sb, double Sbz, double step)
{
	std::vector<std::complex<double>> state(4);
    std::vector<std::vector<std::vector<std::complex<double>>>> testRDMs(2 * PI / step + 1);

    std::stringstream RDMfilename;
    std::stringstream concurrencefilename;
    RDMfilename << "build/entanglement_loss/RDM/RDM_internal_Sb=" << round(2 * Sb) << "\\2_Sbz=" << round(2 * Sbz) << "\\2.txt";
    concurrencefilename << "build/entanglement_loss/concurrence/concurrence_internal_Sb=" << round(2 * Sb) << "\\2_Sbz=" << round(2 * Sbz) << "\\2.txt";
    std::ofstream concurrencefile;
    concurrencefile.open(concurrencefilename.str());

	double t;
	for (int n = 0; n < 2 * PI / step; n++)
	{
		t = n * step;
		state = intra_fourstate(Sb, Sbz, t);
    	std::vector<std::vector<std::complex<double>>> res(4);
    	for (unsigned int k = 0; k < 4; k++)
        	res[k] = std::vector<std::complex<double>>(4);

        res[0][0] += state[0] * std::conj(state[0]);

        res[1][1] += state[1] * std::conj(state[1]);
        res[1][2] += state[1] * std::conj(state[2]);

        res[2][1] += state[2] * std::conj(state[1]);
        res[2][2] += state[2] * std::conj(state[2]);

        res[3][3] += state[3] * std::conj(state[3]);
        testRDMs[n] = res;
        concurrencefile << n * step << " " << concurrence(res) << std::endl;
	}

    save_RDM(testRDMs, RDMfilename.str());
    concurrencefile.close();
}

void intra_spin_entanglement_dynamics_mixed(int N, double step)
{
	double Sbmax = N / 2.0;

	std::vector<double> coefs(floor(Sbmax + 0.1) + 1);
    coefs[0] = sqrt(1.0 / pow(2, N));
    for (unsigned int k = 1; k < coefs.size(); k++)
        coefs[k] = sqrt((binom(N, k) - binom(N, k - 1)) / pow(2, N));

	std::vector<std::complex<double>> state(4);
    std::vector<std::vector<std::vector<std::complex<double>>>> testRDMs(2 * PI / step + 1);

    std::stringstream RDMfilename;
    std::stringstream concurrencefilename;
    RDMfilename << "build/entanglement_loss/RDM/RDM_internal_N=" << N << ".txt";
    concurrencefilename << "build/entanglement_loss/concurrence/concurrence_internal_mixed_N=" << N << ".txt";
    std::ofstream concurrencefile;
    concurrencefile.open(concurrencefilename.str());

	double t;
	for (int n = 0; n < 2 * PI / step; n++)
	{
		std::vector<std::vector<std::vector<std::complex<double>>>> state_big(floor(Sbmax + 0.1) + 1);
		for (unsigned int i = 0; i < state_big.size(); i++)
		{
			state_big[i].resize(2 * (Sbmax - i) + 1);
			for (unsigned int j = 0; j < state_big[i].size(); j++)
				state_big[i][j].resize(4);
		}
		t = n * step;
		for (unsigned int i = 0; i < state_big.size(); i++)
		{
			double Sb = Sbmax - i;
			for (unsigned int j = 0; j < state_big[i].size(); j++)
			{
				double Sbz = Sb - j;
				state = intra_fourstate(Sb, Sbz, t);

				state[0] *= coefs[i];
				state[1] *= coefs[i];
				state[2] *= coefs[i];
				state[3] *= coefs[i];
				

				state_big[i][j][1] += state[1];
				state_big[i][j][2] += state[2];
				if (j > 0)
					state_big[i][j-1][0] += state[0];
				if (j < state_big[i].size() - 1)
					state_big[i][j+1][3] += state[3];
			}
		}

    	std::vector<std::vector<std::complex<double>>> res(4);
    	for (unsigned int k = 0; k < 4; k++)
        	res[k] = std::vector<std::complex<double>>(4);

		for (unsigned int i = 0; i < state_big.size(); i++)
		{
			for (unsigned int j = 0; j < state_big[i].size(); j++)
			{
        		res[0][0] += state_big[i][j][0] * std::conj(state_big[i][j][0]);
        		res[0][1] += state_big[i][j][0] * std::conj(state_big[i][j][1]);
        		res[0][2] += state_big[i][j][0] * std::conj(state_big[i][j][2]);
        		res[0][3] += state_big[i][j][0] * std::conj(state_big[i][j][3]);

        		res[1][0] += state_big[i][j][1] * std::conj(state_big[i][j][0]);
        		res[1][1] += state_big[i][j][1] * std::conj(state_big[i][j][1]);
        		res[1][2] += state_big[i][j][1] * std::conj(state_big[i][j][2]);
        		res[1][3] += state_big[i][j][1] * std::conj(state_big[i][j][3]);

        		res[2][0] += state_big[i][j][2] * std::conj(state_big[i][j][0]);
        		res[2][1] += state_big[i][j][2] * std::conj(state_big[i][j][1]);
        		res[2][2] += state_big[i][j][2] * std::conj(state_big[i][j][2]);
        		res[2][3] += state_big[i][j][2] * std::conj(state_big[i][j][3]);

        		res[3][0] += state_big[i][j][2] * std::conj(state_big[i][j][0]);
        		res[3][1] += state_big[i][j][2] * std::conj(state_big[i][j][1]);
        		res[3][2] += state_big[i][j][2] * std::conj(state_big[i][j][2]);
        		res[3][3] += state_big[i][j][2] * std::conj(state_big[i][j][3]);
			}
		}
        testRDMs[n] = res;
	//	if (n == 0)
	//		std::cout << concurrence(res) << std::endl;
        concurrencefile << n * step << " " << concurrence(res) << std::endl;
	}

    save_RDM(testRDMs, RDMfilename.str());
    concurrencefile.close();
}

std::vector<std::complex<double>> intra_fourstate(double Sb, double Sbz, double t)
{
	std::vector<std::complex<double>> state(4);
	auto A = [](double a_Sb, double t)
	{
		return std::exp(-I * a_Sb * t) - std::exp(I * (a_Sb + 1) * t);
	};
	auto B = [](double b_Sb, double b_Sbz, double t)
	{
		return (b_Sb - b_Sbz + 1) * std::exp(-I * b_Sb * t) + (b_Sb + b_Sbz) * std::exp(I * (b_Sb + 1) * t);
	};
	auto C = [](double Sb, double Sbz, double t)
	{
		return (Sb + Sbz + 1) * std::exp(-I * Sb * t) + (Sb - Sbz) * std::exp(I * (Sb + 1) * t);
	};
	if (Sb == 0)
	{
		state[0] = sqrt(Sb + Sbz + 1) * sqrt(Sb - Sbz) / ((2 * Sb + 1) * (2 * Sb + 2)) * B(Sb + 0.5, Sbz + 0.5, t);
		state[0] += sqrt(Sb + Sbz + 1) * sqrt(Sb - Sbz) * (Sb - Sbz + 1) / ((2 * Sb + 1) * (2 * Sb + 2)) * A(Sb + 0.5, t);
		state[1] = (Sb + Sbz + 1) / ((2 * Sb + 1) * (2 * Sb + 2)) * B(Sb + 0.5, Sbz + 0.5, t);
		state[1] += (Sb - Sbz + 1) * (Sb + Sbz + 1) / ((2 * Sb + 1) * (2 * Sb + 2)) * A(Sb + 0.5, t);
		state[2] = (Sb - Sbz + 1) * (Sb + Sbz + 1) / ((2 * Sb + 1) * (2 * Sb + 2)) * A(Sb + 0.5, t);
		state[2] += (Sb - Sbz + 1) / ((2 * Sb + 1) * (2 * Sb + 2)) * C(Sb + 0.5, Sbz - 0.5, t);
		state[3] = sqrt(Sb - Sbz + 1) * sqrt(Sb + Sbz) * (Sb + Sbz + 1) / ((2 * Sb + 1) * (2 * Sb + 2)) * A(Sb + 0.5, t);
		state[3] += sqrt(Sb - Sbz + 1) * sqrt(Sb + Sbz) / ((2 * Sb + 1) * (2 * Sb + 2)) * C(Sb + 0.5, Sbz - 0.5, t);
	}
	else
	{
		state[0] = sqrt(Sb + Sbz + 1) * sqrt(Sb - Sbz) * (1.0 / ((2 * Sb + 1) * (2 * Sb + 2)) * B(Sb + 0.5, Sbz + 0.5, t) - 1.0 / ((2 * Sb + 1) * (2 * Sb)) * B(Sb - 0.5, Sbz + 0.5, t));
		
		state[0] += sqrt(Sb + Sbz + 1) * sqrt(Sb - Sbz) * ((Sb - Sbz + 1) / ((2 * Sb + 1) * (2 * Sb + 2)) * A(Sb + 0.5, t) + (Sb + Sbz) / ((2 * Sb + 1) * (2 * Sb)) * A(Sb - 0.5, t));
		state[1] = (Sb + Sbz + 1) / ((2 * Sb + 1) * (2 * Sb + 2)) * B(Sb + 0.5, Sbz + 0.5, t) + (Sb - Sbz) / ((2 * Sb + 1) * (2 * Sb)) * B(Sb - 0.5, Sbz + 0.5, t);
		state[1] += (Sb - Sbz + 1) * (Sb + Sbz + 1) / ((2 * Sb + 1) * (2 * Sb + 2)) * A(Sb + 0.5, t) - (Sb + Sbz) * (Sb - Sbz) / ((2 * Sb + 1) * (2 * Sb)) * A(Sb - 0.5, t);
		state[2] = (Sb - Sbz + 1) * (Sb + Sbz + 1) / ((2 * Sb + 1) * (2 * Sb + 2)) * A(Sb + 0.5, t) - (Sb + Sbz) * (Sb - Sbz) / ((2 * Sb + 1) * (2 * Sb)) * A(Sb - 0.5, t);
		state[2] += (Sb - Sbz + 1) / ((2 * Sb + 1) * (2 * Sb + 2)) * C(Sb + 0.5, Sbz - 0.5, t) + (Sb + Sbz) / ((2 * Sb + 1) * (2 * Sb)) * C(Sb - 0.5, Sbz - 0.5, t);
		state[3] = sqrt(Sb - Sbz + 1) * sqrt(Sb + Sbz) * ((Sb + Sbz + 1) / ((2 * Sb + 1) * (2 * Sb + 2)) * A(Sb + 0.5, t) + (Sb - Sbz) / ((2 * Sb + 1) * (2 * Sb)) * A(Sb - 0.5, t));
		state[3] += sqrt(Sb - Sbz + 1) * sqrt(Sb + Sbz) * (1.0 / ((2 * Sb + 1) * (2 * Sb + 2)) * C(Sb + 0.5, Sbz - 0.5, t) - 1.0 / ((2 * Sb + 1) * (2 * Sb)) * C(Sb - 0.5, Sbz - 0.5, t));
	}

	state[0] *= 1 / sqrt(2);
	state[1] *= 1 / sqrt(2);
	state[2] *= 1 / sqrt(2);
	state[3] *= 1 / sqrt(2);

	return state;
}
