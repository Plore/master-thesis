#ifndef INFORMATION_H
#define INFORMATION_H
#include <Eigen/Dense>

double index_of_correlation(std::vector<std::string> &polarizations, std::vector<double> &coefs, int n_subsystems);

double string_to_code(std::string ss);

Eigen::MatrixXd trace_first_index(Eigen::MatrixXd &full);
Eigen::MatrixXd trace_last_index(Eigen::MatrixXd &full);
Eigen::MatrixXd reduced_density_matrix(Eigen::MatrixXd full, int n);
double concurrence(std::vector<std::vector<std::complex<double>>> &density_matrix);
#endif
