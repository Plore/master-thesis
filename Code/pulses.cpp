#include <iostream>
#include <cmath>
#include <vector>
#include <complex>
#include <functional>

#include "dynamics.h"

typedef std::vector<std::complex<double>> compVec;
typedef std::function<compVec(compVec&)> gate;

compVec X(compVec &inputstate)
{
    compVec down = compVec(inputstate.begin(), inputstate.begin() + inputstate.size() / 2);
    compVec up   = compVec(inputstate.begin() + inputstate.size() / 2, inputstate.end());
    up.insert(up.end(), down.begin(), down.end());
    return up;
}

compVec Y(compVec &inputstate)
{
    compVec res;
    for (unsigned int n = inputstate.size() / 2; n < inputstate.size(); n++)
    {
        res.push_back(inputstate[n] * (-I));
    }
    for (unsigned int n = 0; n < inputstate.size() / 2; n++)
    {
        res.push_back(inputstate[n] * I);
    }
    return res;
}

compVec Z(compVec &inputstate)
{
    compVec res;
    for(unsigned int n = 0; n < inputstate.size(); n++)
    {
        res.push_back(inputstate[n] * ((n < inputstate.size() / 2) ? 1.0 : -1.0));
    }
    return res;
}

compVec rot(compVec &inputstate, double angle, compVec (*Axis)(compVec&))
{
    compVec res;
    compVec temp_rot = Axis(inputstate);
    for(unsigned int n = 0; n < inputstate.size(); n++)
    {
        res.push_back(inputstate[n] * cos(angle) - I * sin(angle) * temp_rot[n]);
    }
    return res;
}

compVec measure_Z(compVec &inputstate)
{
    compVec res;
    for(unsigned int n = 0; n < inputstate.size(); n++)
    {
        res.push_back((n < inputstate.size() / 2) ? inputstate[n] : 0);
    }
    return res;
}

std::vector<compVec> sequence(compVec &initial, std::vector<gate> &actions, std::vector<double> &times, double step_t)
{
    if (actions.size() != times.size())
        std::cerr << "unequal lengths" << std::endl;
    std::vector<compVec> res;
    compVec state = initial;
    res.push_back(state);
    for (unsigned int n = 0; n < times.size(); n++)
    {
        for (int m = 0; m < (times[n] - ((n == 0) ? 0 : times[n-1])) / step_t; m++)
        {
            state = evolve_single(state, step_t);
            res.push_back(state);
        }
        state = actions[n](state);
        res.push_back(state);
    }
    for (int m = 0; m < (PI - times[times.size()-1]) / step_t; m++)
    {
        state = evolve_single(state, step_t);
        res.push_back(state);
    }
    return res;
}
