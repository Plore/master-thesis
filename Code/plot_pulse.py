import numpy as np
import matplotlib.pyplot as plt

states = np.genfromtxt("pulsetest.txt", unpack=True)
states_real = states[::2]
states_imag = states[1::2]
states_modsquare = states_real**2 + states_imag**2
Sb = (len(states_real) / 2 - 1) / 2
ts = np.linspace(0, np.pi, len(states_real[0]))
colors = plt.get_cmap("jet")(np.linspace(0,1,len(states_real)))
#for n in range(len(states_real)):
#    plt.plot(ts, states_real[n]**2 + states_imag[n]**2, color=colors[n], label="$|{}>, S_b^z={}$".format(int(n / (2 * Sb + 1)), Sb - (n % (2 * Sb + 1))))

up   = sum(np.array([states_real[n]**2 + states_imag[n]**2 for n in range(int(2 * Sb + 1))]))
down = sum(np.array([states_real[n]**2 + states_imag[n]**2 for n in range(int(2 * Sb + 1), int(2 * (2 * Sb + 1)))]))

plt.plot(ts, up, label=r"$\left|\langle\Uparrow|\Psi(t)\rangle\right|^2$")
plt.plot(ts, down, label=r"$\left|\langle\Downarrow|\Psi(t)\rangle\right|^2$")

plt.legend(loc="upper right", bbox_to_anchor=(1.1, 1.1))
plt.savefig("pulsetest.pdf")
plt.clf()
plt.imshow(states_modsquare, interpolation="nearest", aspect=0.1, cmap="Blues_r", extent=(0,np.pi,0,len(states_real)), origin="lower")
plt.colorbar()
plt.savefig("pulses_imshow.pdf")
