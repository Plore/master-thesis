#ifndef CLEBSCHGORDAN_H
#define CLEBSCHGORDAN_H
#include <Eigen/Dense>

// calculate specific CG coefficient <J, mj, L, I,|L, mj-mi, I, mi>
Eigen::VectorXd clebsch(double J, double mj, double J1, double J2);

Eigen::VectorXd alphas(double J1, double J2, double mj1);

std::vector<double> ds(int N, int M, double I);

#endif
