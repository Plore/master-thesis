#include <iostream>
#include <cmath>
#include <vector>
#include <complex>

#include "dynamics.h" 

std::complex<double> corr_xx(double Sb, double Sbz, double h, double t)
{
    return 1.0 / (4 * pow(2 * Sb + 1, 2)) * 
           ((Sb - Sbz + 1) * (Sb + Sbz + 1) * std::exp(-I * h * t) 
          + (Sb - Sbz) * (Sb + Sbz) * std::exp(-I * h * t) 
          + (Sb - Sbz + 1) * (Sb - Sbz) * std::exp(I * (2 * Sb + 1 - h) * t) 
          + (Sb + Sbz) * (Sb + Sbz + 1) * std::exp(-I * (2 * Sb + 1 + h) * t));
}

std::complex<double> corr_yx(double Sb, double Sbz, double h, double t)
{
    return I * corr_xx(Sb, Sbz, h, t);
}

std::complex<double> corr_zz(double Sb, double Sbz, double h, double t)
{
    return 1.0 / (4 * pow(2 * Sb + 1, 2)) * (pow(-2 * Sbz + 1, 2) + 4 * (Sb - Sbz + 1) * (Sb + Sbz) * std::cos((2 * Sb + 1) * t));
}

std::complex<double> corr(int N, std::complex<double> (*corr_type)(double, double, double, double), std::vector<double> &degs, double a, double h, double t)
{
    double gamma = a / N;
    std::complex<double> res = 0;
    double min_J = 1.0 / 2 * N - (int)(1.0 / 2 * N);
    double J = N * 1.0 / 2;
    for (int k = 0; J >= min_J; k++)
    {
        J = N * 1.0 / 2 - k;
        double mj = -J;
        for (int l = 0; l <= (int)(J * 2); l++)
        {
            mj = -J + l;
            res += corr_type(J, mj, h, t) * degs[k] * exp(-gamma * pow(mj, 2));
        }
    }
    double Z = 0;
    for (int j = 0; j <= N; j++)
        Z += binom(N, j) * exp(-gamma / 4 * pow((N - 2 * j), 2));
    return res / Z;
}
