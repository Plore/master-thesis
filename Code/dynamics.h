#ifndef DYNAMICS_H
#define DYNAMICS_H

const std::complex<double> I(0,1);
const double PI = 4 * atan(1);
typedef std::vector<std::complex<double>> compVec;
typedef std::vector<std::vector<std::vector<std::complex<double>>>> wholeState;

// utility
double factorial(int n);

double binom(int n, int m);

std::vector<std::vector<double>> multinomials(double I, int limit);


// tools for handling arbitrary product states

std::vector<int> polarization(int code, int length);

// given binary state code, build vector in Sb eigenbasis
std::vector<std::vector<double>> buildstate(std::string polarization);

// dynamics of bath and system in quantum numbers Sb (+additional), Sbz and S0z
wholeState state_decomposition(int N, std::vector<std::complex<double>> &central, std::vector<std::string> &polarizations, std::vector<double> &bath_coefs);

double norm(wholeState &system);

void print_system(wholeState &system);

// evolution of states with different sets of quantum numbers
void evolve_whole(wholeState &system, double t);

std::vector<compVec> evolve(std::vector<compVec> &state, double t);

compVec evolve_single(compVec &state, double t);

compVec reduced_DM_whole(wholeState &system);

compVec reduced_DM(std::vector<compVec> &pure_state);

compVec reduced_DM_single(compVec &pure_state);

std::vector<compVec> central_bath_RDM(compVec &state_a, compVec &state_b, double Sb);


// the simple cases considered in BS07 and generalizations to bath spin I and mixed states
// relevant entry of central spin RDM for pure states
double alpha_pure_entangled(double Sb, double Sbz, double t);

double alpha_pure_product(int N, int M, double I, std::vector<double> &ds, double t);

// relevant entry of central spin RDM for various mixed states
double alpha_mixed_maximal(int N, std::vector<double> &degs, double I, double t);

double alpha_mixed(int N, std::vector<double> &degs, double I, double T, double t);

double alpha_punishment(int N, std::vector<double> &degs, double I, double a, double t);

// convenience functions
void save_data(std::vector<double> &t_data, std::vector<double> &alpha_data, std::string filename);

void dynamics_pure(int N, int M, double I, double t_end, double stepsize);

void dynamics_mixed(int N, double I, double T, double t_start, double t_end, double stepsize);


// central field
// helper function that constructs thermal state and calls dynamics_central_field
void dynamics_central_field_temperature(int N, double h, double T, double step, double t_end, std::string filename);

void dynamics_central_field(std::vector<compVec> &state, std::vector<std::vector<std::vector<std::vector<std::complex<double>>>>> &eigenvects, std::vector<std::vector<std::vector<double>>> &eigenvals, double h, double step, double t_end, std::string filename);

void dynamics_central_field_maximally_mixed(int N, double h, double step, double t_end, std::string filename);
void save_RDM(std::vector<std::vector<std::vector<std::complex<double>>>> &RDM, std::string filename);


// examine loss of two-spin entanglement
void extra_spin_entanglement_dynamics_eigenstate(double Sb, double Sbz, double step);

void extra_spin_entanglement_dynamics_mixed(int N, double step);

void intra_spin_entanglement_dynamics_eigenstate(double Sb, double Sbz, double step);

void intra_spin_entanglement_dynamics_mixed(int N, double step);

std::vector<std::complex<double>> intra_fourstate(double Sb, double Sbz, double t);

#endif
