import numpy as np
from time import sleep
from math import factorial
from scipy.special import binom
from numpy.linalg import svd
import matplotlib.pyplot as plt

def nullspace(A, atol=1e-13, rtol=0):
    """from scipy cookbook"""
    A = np.atleast_2d(A)
    u, s, vh = svd(A)
    tol = max(atol, rtol * s[0])
    nnz = (s >= tol).sum()
    ns = vh[nnz:].conj().T
    return ns

def C(l, q):
    return np.sqrt(l * (l + 1) - q * (q - 1))

def clebsch(J, mj, I, i, l, a0):
    prodI = lambda k: np.prod([C(I, I - q) for q in range(k, i - 1 + 1)])
    prodl = lambda k: np.prod([C(l, J - I + q) for q in range(int(k - (J - mj) + 1 + (i - k)), k + 1)])
    bc    = lambda k: binom(J - mj, i - k)
    res = 0
    for k in range(0, i + 1):
        if a0[int(I + l - J)][k] != 0:
            res += a0[int(I + l - J)][k] * prodI(k) * prodl(k) * bc(k)
    return res
   # return sum([a0[i][k] * prodI(k) * prodl(k) * bino(k) for k in range(0, i+1)])

def get_clebsch(l, I):
    a0 = np.zeros(shape=(2 * I + 1, 2 * I + 1))
    cg = np.zeros(shape=((I + l) + 1, 2 * I + 1, 2 * I + 1))
    cg[0][0][0] = 1 # |l+I,l+I> = |l,l>|I,I>
    a0[0][0] = 1
    for k in range(1, int(2 * I + 1)):
        mj = I + l - k
        for n in range(0, k):
            J = I + l - n
            for i in range(0, k + 1):
 #               print("{} {} {} {} {} - > {}".format(J, mj, I, i, l, clebsch(J, mj, I, i, l, a0)))
                cg[k][n][i] = clebsch(J, mj, I, i, l, a0)
            cg[k][n] /= np.sqrt(sum([x**2 for x in cg[k][n]])) # normalization
        a0[k] = np.append(nullspace(cg[k,0:n+1,0:k+1]).T[0], np.zeros(2 * I + 1 - (k + 1)))# orthogonality
 #       print(a0[k])
        for i in range(0, k + 1):
            cg[k][k][i] = clebsch(I + l - k, mj, I, i, l, a0)
        cg[k][k] /= np.sqrt(sum([x**2 for x in cg[k][k]])) # normalization

    for k in range(int(2 * I + 1), int(I + l) + 1):
        mj = I + l - k
        for n in range(int(2 * I + 1)):
            J = I + l - n
            for i in range(int(2 * I + 1)):
                cg[k][n][i] = clebsch(J, mj, I, i, l, a0)
            cg[k][n] /= np.sqrt(sum([x**2 for x in cg[k][n]]))
    return cg

# get all coefficients <J,Sbz-I,Sb,I|Sb,Sbz,I,-I>
def get_alphas(Sb, Sbz, I):
   # print("{} {} {}".format(Sb, Sbz, I))
    assert Sb >= Sbz
    return get_clebsch(Sb, I)[2 * I + (Sb - Sbz), :, -1]

def get_ds(N, M, I):
    # alphas[i][j] contains all CG to reach states from Sbz = (N - M - i) * I, Sb = (N - M + i) * I - j
    alphas = []
    for i in range(0, M):
        alphas.append([])
        Sbz = (N - M - i) * I
        for j in range(0, int(2 * i * I) + 1):
            Sb = (N - M + i) * I - j
            alphas[i].append(get_alphas(Sb, Sbz, I))

    d = []
    for m in range(0, M + 1):
        # no need for extra dimension correspondin to N - x, since N - x and M - x are coupled
        d.append(np.zeros(int(2 * m * I + 1)))
        d[m][0] = np.prod([(get_alphas((N - M + r) * I, (N - M - r) * I, I)[0])**2 for r in range(0, m)])

    for m in range(0, M + 1):
        for k in range(1, int(2 * m * I) + 1):
            for kp in range(int(k - 2 * I), k + 1):
                if kp < 0 or kp > 2 * (m - 1) * I:
                    d[m][k] += 0
                else:
                    d[m][k] += d[m-1][kp] * (alphas[m-1][kp][k-kp])**2
    return d

def d_simple(k, N, M):
    return factorial(M - 1) * factorial(N - M) * (N - 2 * k) / factorial(N - k) / factorial(k)

def d_simple_left(k, N, M):
    return (M - k) / (N - 2 * k) * factorial(M - 1) * factorial(N - M) * (N - 2 * k) / factorial(N - k) / factorial(k)

def d_simple_right(k, N, M):
    return (N - M - k) / (N - 2 * k) * factorial(M - 1) * factorial(N - M) * (N - 2 * k) / factorial(N - k) / factorial(k)

#N = 21
#M = 10

# reduced time-dependent density matrix for spin 1/2 in bath of spin I with M down, N - M up spins
def beta(t, N, M, I, ds):
    return np.sum([ds[k] * 1 / (2 * (N * I - k) + 1)**2 * 2 * (2 * M * I - k + 1) * (2 * N * I - 2 * M * I - k) * (1 - np.cos((2 * (N * I - k) + 1) * t)) for k in range(0, int(2 * M * I + 1))])

def beta_paper(t, N, M):
    return 2 * np.sum([d_simple(k, N, M) / (N - 2 * k)**2 * (M - k) * (N - M - k) * (1 - np.cos((N - 2 * k) * t)) for k in range(0, M)])

# necessary for calculating degeneracy of |Sb, Sbz> w.r.t. product basis when examining mixed states
def quadrinomials(n):
    res = [[1]]
    for i in range(1, n + 1):
        res.append([0] * (3 * i + 1))
        for j in range(3 * i + 1):
            for k in range(max(j - 3, 0), min(3 * (i-1), j) + 1):
                res[i][j] += res[i-1][k]
    return res

# known exact result from BS07
def alpha_pure_test(Sb, Sbz, t):
    return 2 * (Sb - Sbz + 1) * (Sb + Sbz) / (2 * Sb + 1)**2 * (1 - np.cos((2 * Sb + 1) * t))

# mixed state is a statistical ensemble of pure states
# in the maximally mixed state, weights are just the degeneracies of the eigenstates
def alpha_mixed_test(N, I, degs, t):
    J = np.arange(I * N, I * N - int(I * N) - 1, -1)
    res = 0
    for j in range(len(J)):
        for mj in np.arange(-J[j], J[j] + 1):
            res += alpha_pure_test(J[j], mj, t) * degs[j]

    res /= (2 * I + 1)**N
    return res

# state with temperature-dependent averaging
def alpha_mixed(N, I, degs, T, t):
    J = np.arange(I * N, I * N - int(I * N) - 1, -1)
#    p = np.arange(np.exp(
    res = 0
    Z = 0
    for j in range(len(J)):
        for mj in np.arange(-J[j], J[j] + 1):
            res += alpha_pure_test(J[j], mj, t) * degs[j] * np.exp(-1 / T * (-J[j] - 1)) # S0 antiparallel to Sb
            res += alpha_pure_test(J[j], mj, t) * degs[j] * np.exp(-1 / T * J[j]) # S0 parallel to Sb
            Z += degs[j] * np.exp(-1 / T * (-J[j] - 1))
            Z += degs[j] * np.exp(-1 / T * J[j])

    return res / Z


def alpha_punishment(N, degs, I, a, t):
    alpha = a / N
    J = np.arange(I * N, I * N - int(I * N) - 1, -1)
    res = 0
    Sbzq = 0
    for j in range(len(J)):
        for mj in np.arange(-J[j], J[j] + 1):
            #print("{} {} {} {}".format(J[j], mj, alpha_pure_test(J[j], mj, t), degs[j]))
            res += alpha_pure_test(J[j], mj, t) * degs[j] * np.exp(-alpha * mj**2)

    Z = np.sum([binom(N, j) * np.exp(-alpha / 4 * (N - 2 * j)**2) for j in range(0, N+1)])
    #print(Z)
    res /= Z
    return res

def S_central_z_punishment(N, degs, I, a, t):
    gamma = a / N
    J = np.arange(I * N, I * N - int(I * N) - 1, -1)
    res = 0
    Sbzq = 0
    for j in range(len(J)):
        for mj in np.arange(-J[j], J[j] + 1):
            res += - 1 / 2 * ((1 - alpha_pure_test(J[j], mj, t)) * degs[j] * np.exp(-gamma * mj**2) - alpha_pure_test(J[j], mj, t) * degs[j] * np.exp(-gamma * (mj - 1)**2))

    Z = np.sum([binom(N, j) * np.exp(-gamma / 4 * (N - 2 * j)**2) for j in range(0, N+1)])
    res /= Z
    return res


#########################
########  tests  ########
#########################

# comparison with exact results of Bortz and Stolze for I=1/2 (pure states)

I = 1/2

for M in [5, 10, 20, 50]:
    N = 2 * M + 1
    print("I = {}  N = {}  M = {}".format(I, N, M))
    ds = get_ds(N-1, M-1, I)[-1]

    ts = np.linspace(0, np.pi, 1000)

    betas_paper   = np.array([beta_paper(t, N, M) for t in ts])
    betas_program = np.array([beta(t, N-1, M-1, I, ds) for t in ts])

    sz_paper   = -0.5 * (1 - 2 * betas_paper)
    sz_program = -0.5 * (1 - 2 * betas_program)
    ee_paper   = -betas_paper * np.log2(betas_paper) - (1 - betas_paper) * np.log2(1 - betas_paper)
    ee_program = -betas_program * np.log2(betas_program) - (1 - betas_program) * np.log2(1 - betas_program)

    with open("build/comparisons/sz_I=0.5_M={}_N=2M+1.txt".format(M), "w") as outfile:
        for i in range(len(ts)):
            outfile.write("{} {} {}\n".format(ts[i], sz_paper[i], sz_program[i]))
    with open("build/comparisons/ee_I=0.5_M={}_N=2M+1.txt".format(M), "w") as outfile:
        for i in range(len(ts)):
            outfile.write("{} {} {}\n".format(ts[i], ee_paper[i], ee_program[i]))


for M in [5, 10, 20, 50]:
    N = 2 * M
    print("I = {}  N = {}  M = {}".format(I, N, M))
    ds = get_ds(N-1, M-1, I)[-1]

    ts = np.linspace(0, np.pi, 1000)

    betas_paper   = np.array([beta_paper(t, N, M) for t in ts])
    betas_program = np.array([beta(t, N-1, M-1, I, ds) for t in ts])

    sz_paper   = -0.5 * (1 - 2 * betas_paper)
    sz_program = -0.5 * (1 - 2 * betas_program)
    ee_paper   = -betas_paper * np.log2(betas_paper) - (1 - betas_paper) * np.log2(1 - betas_paper)
    ee_program = -betas_program * np.log2(betas_program) - (1 - betas_program) * np.log2(1 - betas_program)

    with open("build/comparisons/sz_I=0.5_M={}_N=2M.txt".format(M), "w") as outfile:
        for i in range(len(ts)):
            outfile.write("{} {} {}\n".format(ts[i], sz_paper[i], sz_program[i]))
    with open("build/comparisons/ee_I=0.5_M={}_N=2M.txt".format(M), "w") as outfile:
        for i in range(len(ts)):
            outfile.write("{} {} {}\n".format(ts[i], ee_paper[i], ee_program[i]))


#########################
### try some mixed states
#
#A = 2
#N = 9
##
#########################
## punishment in Sbz
#
#a = 1
#
##degs = np.insert(np.ediff1d(binoms[N][:int(3 * N / 2) + 1]), 0, 1)
#degs = np.insert([binom(N, j) - binom(N, j-1) for j in range(1, int(N / 2) + 1)], 0, 1)
##print(degs)
##print(alpha_punishment(N, degs, 1/2, a, 1.0))
#ts = np.arange(0, np.pi, 0.01)
##szs = np.array(S_zero_t_punishment for t in ts)
#szs = np.array([-0.5 * (1 - 2 * alpha_punishment(N, degs, 1/2, a, t)) for t in ts])
#with open("res/S0z_N={}_a={:1.1f}.txt".format(N, a), "w") as outfile:
#    for t, sz in zip(ts, szs):
#        outfile.write("{} {}\n".format(t, sz))
#plt.plot(ts, szs, 'b-')
#plt.savefig("dynamics_punishment_N={}_a={}.pdf".format(N, a))
#plt.clf()
##print(S_central_z_punishment(N, degs, 1/2, a, 0.5))


#degs = np.insert([binom(N, j) - binom(N, j-1) for j in range(1, int(N / 2) + 1)], 0, 1)
#Sbzq_data = []
#a_data = np.arange(0, 100, 0.1)
#for a in a_data:
#    Sbzq_data.append(alpha_punishment(N, degs, 1/2, a, 0))
#
#with open("Sbzq_data.txt", "w") as outfile:
#    for i in range(len(a_data)):
#        outfile.write("{:.1f} {}\n".format(a_data[i], Sbzq_data[i]))
#
#plt.plot(np.arange(0, 100, 0.1), Sbzq_data)
#plt.savefig("Sbzq.pdf")
#print("{:.10f}".format(Sbzq_data[0]))

#################################
## comparison of short-time dynamics w.r.t. different N-dependent time scales
#
#ts = np.linspace(0,5,100)
#
#fig = plt.figure(figsize=(8,8))
#ax1 = fig.add_subplot(211)
#ax2 = fig.add_subplot(212)
#
#for N in range(2, 7):
#    degs = np.insert(np.ediff1d(quads[N][:int(3 * N / 2) + 1]), 0, 1)
#
#    # time scale 1 is determined by A
#    szs1 = [0.5 * (1 - 2 * alpha_mixed_test(N, degs, t / A)) for t in ts]
#    ax1.plot(ts, szs1, label="N = {}".format(N))
#
#    # time scale 2 is determined by T = 1 / A / sqrt(N)
#    T = 1 / A / np.sqrt(N)
#    print(T)
#    szs2 = [0.5 * (1 - 2 * alpha_mixed_test(N, degs, t * T)) for t in ts]
#    ax2.plot(ts, szs2, label="N = {}".format(N))
#    with open("szdata{}.txt".format(N), "w") as outfile:
#        for i in range(len(ts)):
#            outfile.write("{} {}\n".format(ts[i], szs2[i]))
#
#ax1.set_ylim(-0.1, 0.6)
#ax2.legend(bbox_to_anchor=(1.12,1.5))
#fig.savefig("test4.pdf")


# plot long-time-dynamics w.r.t. timescale T = 1 / A / sqrt(N)
#
#for N in [3, 5, 7, 11]:
#    ts = np.arange(0, 20, 0.01)
#    T = 1 / A / np.sqrt(N)
#    print(T)
#    degs = np.insert(np.ediff1d(quads[N][:int(3 * N / 2) + 1]), 0, 1)
#    alphas = np.array([alpha_mixed_test(N, degs, t * T) for t in ts])
#    szs = 0.5 * (1 - 2 * alphas)
#    with open("szdata{}_long.txt".format(N), "w") as outfile:
#        for i in range(len(ts)):
#            outfile.write("{} {}\n".format(ts[i], alphas[i]))
#

# more tests at normal timescale

#N = 271
#quads = quadrinomials(N)
#degs = np.insert([binom(N, j) - binom(N, j-1) for j in range(1, int(N / 2) + 1)], 0, 1)
##degs = np.insert(np.ediff1d(quads[N][:int(3 * N / 2) + 1]), 0, 1)
#print(degs)
#I = 1.0 / 2
#ts = np.arange(0, 3.1416, 0.01)
#alphas = np.array([alpha_mixed_test(N, I, degs, t) for t in ts])
#szs = 0.5 * (1 - 2 * alphas)
#with open("res/S0z_mixed_maximal_I={}_N={}.txt".format(I, N), "w") as outfile:
#    for i in range(len(ts)):
#        outfile.write("{:.02f} {}\n".format(ts[i], alphas[i]))

################################
# Temperature dependence

#N = 7
#I = 1/2
#quads = quadrinomials(N)
#for T in [0.1, 0.2, 0.3, 0.4, 0.5, 1.0, 2.0, 5.0]:
#    ts = np.arange(0, np.pi, 0.01)
#    degs = np.insert(np.ediff1d(quads[N][:int(3 * N / 2) + 1]), 0, 1)
##    degs = np.insert([binom(N, j) - binom(N, j-1) for j in range(1, int(N / 2) + 1)], 0, 1)
#    alpha_mixed(N, I, degs, T, 0)
#    szs = [0.5 * (1 - 2 * alpha_mixed(N, I, degs, T, t)) for t in ts]
#    plt.plot(ts, szs)
#    plt.savefig("temperature_dynamics_N={}_T={}.pdf".format(N, T))
#    plt.clf()
