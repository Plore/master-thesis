#include <iostream>
#include <iomanip>
#include <vector>
#include <sstream>
#include <fstream>
#include <complex>
#include <cmath>
#include <string>
#include <bitset>

#include "clebschgordan.h"
#include "dynamics.h"
//#include "corr.h"
#include "pulses.h"
#include "information.h"
#include "bath_entanglement.h"

int main() {

	// dynamics with external magnetic field on central spin

	double step = 0.01;

	// many h values for average height dependence
	std::vector<double> hs2 = {0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9};
	for (int i = 0; i < 9 / 0.2; i++)
		hs2.push_back(1.0 + i * 0.2);
	for (int h = 10; h < 50; h+=2)
		hs2.push_back(h);
	for (int h = 50; h <= 200; h+=5)
		hs2.push_back(h);

	// fewer h values for temperature dependence
	std::vector<double> hs = {1,2,5,10,20,50,100,200,500};

	std::vector<double> Ts1 = {0.1, 0.2, 0.5, 1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0, 1000, 1000000};
	std::vector<double> Ts2 = {0.1, 0.2, 0.3, 0.4, 0.6, 0.8, 1.2, 1.6, 2.4, 3.2, 4.8, 6.4, 9.6, 12.8, 19.2, 25.6, 38.4, 51.2, 76.8, 102.4, 153.6, 204.8, 307.2, 409.6};
	std::vector<int> Ns = {5,10,20,50};
	for (unsigned int n = 0; n < Ns.size(); n++)
	{
		int N = Ns[n];
		std::cout << "N = " << N << std::endl;
		for (unsigned int i = 0; i < hs.size(); i++)
		{
			double h = hs[i];
			int periods = 10;
			std::cout << h << std::endl;

			for (unsigned int m = 0; m < Ts1.size(); m++)
			{
				std::stringstream filename;
				filename << "build/central_field/RDM/central_field_RDM_N=" << N << "_h=" << std::setprecision(1) << std::fixed << h << "_T=" << Ts1[m] << ".txt";
				dynamics_central_field_temperature(N, h, Ts1[m], step, periods * PI, filename.str());
			}
			for (unsigned int m = 0; m < Ts2.size(); m++)
			{
				std::stringstream filename;
				filename << "build/central_field/RDM/central_field_RDM_N=" << N << "_h=" << std::setprecision(1) << std::fixed << h << "_T=" << Ts2[m] << ".txt";
				dynamics_central_field_temperature(N, h, Ts2[m], step, periods * PI, filename.str());
			}
		}
		for (unsigned int i = 0; i < hs2.size(); i++)
		{
			double h = hs2[i];
			// for evaluating average height at maximally mixed state
			std::stringstream filename;
			filename << "build/central_field/RDM/central_field_RDM_N=" << N << "_h=" << std::setprecision(1) << std::fixed << h << ".txt";
			dynamics_central_field_temperature(N, h, 1000000, 0.01, 10 * PI, filename.str());
		}
	}


    // entanglement loss of central spin

    double step = 0.01;

    for (int N = 2; N < 70; N++)
    {
        std::cout << "N = " << N << std::endl;

        // entanglement with a single bath spin; rest of bath is an Sb, Sbz eigenstate
        double Sb = N / 2.0;
        double Sbz;
        for (int j = N - 0; j >= -(N - 0); j-=2)
        {
            Sbz = j / 2.0;
            intra_spin_entanglement_dynamics_eigenstate(Sb, Sbz, step);
        }
        // entanglement with a bath spin, rest of bath in maximally mixed state
		intra_spin_entanglement_dynamics_mixed(N, step);

        // entanglement with an external spin, bath in Sb, Sbz eigenstate
        Sb = N / 2.0;
        for (int j = N; j >= -(N); j-=2)
        {
            Sbz = j / 2.0;
            extra_spin_entanglement_dynamics_eigenstate(Sb, Sbz, step);
        }
        // entanglement with an external spin, bath in maximally mixed state
        extra_spin_entanglement_dynamics_mixed(N, step);
    }


	// investigate effect of initial bath entanglement on central spin decoherence
	std::ofstream indexfile;
	indexfile.open("build/bath_entanglement/entanglement_correlations_ghz.txt");
	for (int f = 0; f < 50; f++)
	{
		double a = 0 + f / 50.0;
		double b = 1 - f / 50.0;
		std::vector<double> coefs = {a, b};
		std::string poldown(9, '0');
		std::string polup(9, '1');
		std::vector<std::string> pols = {poldown, polup};
		double i = index_of_correlation(pols, coefs, 9);
		indexfile << f << " " << i << std::endl;
		dynamics_GHZ(9, f, 0.01, (9+1) * PI);
	}
	indexfile.close();

	/* less efficient method

    for (int N = 1; N < 15; N++)
	{
		std::cout << N << std::endl;
		dynamics_W(N, 0.005, 10 * PI / (N + 1));
		dynamics_GHZ(N, 25, 0.005, 10 * PI / (N + 1));
	}
	*/

	for (int N = 2; N <= 100; N++)
	{
		std::cout << N << std::endl;
		dynamics_GHZ_sym_largeN(N, 0.001, 10 * PI / (N + 1));
		dynamics_W_largeN(N, 0.001, 10 * PI / (N + 1));
	}

	/* experimental stuff

	dynamics_experimental(0.01);

	dynamics_pairwise(0.01);

	for (int n = 0; n <= N; n++)
	{
		std::string pol = std::bitset<N>(pow(2, n) - 1).to_string();
		// std::reverse(pol.begin(), pol.end());
		std::cout << pol << std::endl;
		pols.push_back(pol);
	}
	pols.push_back(empty);
	std::vector<std::string> pols_copy;
	for (int i = 0; i < N / 2; i++)
	{
		pols_copy.clear();
		while(!pols.empty())
		{
			std::string pol = pols.back();
			pols.pop_back();
			std::string pol_copy(pol);
			pol.append("01");
			pol_copy.append("10");
			pols_copy.push_back(pol);
			pols_copy.push_back(pol_copy);
		}
		pols = pols_copy;
	}
	for (auto pol:pols)
		std::cout << pol << std::endl;

	*/


    /*
     * correlations test
     *

    int N = 49;
    //double h = 0.1;
    //double a = 0;

    double stepsize = 0.01;
    const double PI = 4 * atan(1);

    std::vector<double> degs;
    degs.push_back(1);
    for (int j = 1; j <= N / 2; j++)
    {
        degs.push_back(binom(N, j) - binom(N, j - 1));
    }

    for (double a : {0, 1, 10})
    {
        for (double h : {0, 1, 2, 5})
        {
            std::ofstream myfile;
            char outfile[50];
            sprintf(outfile, "corrtest_a=%.1f_h=%.1f.txt", a, h);
            myfile.open(std::string(outfile));

            if (myfile.is_open())
            {
                for(int n = 0; n < 2 * PI / stepsize; n++)
                {
                    std::complex<double> val = corr(N, corr_xx, degs, a, h, stepsize * n);
                    myfile << n * stepsize << " " << val.real() << " " << val.imag() << std::endl;
                }
            }
        }
    }
    */


    // temperature dependence of central spin polarization

	// spin 3/2

    double I = 3.0/2;
    double stepsize_t;
    double t_start = 0;
    double t_end = 3.14159;

    int N_max = 40;

    for (int N = 2; N < N_max; N+=1)
    {
        std::cout << N << std::endl;
        for (int m = 0; m < (int)((0.200 - 0.050) / 0.005); m++)
        {
            double T = 0.050 + 0.005 * m;
            stepsize_t = 0.02 / N * T;
            std::cout << T << std::endl;
            dynamics_mixed(N, I, T, t_start, t_end, stepsize_t);
        }
        for (int m = 0; m < (int)((1.000 - 0.200) / 0.005); m++)
        {
            double T = 0.200 + 0.005 * m;
            stepsize_t = 0.01 / N;
            std::cout << T << std::endl;
            dynamics_mixed(N, I, T, t_start, t_end, stepsize_t);
        }
        for (int m = 0; m < (int)((50.000 - 1.000) / 0.100); m++)
        {
            double T = 1.000 + 0.100 * m;
            stepsize_t = 0.1 / N;
            std::cout << T << std::endl;
            dynamics_mixed(N, I, T, t_start, t_end, stepsize_t);
        }
    }
	// asymptotic temperature
	for (int N = 1; N < 150; N++)
	{
		std::cout << N << std::endl;
		dynamics_mixed(N, I, 10000, t_start, t_end, 0.01);
	}


	// spin 1/2

	double I = 1.0/2;
	double stepsize_t;
	double t_start = 0;
	double t_end = 3.14159;

	int N_max = 40;

	for (int N = 2; N < N_max; N+=1)
	{
		std::cout << N << std::endl;
		for (int m = 0; m < (int)((0.200 - 0.050) / 0.005); m++)
		{
			double T = 0.050 + 0.005 * m;
			stepsize_t = 0.02 / N * T;
			std::cout << T << std::endl;
			dynamics_mixed(N, I, T, t_start, t_end, stepsize_t);
		}
		for (int m = 0; m < (int)((1.000 - 0.200) / 0.005); m++)
		{
			double T = 0.200 + 0.005 * m;
			stepsize_t = 0.05 / N;
			std::cout << T << std::endl;
			dynamics_mixed(N, I, T, t_start, t_end, stepsize_t);
		}
		for (int m = 0; m < (int)((50.000 - 1.000) / 0.100); m++)
		{
			double T = 1.000 + 0.100 * m;
			stepsize_t = 0.1 / N;
			std::cout << T << std::endl;
			dynamics_mixed(N, I, T, t_start, t_end, stepsize_t);
		}
	}
	// asymptotic temperature
	for (int N = 1; N < 150; N++)
	{
		std::cout << N << std::endl;
		dynamics_mixed(N, I, 10000, t_start, t_end, 0.01);
	}


    /*
     * "punishment" dynamics
     *

    double I = 1.0 / 2;
    double stepsize = 0.01;
    double t_end = 3.1416;

    for (int N : {49, 99, 149})
    {
        std::vector<double> degs;
        degs.push_back(1);
        for (int j = 1; j <= N / 2; j++)
        {
            degs.push_back(binom(N, j) - binom(N, j - 1));
        }

        for (double a : {0.1, 1.0, 10.0})
        {
           std::vector<double> t_data;
           std::vector<double> S0z_punishment_data;

           for (int n = 0; n < t_end / stepsize; n++)
           {
               t_data.push_back(n * stepsize);
               S0z_punishment_data.push_back(-0.5 * (1 - 2 * alpha_punishment(N, degs, I, a, n * stepsize)));
           }

           char outfile[50];
           sprintf(outfile, "S0z_punishment_N=%d_a=%.1f.txt", N, a);
           save_data(t_data, S0z_punishment_data, std::string(outfile));
        }
    }

    */

	// exemplary plots of dynamics for Sb, Sbz eigenstate and maximally mixed state, respectively

    double I = 1.0 / 2;
    double stepsize = 0.01;
    double t_end = 2 * PI;

	for (int N = 10; N < 101; N+=10)
	{
		std::cout << N << std::endl;
		std::vector<std::vector<double>> multis = multinomials(I, N);
		std::vector<double> degs;
		degs.push_back(1);
		for (int j = 1; j <= floor(N * I); j++)
		{
			degs.push_back(multis[N][j] - multis[N][j-1]);
		}

		std::vector<double> t_data;
		std::vector<double> S0z_mixed;
		std::vector<double> S0z_eigenstate;
		std::stringstream filename;
		filename << "build/examples/S0z_examples_N=" << N << ".txt";
		std::ofstream myfile(filename.str());

		for (int n = 0; n < t_end / stepsize; n++)
		{
			t_data.push_back(n * stepsize);
			S0z_mixed.push_back(-0.5 * (1 - 2 * alpha_mixed_maximal(N, degs, I, n * stepsize)));
			S0z_eigenstate.push_back(-0.5 * (1 - 2 * alpha_pure_entangled(N/2.0, N/2.0 - 2, n * stepsize)));
			myfile << n * stepsize << " " << S0z_mixed[n] << " " << S0z_eigenstate[n] << std::endl;
		}
		myfile.close();
	}

    return 0;
}
