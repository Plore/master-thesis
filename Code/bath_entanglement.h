#ifndef BATH_ENTANGLEMENT_H
#define BATH_ENTANGLEMENT_H

void dynamics_GHZ(int N, double f, double step, double t_end);

void dynamics_W(int N, double step, double t_end);

void dynamics_experimental(double step);

void dynamics_pairwise(double step);

void dynamics_GHZ_sym_largeN(int N, double step, double t_end);

void dynamics_W_largeN(int N, double step, double t_end);

#endif
