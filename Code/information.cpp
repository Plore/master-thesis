#include <iostream>
#include <vector>
#include <cmath>
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <functional>

#include "information.h"

double string_to_code(std::string ss)
{
    double res = 0;
    int len = ss.size();
    for (unsigned int i = 0; i < ss.size(); i++)
    {
        res += pow(2, i) * ((ss[len - i - 1] == '0') ? 0 : 1);
    }
    return res;
}

double index_of_correlation(std::vector<std::string> &polarizations, std::vector<double> &coefs, int n_subsystems)
{
	// normalize coefficients
    double sum = 0;
    for (auto d : coefs)
        sum += pow(d, 2);
    for (unsigned int i = 0; i < coefs.size(); i++)
	{
        coefs[i] /= sqrt(sum);
	}

    int dim = pow(2, polarizations[0].size());
    Eigen::MatrixXd res = Eigen::MatrixXd::Zero(dim, dim);
    for (unsigned int i = 0; i < polarizations.size(); i++)
    {
        for (unsigned int j = 0; j < polarizations.size(); j++)
        {
            double y = string_to_code(polarizations[i]);
            double x = string_to_code(polarizations[j]);
            res(y, x) += coefs[i] * coefs[j];
        }
    }
	//std::cout << res << std::endl << std::endl;

    double index = 0;

    // sum of von Neumann entropies of the individual reduced systems
    for (int n = 1; n <= n_subsystems; n++)
    {
        Eigen::MatrixXd rdm = reduced_density_matrix(res, n);
        Eigen::VectorXcd evs = rdm.eigenvalues();
        for (int i = 0; i < evs.size(); i++)
        {
            if (evs[i].real() > 0)
                index -= evs[i].real() * log(evs[i].real());
        }
    }
    // subtract von Neumann entropy of the entire system
    Eigen::VectorXcd evs = res.eigenvalues();
    for (int i = 0; i < evs.size(); i++)
    {
        if (evs[i].real() > 0)
            index += evs[i].real() * log(evs[i].real());
    }
    return index;
}

Eigen::MatrixXd trace_first_index(Eigen::MatrixXd &full)
{
    int d = full.rows();
    Eigen::MatrixXd out = Eigen::MatrixXd::Zero(d / 2, d / 2);
    for (int i = 0; i < d / 2; i++)
    {
        for (int j = 0; j < d / 2; j++)
        {
            out(i,j) += full(i,j);
            out(i,j) += full(i + d / 2, j + d / 2);
        }
    }
    return out;
}

Eigen::MatrixXd trace_last_index(Eigen::MatrixXd &full)
{
    int d = full.rows();
    Eigen::MatrixXd out = Eigen::MatrixXd::Zero(d / 2, d / 2);
    for (int i = 0; i < d; i+= 2)
    {
        for (int j = 0; j < d; j+=2)
        {
            out(i / 2, j / 2) += full(i, j);
            out(i / 2, j / 2) += full(i + 1, j + 1);
        }
    }
    return out;
}

Eigen::MatrixXd reduced_density_matrix(Eigen::MatrixXd full, int n)
{
    int d = full.rows();
    int n_systems = log2(d);

    for (int i = 1; i < n; i++)
    {
        Eigen::MatrixXd a = trace_first_index(full);
        full = a;
    }
    for (int i = 1; i <= n_systems - n; i++)
    {
        Eigen::MatrixXd a = trace_last_index(full);
        full = a;
    }
    return full;
}

double concurrence(std::vector<std::vector<std::complex<double>>> &density_matrix)
{
    Eigen::MatrixXcd rho = Eigen::MatrixXcd::Zero(4, 4);
    for (unsigned int i = 0; i < rho.rows(); i++)
    {
        for (unsigned int j = 0; j < rho.cols(); j++)
        {
            rho(i,j) = density_matrix[i][j];
        }
    }
    Eigen::MatrixXcd sysy = Eigen::MatrixXcd::Zero(4,4);
    sysy(0,3) = -1;
    sysy(1,2) = 1;
    sysy(2,1) = 1;
    sysy(3,0) = -1;
    Eigen::MatrixXcd R = rho * sysy * rho.conjugate() * sysy;
    Eigen::VectorXcd evs = R.eigenvalues();
    Eigen::VectorXd evs_r = evs.real();
    std::vector<double> evs_real(evs_r.data(), evs_r.data() + evs_r.size());
    std::sort(evs_real.begin(), evs_real.end(), std::greater<double>());
    double maxlamb = evs_real[0] - evs_real[1] - evs_real[2] - evs_real[3];
    return std::max(0.0, maxlamb);
}
