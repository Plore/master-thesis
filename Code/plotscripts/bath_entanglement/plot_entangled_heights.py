import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import argrelextrema
from scipy.stats import linregress

Ns = np.arange(4, 27)
maxima    = []
maxima_ts = []
minima    = []
minima_ee = []

for N in Ns:
    data = np.genfromtxt("../../build/bath_entanglement/rdm_pseudomixed_N={}.txt".format(N), unpack=True)
    datalen = len(data[0])
    RDM_data = data[1:]
    szdata = 0.5 * (1 - 2 * data[1])
    evs1 = 0.5 * (RDM_data[0] + RDM_data[6]) + np.sqrt(0.25 * (RDM_data[0] + RDM_data[6])**2 - RDM_data[0] * RDM_data[6] + RDM_data[2] * RDM_data[4] - RDM_data[3] * RDM_data[5])
    evs2 = 0.5 * (RDM_data[0] + RDM_data[6]) - np.sqrt(0.25 * (RDM_data[0] + RDM_data[6])**2 - RDM_data[0] * RDM_data[6] + RDM_data[2] * RDM_data[4] - RDM_data[3] * RDM_data[5])
    eedata = - evs1 * np.log2(evs1) - evs2 * np.log2(evs2)
    peakind = argrelextrema(szdata, np.greater)[0][0]
    maxima.append(szdata[peakind])
    maxima_ts.append(data[0][peakind])
    minima.append(np.mean(szdata[datalen/2-datalen/8:datalen/2+datalen/8]))
    minima_ee.append(np.mean(eedata[datalen/2-datalen/8:datalen/2+datalen/8]))

plt.axhline(y=-1/6, color="g", label="mixed state asymptotic Sz value")
plt.plot(Ns, maxima, 'r.', label="maximal Sz")
plt.plot(Ns, minima, 'g.', label=r"minimal Sz around $\pi/2$")
plt.plot(Ns, minima_ee, 'y.', label=r"entanglement entrpy around $\pi/2$")
m, b, r, p, stderr = linregress(Ns, minima)
ns = np.linspace(2, 28, 1000)
plt.plot(ns, m * ns + b)
plt.legend(loc="best")
plt.ylim(-0.5, 1.0)
plt.xlim(0, 30)
plt.savefig("../../build/bath_entanglement/pseudomixed_heights.pdf")
