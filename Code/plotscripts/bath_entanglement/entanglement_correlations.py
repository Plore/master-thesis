import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import argrelextrema
from tools import eedata

# pseudomixed states

#with open("../../build/bath_entanglement/entanglement_correlations_pseudomixed.txt", "r") as infile:
#    data = infile.readlines()
#
#coefs_list = np.array([[float(x) for x in s.split()] for s in data[::2]])
#other = np.array([[float(x) for x in s.split()] for s in data[1::2]]).T
#
#bath_z = []
#for i in range(len(coefs_list)):
#    N = len(coefs_list[i]) - 1
#    z = 0
#    for j in range(N + 1):
#        z += coefs_list[i][j]**2 * (- N / 2 + j)
#    bath_z.append(z)
#
#max_index = other[0]
#index = other[1]
#mid_sz = other[2]
#mid_ee = other[3]
#
#plt.plot(bath_z, mid_sz, 'r.', label=r"$\langle S^z \rangle \quad t \sim \pi / 2$")
#plt.plot(bath_z, mid_ee, 'g.', label=r"$E \quad t \sim \pi / 2$")
#plt.axhline(-0.5, color="r")
#plt.legend(loc="best")
#plt.savefig("../../build/bath_entanglement/entanglement_correlations_pseudomixed_bath_z.pdf")
#plt.clf()
#
#plt.plot(index, mid_sz, 'r.', label=r"$\langle S^z \rangle \quad t \sim \pi / 2$")
#plt.plot(index, mid_ee, 'g.', label=r"$E \quad t \sim \pi / 2$")
#plt.axhline(-0.5, color="r")
#plt.legend(loc="best")
#plt.savefig("../../build/bath_entanglement/entanglement_correlations_pseudomixed_index.pdf")
#plt.clf()

# GHZ states

fs, index = np.genfromtxt("../../build/bath_entanglement/entanglement_correlations_ghz.txt", unpack=True)
a = fs / 50
b = 1 - fs / 50
bath_z = [-9/2 * a[i] + 9/2 * b[i] for i in range(len(fs))]

mid_sz = []
mid_ee = []
for f in fs:
    data = np.genfromtxt("../../build/bath_entanglement/GHZ/RDM_GHZ_N=9_f={0:d}_z.txt".format(int(f)), unpack=True)
    datalen = len(data[0])
    RDM_data = data[1:]
    sz = 0.5 * (1 - 2 * data[1])
    ee = eedata(RDM_data[0], RDM_data[2], RDM_data[3], RDM_data[4], RDM_data[5], RDM_data[6])
    print(f, data[1][np.pi/2/0.01], sz[np.pi/2/0.01], ee[np.pi/2/0.01]) 
    mid_sz.append(0.5 - sz[np.pi/2/0.01])
    mid_ee.append(ee[np.pi/2/0.01])

fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(10,4), sharey=True)

box = axes[0].get_position()
axes[0].set_position([box.x0, box.y0, box.width, box.height * 0.85])
box = axes[1].get_position()
axes[1].set_position([box.x0, box.y0, box.width, box.height * 0.85])

axes[0].plot(index, mid_sz, 'r.', label=r"$\langle S_0^z \rangle(t = \pi / 2)$")
axes[0].plot(index, mid_ee, 'g.', label=r"$E(t = \pi / 2)$")
axes[0].set_xlim(-0.5,1.1*max(index))
axes[0].set_xlabel("$I$")

axes[1].plot(bath_z, mid_sz, 'r.', label=r"$\langle S_0^z \rangle(t = \pi / 2)$")
axes[1].plot(bath_z, mid_ee, 'g.', label=r"$E(t = \pi / 2)$")
axes[1].set_xlabel("$S_b^z$")
leg = axes[1].legend(loc="upper center", bbox_to_anchor=(0.5, 0.99), bbox_transform=fig.transFigure, ncol=2, borderaxespad=0)
fig.tight_layout(pad=0.1, w_pad=0.5, rect=(0,0,1,0.85))
fig.savefig("../../build/bath_entanglement/entanglement_correlations_ghz.pdf")
