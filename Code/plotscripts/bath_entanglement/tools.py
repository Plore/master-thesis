import numpy as np

# entanglement entropy of matrix  a b
#                                 c d
# a and d are always real (hermiticity)

def eedata(ar, br, bi, cr, ci, dr):
    evs1 = 0.5 * (ar + dr) + np.sqrt(0.25 * (ar + dr)**2 - ar * dr + br * cr - bi * ci)
    evs2 = 0.5 * (ar + dr) - np.sqrt(0.25 * (ar + dr)**2 - ar * dr + br * cr - bi * ci)
    return - evs1 * np.log2(evs1) - evs2 * np.log2(evs2)

eedata = np.vectorize(eedata)

def sz(rho):
    return -0.5 * (rho[0] - rho[-2]) # a and d are always real, ignore imaginary parts

def sx(rho):
    return 0.5 * (rho[2] + rho[4]) # imaginary parts cancel out (hermiticity)

def sy(rho):
    return 0.5 * (rho[3] - rho[5]) # real parts cancel out (hermiticity)
