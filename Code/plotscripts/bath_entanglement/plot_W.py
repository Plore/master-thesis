import numpy as np
import matplotlib.pyplot as plt
from tools import sx, sy, sz, eedata

funcs = [sx, sz]
initdirs = ["x", "z"]

for i in range(2):
    initdir = initdirs[i]
    func = funcs[i]
    fig, ax = plt.subplots(figsize=(10,5),nrows=2,ncols=1,sharex=True)
    for N in [5,10,20,50,100]:
        data = np.genfromtxt("../../build/bath_entanglement/W/RDM_W_N={}_{}.txt".format(N, initdir), unpack=True)
        ts = data[0]
        RDM_data = data[1:]
        dir_data = func(RDM_data)
        ees = eedata(RDM_data[0], RDM_data[2], RDM_data[3], RDM_data[4], RDM_data[5], RDM_data[6])
        ax[0].plot(ts * (N+1), dir_data, label="$N={}$".format(N))
        ax[1].plot(ts * (N+1), ees, label="$N={}$".format(N))
    
    ax[0].set_ylabel(r"$\langle S_0^{} \rangle$".format(initdir))
    ax[0].set_xlim(0, 10 * np.pi)
    ax[0].set_ylim(-0.5, 0.5)
    ax[0].set_yticks([-0.5, -0.3, -0.1, 0.1, 0.3, 0.5])
    ax[1].set_xlabel("$t / (N+1)$")
    ax[1].set_ylabel("$E$")
    ax[1].set_xlim(0, 10 * np.pi)
    ax[1].set_ylim(0, 1)
    ax[0].legend(bbox_to_anchor=(0, 1.20, 1, 0.1), mode="expand", ncol=5, borderaxespad=0, handletextpad=0.1, columnspacing=0.2)
    fig.tight_layout(pad=0.2, h_pad=0.4, rect=(0,0,1,0.9))
    fig.savefig("../../build/bath_entanglement/W/dynamics_W_{}.pdf".format(initdir))
