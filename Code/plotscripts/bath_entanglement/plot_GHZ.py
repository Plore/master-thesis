import numpy as np
import matplotlib.pyplot as plt 
from tools import sx, sy, sz, eedata 

pols  = ['x', 'z']
funcs = [sx, sz]
fs = [0,9,18,25,35,45]
#colors = plt.get_cmap("Set1")(np.linspace(0, 1, len(fs)))

#########################################
# z-component with initial z polarisation
#########################################

# dependence on a, b
fig, ax = plt.subplots(figsize=(10,5),nrows=2,ncols=1,sharex=True)
for j in range(len(fs)):
    f = fs[j]
    a = f / 50
    b = 1 - f / 50
    ab = np.sqrt(a**2 + b**2)
    a /= ab
    b /= ab
    data = np.genfromtxt("../../build/bath_entanglement/GHZ/RDM_GHZ_N=9_f={}_z.txt".format(f), unpack=True)
    ts = data[0]
    RDM_data = data[1:]
    dir_data = sz(RDM_data)
    ees = eedata(RDM_data[0], RDM_data[2], RDM_data[3], RDM_data[4], RDM_data[5], RDM_data[6])
    ax[0].plot(ts, dir_data, label="$a={:.2f}$".format(a, b))
    ax[1].plot(ts, ees)

ax[0].set_ylabel(r"$\langle S_0^z \rangle$")
ax[0].set_xlim(0, np.pi)
ax[0].set_ylim(-0.5, 0.5)
ax[0].set_xticks([0, 0.25 * np.pi, 0.5 * np.pi, 0.75 * np.pi, np.pi])
ax[0].set_yticks([-0.5, -0.3, -0.1, 0.1, 0.3, 0.5])
ax[0].set_xticklabels(["$0$", r"$\frac{\pi}{4}$", r"$\frac{\pi}{2}$", r"$\frac{3\pi}{4}$", r"$\pi$"])
ax[1].set_xlabel("$t$")
ax[1].set_ylabel("$E$")
ax[1].set_xlim(0, np.pi)
ax[1].set_ylim(0, 1)

ax[0].legend(bbox_to_anchor=(0.99, 0.5), loc="center right", bbox_transform=fig.transFigure, handletextpad=0.1, labelspacing=0.7, borderaxespad=0)
fig.tight_layout(pad=0.2, h_pad=0.5, rect=(0,0,0.82,1))
fig.savefig("../../build/bath_entanglement/GHZ/dynamics_GHZ_z.pdf")

# dependence on N
fig, ax = plt.subplots(figsize=(10,5),nrows=2,ncols=1,sharex=True)
for N in [5,10,20,50,100]:
    RDM_data = np.genfromtxt("../../build/bath_entanglement/GHZ/RDM_GHZ_N={}_sym_z.txt".format(N), unpack=True)
    ts = RDM_data[0]
    RDM_data = RDM_data[1:]
    dir_data = sz(RDM_data)
    ees = eedata(RDM_data[0], RDM_data[2], RDM_data[3], RDM_data[4], RDM_data[5], RDM_data[6])
    ax[0].plot(ts * (N+1), dir_data, label="$N={}$".format(N))
    ax[1].plot(ts * (N+1), ees, label="$N={}$".format(N))
ax[0].set_ylabel(r"$\langle S_0^z \rangle$")
ax[0].set_xlim(0, 10 * np.pi)
ax[0].set_ylim(-0.5, 0.5)
ax[0].set_yticks([-0.5, -0.3, -0.1, 0.1, 0.3, 0.5])
ax[1].set_xlabel("$t\:/\:(N+1)$")
ax[1].set_ylabel("$E$")
ax[1].set_xlim(0, 10 * np.pi)
ax[1].set_ylim(0, 1)
ax[0].legend(bbox_to_anchor=(0, 1.20, 1, 0.1), mode="expand", ncol=5, borderaxespad=0, handletextpad=0.1, columnspacing=0.2)
fig.tight_layout(pad=0.2, h_pad=0.4, rect=(0,0,1,0.9))
fig.savefig("../../build/bath_entanglement/GHZ/dynamics_GHZ_sym_z.pdf")


################################################
# x- and y-component with initial x polarisation
################################################

# dependence on a, b
fig, ax = plt.subplots(figsize=(10,7),nrows=3,ncols=1,sharex=True)
for j in range(len(fs)):
    f = fs[j]
    a = f / 50
    b = 1 - f / 50
    ab = np.sqrt(a**2 + b**2)
    a /= ab
    b /= ab
    data = np.genfromtxt("../../build/bath_entanglement/GHZ/RDM_GHZ_N=9_f={}_x.txt".format(f), unpack=True)
    ts = data[0]
    RDM_data = data[1:]
    sxdata = sx(RDM_data)
    sydata = sy(RDM_data)
    ees = eedata(RDM_data[0], RDM_data[2], RDM_data[3], RDM_data[4], RDM_data[5], RDM_data[6])
    ax[0].plot(ts, sxdata, label="$a={:.2f}$".format(a, b))
    ax[1].plot(ts, sydata, label="$a={:.2f}$".format(a, b))
    ax[2].plot(ts, ees)

ax[0].set_ylabel(r"$\langle S_0^x \rangle$")
ax[0].set_xlim(0, np.pi)
ax[0].set_ylim(-0.5, 0.5)
ax[0].set_xticks([0, 0.25 * np.pi, 0.5 * np.pi, 0.75 * np.pi, np.pi])
ax[0].set_yticks([-0.5, -0.3, -0.1, 0.1, 0.3, 0.5])
ax[0].set_xticklabels(["$0$", r"$\frac{\pi}{4}$", r"$\frac{\pi}{2}$", r"$\frac{3\pi}{4}$", r"$\pi$"])

ax[1].set_ylabel(r"$\langle S_0^y \rangle$")
ax[1].set_xlim(0, np.pi)
ax[1].set_ylim(-0.5, 0.5)
ax[1].set_yticks([-0.5, -0.3, -0.1, 0.1, 0.3, 0.5])

ax[2].set_xlabel("$t$")
ax[2].set_ylabel("$E$")
ax[2].set_xlim(0, np.pi)
ax[2].set_ylim(0, 1)

ax[0].legend(bbox_to_anchor=(0.99, 0.5), loc="center right", bbox_transform=fig.transFigure, handletextpad=0.1, labelspacing=0.7, borderaxespad=0)
fig.tight_layout(pad=0.2, h_pad=0.5, rect=(0,0,0.82,1))
fig.savefig("../../build/bath_entanglement/GHZ/dynamics_GHZ_x.pdf")

# dependence on N
fig, ax = plt.subplots(figsize=(10,5),nrows=2,ncols=1,sharex=True)
for N in [5,10,20,50,100]:
    RDM_data = np.genfromtxt("../../build/bath_entanglement/GHZ/RDM_GHZ_N={}_sym_x.txt".format(N), unpack=True)
    ts = RDM_data[0]
    RDM_data = RDM_data[1:]
    sxdata = sx(RDM_data)
    ees = eedata(RDM_data[0], RDM_data[2], RDM_data[3], RDM_data[4], RDM_data[5], RDM_data[6])
    ax[0].plot(ts * (N+1), sxdata, label="$N={}$".format(N))
    ax[1].plot(ts * (N+1), ees, label="$N={}$".format(N))
ax[0].set_ylabel(r"$\langle S_0^x \rangle$")
ax[0].set_xlim(0, 10 * np.pi)
ax[0].set_ylim(-0.5, 0.5)
ax[0].set_yticks([-0.5, -0.3, -0.1, 0.1, 0.3, 0.5])

ax[1].set_xlabel("$t\:/\:(N+1)$")
ax[1].set_ylabel("$E$")
ax[1].set_xlim(0, 10 * np.pi)
ax[1].set_ylim(0, 1)

ax[0].legend(bbox_to_anchor=(0, 1.20, 1, 0.1), mode="expand", ncol=5, borderaxespad=0, handletextpad=0.1, columnspacing=0.2)
fig.tight_layout(pad=0.2, h_pad=0.4, rect=(0,0,1,0.90))
fig.savefig("../../build/bath_entanglement/GHZ/dynamics_GHZ_sym_x.pdf")
