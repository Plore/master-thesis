import numpy as np
import matplotlib.pyplot as plt
from tools import eedata


fig, ax = plt.subplots(figsize=(11,4),nrows=1,ncols=2,sharex=True)

data = np.genfromtxt("../../build/bath_entanglement/GHZ/RDM_GHZ_N=9_f=25_z.txt", unpack=True)
ts = data[0]
RDM_data = data[1:]
szs = 0.5 * (1 - 2 * RDM_data[0])
ees = eedata(RDM_data[0], RDM_data[2], RDM_data[3], RDM_data[4], RDM_data[5], RDM_data[6])
ax[0].plot(ts, szs, 'b-')
ax[1].plot(ts, ees, 'b-')

ax[0].set_ylim(-0.5,0.5)
ax[0].set_xlabel("$t$")
ax[1].set_ylim(0,1)
ax[1].set_xlabel("$t$")
ax[0].set_title(r"$\langle S_0^z \rangle$")
ax[1].set_title(r"$E$")

fig.tight_layout()
fig.savefig("onthefly.pdf")
