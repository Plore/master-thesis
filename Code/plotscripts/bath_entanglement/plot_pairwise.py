import numpy as np
import matplotlib.pyplot as plt
from tools import sx, sy, sz, eedata

funcs = [sx, sy, sz]
initdirs = ["x", "y", "z", "-x", "-y", "-z"]
for i in range(6):
    func = funcs[i%3]
    initdir = initdirs[i]
    fig, ax = plt.subplots(figsize=(10,5), nrows=2, ncols=1, sharex=True)

    data = np.genfromtxt("../../build/bath_entanglement/RDM_pairwise_{}.txt".format(initdir), unpack=True)
    ts = data[0]
    RDM_data = data[1:]
    dir_data = func(RDM_data)
    ees = eedata(RDM_data[0], RDM_data[2], RDM_data[3], RDM_data[4], RDM_data[5], RDM_data[6])
    ax[0].plot(ts, dir_data, 'b-')
    ax[1].plot(ts, ees, 'b-')

    ax[0].set_ylim(-0.5, 0.5)
    ax[0].set_xlim(0, np.pi)
    ax[0].set_xticks([0, 0.25 * np.pi, 0.5 * np.pi, 0.75 * np.pi, np.pi])
    ax[0].set_yticks([-0.5, -0.3, -0.1, 0.1, 0.3, 0.5])
    ax[0].set_ylabel(r"$\langle S_0^{} \rangle$".format(initdirs[i%3]))

    ax[1].set_ylim(0,1)
    ax[1].set_xlabel("$t$")
    ax[1].set_ylabel(r"$E$")
    ax[1].set_xticks([0, 0.25 * np.pi, 0.5 * np.pi, 0.75 * np.pi, np.pi])
    ax[1].set_xticklabels(["$0$", r"$\frac{\pi}{4}$", r"$\frac{\pi}{2}$", r"$\frac{3\pi}{4}$", r"$\pi$"])

    fig.tight_layout(pad=0.2, h_pad=0.5)
    fig.savefig("../../build/bath_entanglement/dynamics_pairwise_{}.pdf".format(initdir))
