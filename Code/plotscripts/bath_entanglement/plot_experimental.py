import numpy as np
import matplotlib.pyplot as plt
from tools import sx, sy, sz, eedata

funcs = [sx, sy, sz]
initdirs = ["x", "y", "z", "-x", "-y", "-z"]
for i in range(6):
    func = funcs[i%3]
    initdir = initdirs[i]
    fig, ax = plt.subplots(figsize=(11,4),nrows=1,ncols=2,sharex=True)

    data = np.genfromtxt("../../build/bath_entanglement/RDM_experimental_{}.txt".format(initdir), unpack=True)
    ts = data[0]
    RDM_data = data[1:]
    dir_data = func(RDM_data)
    ees = eedata(RDM_data[0], RDM_data[2], RDM_data[3], RDM_data[4], RDM_data[5], RDM_data[6])
    ax[0].plot(ts, dir_data, 'b-')
    ax[1].plot(ts, ees, 'b-')

    ax[0].set_ylim(-0.5,0.5)
    ax[0].set_xlabel("$t$")
    ax[1].set_ylim(0,1)
    ax[1].set_xlabel("$t$")
    ax[0].set_title(r"$\langle S_0^{} \rangle$".format(initdirs[i%3]))
    ax[1].set_title(r"$E$")

    fig.tight_layout()
    fig.savefig("../../build/bath_entanglement/dynamics_experimental_{}.pdf".format(initdir))
