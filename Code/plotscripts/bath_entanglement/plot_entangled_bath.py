import sys
import numpy as np
import matplotlib.pyplot as plt
from tools import sx, sy, sz, eedata


########################
# RDM for product states
########################

#print("plotting product states ...")
#for M in range(0,11):
#    data = np.genfromtxt("../../build/bath_entanglement/rdm_product_M={}.txt".format(M), unpack=True)
#    ts = data[0]
#    RDM_data = data[1:]
#    fig, ax = plt.subplots(nrows=3,ncols=2,sharex=True)
#    ax[0][0].plot(ts, RDM_data[0])
#    ax[0][0].plot(ts, RDM_data[1])
#    ax[0][1].plot(ts, RDM_data[2])
#    ax[0][1].plot(ts, RDM_data[3])
#    ax[1][0].plot(ts, RDM_data[4])
#    ax[1][0].plot(ts, RDM_data[5])
#    ax[1][1].plot(ts, RDM_data[6])
#    ax[1][1].plot(ts, RDM_data[7])
#    ax[1][1].plot(ts, RDM_data[-1], "r--")
#    # Sz and EE for doublechecking
#    szs = 0.5 * (1 - 2 * RDM_data[0])
#    ees = eedata(RDM_data[0], RDM_data[2], RDM_data[3], RDM_data[4], RDM_data[5], RDM_data[6])
#    ax[2][0].plot(ts, szs)
#    ax[2][1].plot(ts, ees)
#    ax[2][0].set_ylim(-0.5,0.5)
#    ax[2][1].set_ylim(0,1)
#    ax[2][0].set_title(r"$\langle S_0^z \rangle$")
#    ax[2][1].set_title(r"$E$")
#    # check with simpler method for definite Sbz
#    ts, upup = np.genfromtxt("../../build/bath_entanglement/rdm_test_M={}.txt".format(M), unpack=True)
#    ax[2][0].plot(ts, -0.5 * (1 - 2 * upup), "r--")
#    fig.savefig("../../build/bath_entanglement/rdm_product_M={}.pdf".format(M))
#    plt.clf()

##############################
# RDM for "pseudomixed" states
##############################

#print("plotting pseudomixed states ...")
#for N in range(3,28):
#    data = np.genfromtxt("../../build/bath_entanglement/rdm_pseudomixed_N={}.txt".format(N), unpack=True)
#    ts = data[0]
#    RDM_data = data[1:]
#    fig, ax = plt.subplots(nrows=3,ncols=2,sharex=True)
#    ax[0][0].plot(ts, RDM_data[0])
#    ax[0][0].plot(ts, RDM_data[1])
#    ax[0][1].plot(ts, RDM_data[2])
#    ax[0][1].plot(ts, RDM_data[3])
#    ax[1][0].plot(ts, RDM_data[4])
#    ax[1][0].plot(ts, RDM_data[5])
#    ax[1][1].plot(ts, RDM_data[6])
#    ax[1][1].plot(ts, RDM_data[7])
#    ax[1][1].plot(ts, RDM_data[-1], "r--")
#    # Sz and EE for doublechecking
#    szs = 0.5 * (1 - 2 * RDM_data[0])
#    ees = eedata(RDM_data[0], RDM_data[2], RDM_data[3], RDM_data[4], RDM_data[5], RDM_data[6])
#    ax[2][0].plot(ts, szs)
#    ax[2][1].plot(ts, ees)
#    ax[2][0].set_ylim(-0.5,0.5)
#    ax[2][1].set_ylim(0,1)
#    ax[2][0].set_title(r"$\langle S_0^z \rangle$")
#    ax[2][1].set_title(r"$E$")
#    fig.savefig("../../build/bath_entanglement/rdm_pseudomixed_N={}.pdf".format(N))
#    #midheight_sz = np.mean(szs[len(ts)/2 - 25: len(ts)/2 + 25])
#    #midheight_ee = np.mean(ees[len(ts)/2 - 25: len(ts)/2 + 25])
#    #with open("entanglement_correlations_pseudomixed.txt", "a") as indexfile:
#    #    indexfile.write("{} {}\n".format(midheight_sz, midheight_ee))
#    plt.clf()

#########################
# pairwise triplet states
#########################

#print("plotting pairwise entangled bath ...")
#for N in [6]:
#    data = np.genfromtxt("../../build/bath_entanglement/rdm_pairwise_3triplets.txt", unpack=True)
#    ts = data[0]
#    RDM_data = data[1:]
#    fig, ax = plt.subplots(figsize=(10,3),nrows=1,ncols=2,sharex=True)
#    #ax[0][0].plot(ts, RDM_data[0])
#    #ax[0][0].plot(ts, RDM_data[1])
#    #ax[0][1].plot(ts, RDM_data[2])
#    #ax[0][1].plot(ts, RDM_data[3])
#    #ax[1][0].plot(ts, RDM_data[4])
#    #ax[1][0].plot(ts, RDM_data[5])
#    #ax[1][1].plot(ts, RDM_data[6])
#    #ax[1][1].plot(ts, RDM_data[7])
#    #ax[1][1].plot(ts, RDM_data[-1], "r--")
#    # Sz and EE for doublechecking
#    szs = 0.5 * (1 - 2 * RDM_data[0])
#    ees = eedata(RDM_data[0], RDM_data[2], RDM_data[3], RDM_data[4], RDM_data[5], RDM_data[6])
#    ax[0].plot(ts, szs)
#    ax[0].set_xlim(0, 2 * np.pi)
#    ax[0].set_ylim(-0.5,0.5)
#    ax[0].set_xlabel("$t$")
#    ax[0].set_ylabel(r"$\langle S_0^z \rangle$")
#
#    ax[1].plot(ts, ees)
#    ax[1].set_xlim(0, 2 * np.pi)
#    ax[1].set_ylim(0,1)
#    ax[1].set_xlabel("$t$")
#    ax[1].set_ylabel(r"$E$")
#
#    fig.tight_layout()
#    fig.suptitle("$N=6$")
#    fig.savefig("../../build/bath_entanglement/rdm_pairwise_3triplets.pdf")
#    plt.clf()

##################
# GHZ class states
##################
