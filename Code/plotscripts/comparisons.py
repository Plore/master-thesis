import matplotlib.pyplot as plt
import numpy as np

####################################
# Comparison with Bortz, Stolze 2007
####################################

fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(10,6))

for M in [5, 10, 20, 50]:
    N = 2 * M + 1
    ts, sz_paper, sz_program = np.genfromtxt("../build/comparisons/sz_I=0.5_M={}_N=2M+1.txt".format(M), unpack=True)

    line1, = axes[0][0].plot(ts, sz_paper, 'b-', label="BS2007")
    line2, = axes[0][0].plot(ts, sz_program, 'r--', label="Lorenz")

axes[0][0].set_xlim(0, np.pi)
axes[0][0].set_title(r"$N = 2M+1$")
axes[0][0].set_ylabel(r"$\langle S_0^z(t) \rangle$")

for M in [5, 10, 20, 50]:
    N = 2 * M
    ts, sz_paper, sz_program = np.genfromtxt("../build/comparisons/sz_I=0.5_M={}_N=2M.txt".format(M), unpack=True)

    line1, = axes[0][1].plot(ts, sz_paper, 'b-', label="BS2007")
    line2, = axes[0][1].plot(ts, sz_program, 'r--', label="Lorenz")

axes[0][1].set_xlim(0, np.pi)
axes[0][1].set_title(r"$N = 2M$")
axes[0][1].set_ylabel(r"$\langle S_0^z(t) \rangle$")

for M in [5, 10, 20, 50]:
    N = 2 * M + 1
    ts, ee_paper, ee_program = np.genfromtxt("../build/comparisons/ee_I=0.5_M={}_N=2M+1.txt".format(M), unpack=True)

    line1, = axes[1][0].plot(ts, ee_paper, 'b-', label="BS2007")
    line2, = axes[1][0].plot(ts, ee_program, 'r--', label="Lorenz")

axes[1][0].set_xlim(0, np.pi)
axes[1][0].set_ylabel("$E$")

for M in [5, 10, 20, 50]:
    N = 2 * M
    ts, ee_paper, ee_program = np.genfromtxt("../build/comparisons/ee_I=0.5_M={}_N=2M.txt".format(M), unpack=True)

    line1, = axes[1][1].plot(ts, ee_paper, 'b-', label="BS2007")
    line2, = axes[1][1].plot(ts, ee_program, 'r--', label="Lorenz") 
    
axes[1][1].set_xlim(0, np.pi)
axes[1][1].set_ylabel("$E$")

fig.tight_layout()
fig.legend((line1, line2), ("BS2007", "Lorenz"), bbox_to_anchor=(0.90,0.75))
fig.savefig("../build/comparisons/comparison_stolze.pdf")


##############################
# Comparison with Meißner 2014
##############################

#plt.ticklabel_format(style="sci", axis="y", scilimits=(0,0))

# comparison for longer timescale

fig, axes = plt.subplots(nrows=3, ncols=1, figsize=(10,8), sharex=True, sharey=True)
fig.subplots_adjust(hspace=0.1)
#plt.setp(axes[0].get_xticklabels(), visible=False)
Ns = [3,5,7]

for i in range(len(Ns)):
    t, x = np.genfromtxt("../bachelor_programm/szdata{}_long.txt".format(Ns[i]), unpack=True)
    line1, = axes[i].plot(t, 0.5-x, 'b-')
    
    tb, xb = np.genfromtxt("../bachelor_programm/ausgabe{}_long.txt".format(Ns[i]), unpack=True)
    xb *= 2
    line2, = axes[i].plot(tb, xb, 'r--')
    axes[i].set_ylim(0, 0.55)
    axes[i].set_ylabel(r"$\langle S_0^z(t) \rangle$")

axes[2].set_xlabel(r"$t\:\sqrt{N} A$")
fig.legend((line1, line2), (r"$\mathrm{exact}$", r"$\mathrm{Mei{\ss}ner}$"), handlelength=2.3, loc="upper center", bbox_to_anchor=(0.85, 0.98))
fig.tight_layout(pad=0.2, h_pad=0.5)
fig.savefig("../build/comparisons/comparison_meissner.pdf")

# plot relative errors

fig, axes = plt.subplots(nrows=3, ncols=1, figsize=(8,10), sharex=True)
fig.subplots_adjust(hspace=0.1)

for i in range(len(Ns)):
    t, x = np.genfromtxt("../bachelor_programm/szdata{}_long.txt".format(Ns[i]), unpack=True)
    tb, xb = np.genfromtxt("../bachelor_programm/ausgabe{}_long.txt".format(Ns[i]), unpack=True)
    assert len(tb) == len(t)
    xb *= 2
    axes[i].plot(tb, abs(xb - x) / x)
    axes[i].set_ylabel(r"$\Delta \langle S_0^z(t) \rangle$")

axes[0].set_ylim(0,0.000007)
axes[1].set_ylim(0,0.07)
axes[2].set_ylim(0,0.39)

axes[0].ticklabel_format(style="sci", scilimits=(-3,3))

fig.savefig("../build/comparisons/comparison_meissner_error.pdf")

#################################
# comparison with Merkulov et al.
#################################

N = 100
fig, axes = plt.subplots(figsize=(10,4), nrows=1)
ts, sz, _ = np.genfromtxt("../build/examples/S0z_examples_N={}.txt".format(N), unpack=True)
T = 1 / (np.sqrt(N) * 2) # characteristic timescale
sz_merkulov = - 0.5 / 3 * (1 + 2 * (1 - 1 / 4 * ts**2 / T**2) * np.exp(- 1 / 8 * ts**2 / T**2))
axes.plot(ts, sz, 'b-')
axes.plot(ts, sz_merkulov, 'r--', label=r"$\mathrm{Merkulov et al.}$")
axes.set_xlim(0, 2 * np.pi)
axes.set_xticks([0, np.pi / 2, np.pi, 3 / 2 * np.pi, 2 * np.pi])
axes.set_xticklabels([r"$0$", r"$\frac{\pi}{2}$", r"$\pi$", r"$\frac{3 \pi}{2}$", r"$2 \pi$"])
axes.set_xlabel("$t$")
axes.set_ylabel(r"$\langle S_0^z \rangle$")
#axes.legend(loc="best")
fig.tight_layout(pad=0.2)
fig.savefig("../build/comparisons/comparison_merkulov.pdf")

