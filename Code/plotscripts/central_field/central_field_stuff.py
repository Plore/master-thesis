import numpy as np
import matplotlib.pyplot as plt
from  matplotlib.gridspec import GridSpec
from scipy.special import binom
from matplotlib.ticker import MaxNLocator
import scipy.fftpack

builddir = "../../build/central_field/"

def alpha_pure_entangled(Sb, Sbz, t):
    return 2 * (Sb - Sbz + 1) * (Sb + Sbz) / (2 * Sb + 1)**2 * (1 - np.cos((2 * Sb + 1) * t))

alpha_pure_entangled = np.vectorize(alpha_pure_entangled)

def S0z_eigenstate_schneider(Sb, Sbz, h, A, t):
    return 1 / (A**2 * (Sb**2 + Sb - Sbz**2 + Sbz) + (h + A * (Sbz - 1/2))**2) * (-1/2 * (h + A * (Sbz - 1/2))**2 - 0.5 * A**2 * (Sb**2 + Sb - Sbz**2 + Sbz) * np.cos(np.sqrt((h + A * (Sbz - 1/2))**2 + A**2 * (Sb**2 + Sb - Sbz**2 + Sbz)) * t))

def S0z_central_mixed(N, h, A, t):
    res = 0
    Sbs = np.arange((N % 2) / 2, N / 2 + 1)
    degs = np.insert([binom(N, j) - binom(N, j - 1) for j in range(1, int(N/2) + 1)], 0, 1)[::-1]
    for i in range(len(Sbs)):
        Sb = Sbs[i]
        for Sbz in np.arange(-Sb, Sb+1):
            res += S0z_eigenstate_schneider(Sb, Sbz, h, A, t) * degs[i] 

    res /= 2**N
    return res

Ns = [50]
hs = [1,10,20,50,100]
colors = plt.get_cmap("Set1")(np.linspace(0,1,len(hs)))

#ts = np.linspace(0, 2 * np.pi, 100)
#tslars, szlars = np.genfromtxt("S0z_punishment_N=50_h=1.0.txt", unpack=True, usecols=(0,1))
#plt.plot(tslars, szlars, 'b-', label="$N=50, h=1 Lars$")
#szdata = [-S0z_central_mixed(10, 30, 2, t) for t in ts]
#fftdata = np.fft.rfft(szdata)
#plt.plot(ts, szdata, 'g-', label="Schneider superposition")
#print(fftdata)
##plt.plot(fftdata, 'r,')
##plt.plot(ts, [func(t) for t in ts], 'b-', label="Schneider")
#plt.savefig("central_field_test.pdf")
#plt.clf()
##plt.plot(ts, [-0.5 * (1 - 2 * alpha_pure_entangled(Sb, Sbz, t)) for t in ts], 'g-.', label="alpha_pure_entangled(trusted)")

#for N in Ns:
#    fig = plt.figure(figsize=(10,10))
#    gs = GridSpec(2,1, height_ratios=[3,1])
#    ax1 = plt.subplot(gs[0])
#    ax2 = plt.subplot(gs[1])
#    for i in range(len(hs)):
#        h = hs[i]
#        data = np.genfromtxt(builddir+"central_field_RDM_N={:d}_h={:.1f}.txt".format(N, h), unpack=True)
#        ts = data[0]
#        downdownr = data[1]
#        upupr = data[-2]
#        szdata = 0.5 * (1 - 2 * downdownr)
#        ax1.plot(ts, szdata, '-', color=colors[i], label=r"$h={}$".format(h))
#    ax1.legend(loc="lower right")
#    ax1.set_xlabel("$t$")
#    ax1.set_xlim(0, 10 * np.pi)
#    ax1.set_ylabel(r"$\langle S_0^z\rangle$")
#   # .title("$N={}$".format(N))
#    #inset1 = plt.gcf().add_axes([0.22, 0.15, 0.6, 0.20])
#
#    data = np.genfromtxt(builddir+"central_field_RDM_N={:d}_h=100.0.txt".format(N, h), unpack=True)
#    ts = data[0]
#    downdownr = data[1]
#    upupr = data[-2]
#    szdata = 0.5 * (1 - 2 * downdownr)
#    ax2.plot(ts, szdata)
#    ax2.set_xlim(0, 10 * np.pi)
#    ax2.set_xlabel("$t$")
#    ax2.set_ylabel(r"$\langle S_0^z\rangle$")
#    fig.savefig(builddir+"central_field_N={}.pdf".format(N))

