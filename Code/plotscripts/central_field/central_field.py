import numpy as np
import matplotlib.pyplot as plt
from  matplotlib.gridspec import GridSpec
from scipy.special import binom
from scipy.stats import sem
from matplotlib.ticker import MaxNLocator
import scipy.fftpack

def eedata(ar, br, bi, cr, ci, dr):
    evs1 = 0.5 * (ar + dr) + np.sqrt(0.25 * (ar + dr)**2 - ar * dr + br * cr - bi * ci)
    evs2 = 0.5 * (ar + dr) - np.sqrt(0.25 * (ar + dr)**2 - ar * dr + br * cr - bi * ci)
    return - evs1 * np.log2(evs1) - evs2 * np.log2(evs2)

eedata = np.vectorize(eedata)

builddir = "../../build/central_field/"
Ns = [50]
hs = [1, 2, 5, 10, 20, 50, 100]
Ts1 = [0.2, 0.5, 1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0, 1000, 1000000]
Ts2 = [0.1, 0.2, 0.3, 0.4, 0.6, 0.8, 1.2, 1.6, 2.4, 3.2, 4.8, 6.4, 9.6, 12.8, 19.2, 25.6, 38.4, 51.2, 76.8, 102.4, 153.6, 204.8, 307.2, 409.6]
colors = plt.get_cmap("Set1")(np.linspace(0,1,len(hs)))

# plot S_0^z curves 

print("plotting S_0^z curves for different T ...")
for N in Ns:
    for j in range(len(Ts1)):
        T = Ts1[j]
        fig, axes = plt.subplots(figsize=(10,13), nrows=7, sharex=True)
        for i in range(len(hs)):
            h = hs[i]
            data = np.genfromtxt(builddir+"RDM/central_field_RDM_N={:d}_h={:.1f}_T={:.1f}.txt".format(N, h, T), unpack=True)
            ts = data[0]
            downdownr = data[1]
            upupr = data[-2]
            szdata = 0.5 * (1 - 2 * downdownr)
            axes[i].plot(ts, szdata, '-', color=colors[i], label=r"$\omega={}$".format(h))
            axes[i].set_ylim(min(szdata), max(szdata))
            axes[i].yaxis.set_major_locator(MaxNLocator(nbins=5, prune="upper"))
        axes[-1].set_xlabel("$t$")
        axes[-1].set_xlim(0, 10 * np.pi)

        fig.legend(handles=[axes[i].get_legend_handles_labels()[0][0] for i in range(len(hs))], labels=["$\omega={}$".format(hs[i]) for i in range(len(hs))], loc="upper left", bbox_to_anchor=(0.20, 0.99), ncol=4)
        fig.text(0.02, 0.5, r"$\langle S_0^z \rangle$", rotation=90)
        fig.tight_layout(pad=0.2, h_pad=0, rect=(0.05, 0, 1, 0.90))
        fig.subplots_adjust(hspace=0)
        fig.savefig(builddir+"central_field_N={}_T={}.pdf".format(N, T))


# plot entanglement entropy and fourier spectrum of <S_0^z>

print("plotting corresponding entanglement entropies and fourier spectra ...")
for N in Ns:
    for j in range(len(Ts1)):
        T = Ts1[j]
        fig, axes = plt.subplots(figsize=(10,13), ncols=2, nrows=7, sharex='col')
        for i in range(len(hs)):
            h = hs[i]
            data = np.genfromtxt(builddir+"RDM/central_field_RDM_N={:d}_h={:.1f}_T={:.1f}.txt".format(N, h, T), unpack=True)
            ts = data[0]
            delta_t = data[0][1] - data[0][0]

            szdata = 0.5 * (data[-2] - data[1])
            ees = eedata(data[1], data[3], data[4], data[5], data[6], data[7])
            yf = 2 / N * np.abs(scipy.fftpack.fft(szdata)[0:int(len(szdata) / 2)])
            xf = np.linspace(0, 1 / (2 * delta_t), int(len(szdata) / 2))

            axes[i][0].plot(ts, ees, '-', color=colors[i], label=r"$\omega={}$".format(h))
            axes[i][0].yaxis.set_major_locator(MaxNLocator(nbins=5, prune="both"))
            axes[i][0].set_xlim(0,30)

            axes[i][1].plot(xf, yf, '-', color=colors[i], label=r"$\omega={}$".format(h))
            axes[i][1].set_yscale('log')
            axes[i][1].set_xlim(-0.5, 2 / 5 * xf[-1])
            axes[i][1].minorticks_off()
            axes[i][1].set_ylim(1e-4, 1e2)
            axes[i][1].set_yticks([1e-3, 1e-1, 1e1])
        axes[-1][0].set_xlabel("$t$")
        axes[-1][1].set_xlabel("$f$")
        axes[0][0].set_title("$E$")
        axes[0][1].set_title(r"$\mathcal{F}[\langle S_0^z \rangle]$")

        fig.legend(handles=[axes[i][0].get_legend_handles_labels()[0][0] for i in range(len(hs))], labels=["$\omega={}$".format(hs[i]) for i in range(len(hs))], loc="upper left", bbox_to_anchor=(0.1, 0.99), ncol=4)
        fig.subplots_adjust(hspace=0.001)
        fig.tight_layout(pad=0.2, h_pad=0, w_pad=0.5, rect=(0, 0, 1, 0.90))
        fig.savefig(builddir+"central_field_N={}_T={}_spectrum_EE.pdf".format(N, T))


# get average heights of curves 

print("plotting average S_0^z for different T ...")
for N in Ns:
    avg_heights = []
    sem_heights = []
    for j in range(len(Ts2)):
        T = Ts2[j]
        avg_heights.append([])
        sem_heights.append([])
        for i in range(len(hs)):
            h = hs[i]
            data = np.genfromtxt(builddir+"RDM/central_field_RDM_N={:d}_h={:.1f}_T={:.1f}.txt".format(N, h, T), unpack=True)
            ts = data[0]
            downdownr = data[1]
            upupr = data[-2]
            szdata = 0.5 * (1 - 2 * downdownr)
            avg_heights[j].append(np.mean(szdata))
            sem_heights[j].append(sem(szdata))

    avg_heights = np.array(avg_heights)
    sem_heights = np.array(sem_heights)
    
    fig, ax = plt.subplots(figsize=(10, 6))
    for i in range(len(hs)):
        ax.plot(Ts2, avg_heights[:,i], marker="+", linestyle='None', label="$\omega={}$".format(hs[i]))
        
    ax.set_xlim(0.05, 1000)
    ax.set_ylim(0.05, 0.55)
    ax.set_xscale('log')
    ax.set_xlabel("$T$")
    ax.set_ylabel(r"$\overline{\langle S_0^z \rangle}$")
    ax.legend(bbox_to_anchor=(0.8, 1.0), loc="upper left", bbox_transform=fig.transFigure)
    fig.tight_layout(pad=0.2, rect=(0,0,0.8,1))
    fig.savefig(builddir+"central_field_N={}_average_heights_temperature.pdf".format(N))


print("plotting average S_0^z for maximally mixed bath, many values of h ...")
hs = []
hs.extend(np.arange(0.1, 1, 0.1))
hs.extend(np.arange(1.0, 10.0, 0.2))
hs.extend(np.arange(10, 50, 2))
hs.extend(np.arange(50, 200, 5))
Ns = [5,10,20,50,100]
colors = plt.get_cmap("Set1")(np.linspace(0,1,len(Ns)))

fig = plt.figure(figsize=(10,4))
ax = fig.add_subplot(111)
savedata = []
savedata.append(hs)
for n in range(len(Ns)):
    N = Ns[n]
    print(N)
    heights = []
    for i in range(len(hs)):
        h = hs[i]
        data = np.genfromtxt("../../build/central_field/RDM/central_field_RDM_N={}_h={:.1f}.txt".format(N, h), unpack=True)
        ts = data[0]
        downdownr = data[1]
        upupr = data[-2]
        szdata = 0.5 * (1 - 2 * downdownr)
        heights.append(np.mean(szdata[int(len(szdata) / 2) : ]))
    ax.plot(hs, heights, '.', color=colors[n], label="$N={}$".format(N))
    savedata.append(np.array(heights))
    
savedata = np.array(savedata)
np.savetxt("S0z_average_data.txt", savedata.T, delimiter="\t", fmt=["%.3f","%.14e","%.14e","%.14e","%.14e","%.14e"], header="omega\tN=5\t\t\t\t\t\tN=10\t\t\t\t\tN=20\t\t\t\t\tN=50\t\t\t\t\tN=100")
ax.set_yscale("log")
ax.set_xscale("log")
ax.set_xlabel("$\omega$")
ax.set_ylabel(r"$\overline{\langle S_0^z \rangle}$")
ax.set_yticks([0.2, 0.3, 0.4, 0.5, 0.6, 0.7])
ax.set_yticklabels(["$0.2$", "$0.3$", "$0.4$", "$0.5$", "$0.6$", "$0.7$"])
ax.set_xlim(0.08, 250)
ax.set_ylim(0.13, 0.7)
ax.legend(loc="best")
fig.tight_layout(pad=0.2)
fig.savefig("../../build/central_field/central_field_average_height.pdf")


# plot and fft of all components for different h

print("plotting S_0^x and S_0^y components ...")
Ns = [5,10,20,50]
hs = [20,50,100,200]

fig, axes = plt.subplots(figsize=(10, 10), nrows=len(hs), ncols=3, sharex=True)
fig2, axes2 = plt.subplots(figsize=(10, 7), nrows=len(hs), ncols=2, sharex=True, sharey='row')
for i in range(len(hs)):
    h = hs[i]
    data = np.genfromtxt(builddir+"RDM/central_field_RDM_N=50_h={:.1f}.txt".format(h), unpack=True)
    ts = data[0]
    delta_t = data[0][1] - data[0][0]
    szdata = 0.5 * (data[-2] - data[1])
    sxdata = data[3]
    sydata = - data[4]

    axes2[i][0].plot(ts, sxdata)
    axes2[i][1].plot(ts, sydata)
#    axes2[i][2].plot(ts, szdata)
    axes2[i][0].yaxis.set_major_locator(MaxNLocator(nbins=7, prune="upper"))
    axes2[i][1].yaxis.set_major_locator(MaxNLocator(nbins=7, prune="upper"))
#    axes2[i][2].yaxis.set_major_locator(MaxNLocator(nbins=7, prune="upper"))

    szdata_fft = 2 / int(len(ts)) * np.abs(scipy.fftpack.fft(szdata)[0:int(len(ts)/2)])
    sxdata_fft = 2 / int(len(ts)) * np.abs(scipy.fftpack.fft(sxdata)[0:int(len(ts)/2)])
    sydata_fft = 2 / int(len(ts)) * np.abs(scipy.fftpack.fft(sydata)[0:int(len(ts)/2)])
    freqs = np.linspace(0, 1 / (2 * delta_t), int(len(ts) / 2))
    axes[i][0].plot(freqs, szdata_fft, '-', color=colors[i])
    axes[i][1].plot(freqs, sxdata_fft, '-', color=colors[i])
    axes[i][2].plot(freqs, sydata_fft, '-', color=colors[i])
    axes[i][0].set_yscale('log')
    axes[i][1].set_yscale('log')
    axes[i][2].set_yscale('log')

axes2[0][0].set_xlim(0, 10 * np.pi)
axes2[0][0].set_title(r"$\langle S_0^x\rangle$")
axes2[0][1].set_title(r"$\langle S_0^y\rangle$")
axes2[-1][0].set_xlabel("$t$")
axes2[-1][1].set_xlabel("$t$")

fig.tight_layout(pad=0.3, h_pad=0.0, w_pad=0.2)
fig.savefig(builddir+"central_field_N=50_spectrum.pdf")

fig2.tight_layout(pad=0.3, h_pad=0.0, w_pad=0.2)
fig2.savefig(builddir+"central_field_N=50_components.pdf")

