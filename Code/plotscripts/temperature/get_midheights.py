import numpy as np
from scipy.signal import argrelextrema

Ns = np.arange(2, 30)
Ts_low = np.arange(0.05, 1.0, 0.005)
Ts_high = np.arange(1.0, 50.0, 0.1)
Ts = np.append(Ts_low, Ts_high)

# I = 1.5

#for N in Ns:
#    print(N)
#    midheight = []
#
#    for T in Ts:
#        ts, alphas = np.genfromtxt("../../build/temperature/1.5/alpha_mixed/alpha_mixed_N={}_T={:.03f}.txt".format(N, T), unpack=True)
#        szs = 0.5 * (1 - 2 * alphas)
#        av_indices = [int(len(ts) / 2) + x for x in range(-int(len(ts) / (3 * N)), int(len(ts) / (3 * N)))]
#        midheight.append(max([szs[i] for i in av_indices]))
#        
#    with open("../../build/temperature/1.5/midheights_N={}.txt".format(N), "w") as outfile:
#        for i in range(len(Ts)):
#            outfile.write("{:.3f} {:.10f}\n".format(Ts[i], midheight[i]))


# I = 0.5

Ns = np.arange(2, 23)

for N in Ns:
    print(N)
    midheight = []

    for T in Ts:
        ts, alphas = np.loadtxt("../../build/temperature/0.5/alpha_mixed/alpha_mixed_N={}_T={:.03f}.txt".format(N, T), unpack=True)
        szs = 0.5 * (1 - 2 * alphas)
        av_indices = [int(len(ts) / 2) + x for x in range(-int(len(ts) / (1.0 * N)), int(len(ts) / (1.0 * N)))]
        #szs_peakfind = np.array([szs[i] for i in av_indices])
        midheight.append(max([szs[i] for i in av_indices]))
        
    with open("../../build/temperature/0.5/midheights_N={}.txt".format(N), "w") as outfile:
        for i in range(len(Ts)):
            outfile.write("{:.3f} {:.10f}\n".format(Ts[i], midheight[i]))
