import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.signal import argrelextrema
from scipy.stats import linregress
from time import sleep

builddir = "../../build/temperature/1.5/"
#############################
# representative plot, N = 29
#############################

print("Plotting example ...")

N = 29
Ts = [0.2, 0.3, 0.4]
fig, axes = plt.subplots(figsize=(10,7), nrows=3, ncols=1, sharex=True, sharey=True)

def coshfit(x, a, b, c, d):
    return a * np.cosh(b * (x + c)) + d

for i in range(len(Ts)):
    T = Ts[i]
    ts, alphas = np.genfromtxt(builddir+"alpha_mixed/alpha_mixed_N={}_T={:.3f}.txt".format(N, T), unpack=True)
    szs = 0.5 * (1 - 2 * alphas)
    axes[i].plot(ts, szs, label="$T={}$".format(Ts[i]))
    peakind = argrelextrema(szs, np.greater)[0]
    axes[i].set_ylim(-0.2,0.55)
    axes[i].set_xlim(0, np.pi)
    axes[i].set_ylabel(r"$\langle S_0^z \rangle$")
    axes[i].set_yticks([-0.2,-0.1,0.0,0.1,0.2,0.3,0.4,0.5])
    axes[i].legend(loc="lower center")
    popt, pcov = curve_fit(coshfit, ts[peakind[5:-5]], szs[peakind[5:-5]], p0=(0.01, 2, -1.6, 0.3), maxfev=3000)
    print(popt)
    axes[i].plot(ts, coshfit(ts, *popt), 'g')

axes[2].set_xticks([0, np.pi / 4, np.pi / 2, 3 * np.pi / 4, np.pi])
axes[2].set_xticklabels(["$0$", r"$\frac{\pi}{4}$", r"$\frac{\pi}{2}$", r"$\frac{3\pi}{4}$", r"$\pi$"])
axes[2].set_xlabel("$t$")
fig.tight_layout(pad=0.2, h_pad=0.4)
fig.savefig(builddir+"damping_example.pdf")

############################
## plot overview 0 < T < 50
############################

print("Plotting overview ...")

Ns = np.arange(2,30)
Ns_plot = np.arange(4, 30, 3)

fig = plt.figure(figsize=(10, 5))
ax = fig.add_subplot(111)
colors = plt.get_cmap('Accent')(np.linspace(0,1,len(Ns_plot)))

for i in range(len(Ns_plot)):
    Ts, midheights = np.genfromtxt(builddir+"midheights_N={}.txt".format(Ns_plot[i]), unpack=True)
    ax.plot(Ts, midheights, '.', color=colors[i], label="$N={}$".format(Ns_plot[i]))

box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.85, box.height])
ax.legend(loc="upper left", bbox_to_anchor=(0.7, 1.0))
ax.set_xlabel("$T$")
ax.set_ylabel(r"$h$")
ax.set_xlim(0, 15.0)
fig.savefig(builddir+"all_T_overview.pdf")

ax.set_xlim(0.1, 50.0)
ax.set_ylim(0.10, 0.55)
ax.set_xscale('log')
#ax.set_yscale('log')
ax.set_yticks([0.1666, 0.2, 0.3, 0.4, 0.5])
ax.set_yticklabels([r"$\frac{1}{6}$", "$0.2$", "$0.3$", "$0.4$", "$0.5$"])
fig.tight_layout(pad=0.2)
fig.savefig(builddir+"all_T_overview_log.pdf")

#######################################
# plot 0 < T < 1, fit exponential range
#######################################

print("Plotting exponential regime ...")

fig = plt.figure(figsize=(10,5))
ax = fig.add_subplot(111)
xs = np.linspace(0.1, 1.0, 1000)
reglim=[(80, 110), (70, 100), (70, 90), (65, 90), (60, 85), (55, 75), (45, 75), (43, 75), (40, 75), (40, 70),
        (40, 70), (40, 65), (40, 65), (40, 65), (40, 60), (37, 60), (37, 60), (37, 55), (35, 55),  (35, 55),
        (35, 52), (35, 52), (35, 50), (35, 50), (35, 48), (32, 48), (32, 45), (32, 45)]

slopes = []
intercepts = []
colors = plt.get_cmap('Accent')(np.linspace(0,1,len(Ns)))

for i in range(len(Ns)):
    Ts, midheights = np.genfromtxt(builddir+"midheights_N={}.txt".format(Ns[i]), unpack=True)
    height_plot = midheights
    slope, intercept, r_value, p_value, std_err = linregress(Ts[reglim[i][0]:reglim[i][1]], np.log(height_plot[reglim[i][0]:reglim[i][1]]))
    slopes.append(slope)
    intercepts.append(intercept)
    if Ns[i] in Ns_plot:
        ax.plot(Ts, height_plot, '.', color=colors[i], label="$N={}$".format(Ns[i]))
        ax.plot(xs, np.exp(slope * xs) * np.exp(intercept), '-', color=colors[i])
    ax.set_yscale('log')
    ax.set_xlim(0.0, 0.6)
    ax.set_ylim(0.15, 0.6)
    ax.set_yticks([0.2, 0.3, 0.4, 0.5])
    ax.set_yticklabels(["$0.2$", "$0.3$", "$0.4$", "$0.5$"])

ax.legend(loc="lower left",ncol=2,columnspacing=1)
ax.set_xlabel("$T$")
ax.set_ylabel(r"$h$")
fig.tight_layout(pad=0.2)
fig.savefig(builddir+"mid_T_exponential.pdf")

# exponent dependency on N
slopes = np.array(slopes)
slope, intercept, r_value, p_value, std_err = linregress(Ns[4:], slopes[4:])
sx2 = np.sum((Ns-np.mean(Ns))**2)
sd_intercept = std_err * np.sqrt(1.0 / len(Ns) + np.mean(Ns)**2 / sx2)
sd_slope = std_err * np.sqrt(1.0 / sx2)

fig = plt.figure(figsize=(10,6))
ax = fig.add_subplot(111)
ns = np.linspace(0, 35, 1000)
ax.plot(ns, intercept + slope * ns, 'b-')
ax.plot(Ns, slopes, 'r.')
ax.set_xlabel("$N$")
ax.set_ylabel("$k$")
print("Exponent dependency on N")
print(slope, sd_slope)
print(intercept, sd_intercept)
fig.tight_layout(pad=0.2)
fig.savefig(builddir+"mid_T_slopes.pdf")

# prefactor dependency on N
slope, intercept, r_value, p_value, std_err = linregress(Ns[4:], intercepts[4:])
sx2 = np.sum((Ns-np.mean(Ns))**2)
sd_intercept = std_err * np.sqrt(1.0 / len(Ns) + np.mean(Ns)**2 / sx2)
sd_slope = std_err * np.sqrt(1.0 / sx2)
print("Prefactor dependency on N")
print(slope, sd_slope)
print(intercept, sd_intercept)
fig = plt.figure(figsize=(10,6))
ax = fig.add_subplot(111)
ax.plot(Ns, intercepts, 'r.')
ax.set_xlabel("$N$")
fig.tight_layout()
fig.savefig(builddir+"mid_T_intercepts.pdf")

#####################################
# plot 0.05 < T < 0.20, fit arrhenius
#####################################

print("Plotting Arrhenius regime ...")

fig = plt.figure(figsize=(10,5))
ax = fig.add_subplot(111)
colors = plt.get_cmap('Accent')(np.linspace(0,1,len(Ns_plot)))

for i in range(len(Ns_plot)):
    Ts, midheights = np.genfromtxt(builddir+"midheights_N={}.txt".format(Ns_plot[i]), unpack=True)
    Ts_plot = Ts[Ts < 0.2]
    height_plot = 0.5 - midheights[Ts < 0.2]#np.array([0.5 - midheights[j] for j in range(len(Ts)) if 0.4 < midheights[j]])
    ax.plot(1 / Ts_plot, height_plot, '.', color=colors[i], label="$N={}$".format(Ns_plot[i]))
    slope, intercept, r_value, l_value, std_err = linregress(1 / Ts_plot[8:], np.log(height_plot[8:]))
    ts = np.linspace(0.9*Ts_plot[0], 1.1*Ts_plot[-1], 1000)
    ax.plot(1 / ts, np.exp(1 / ts * slope) * np.exp(intercept), '-', color=colors[i])

box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.9, box.height])
ax.set_yscale('log')
ax.set_xlim(4, 22)
ax.set_xlabel("$1/T$")
ax.set_ylabel("$0.5-h$")
ax.legend(loc="center right", bbox_transform=fig.transFigure, bbox_to_anchor=(0.99, 0.55), borderaxespad=0)
fig.tight_layout(pad=0.2, rect=(0,0,0.81,1))
fig.savefig(builddir+"small_T_arrhenius.pdf")

# calculate T, N dependencies using all Ns
intercepts = []
intercept_err = []
slopes = []

for i in range(len(Ns)):
    Ts, midheights = np.genfromtxt(builddir+"midheights_N={}.txt".format(Ns[i]), unpack=True)
    Ts_plot = Ts[Ts < 0.2]
    height_plot = 0.5 - midheights[Ts < 0.2]
    xdata = 1 / Ts_plot[8:]
    ydata = np.log(height_plot[8:])
    slope, intercept, r_value, l_value, std_err = linregress(xdata, ydata)
    print(slope)
    slopes.append(slope)
    intercepts.append(intercept)
    sx2 = sum([(x - np.mean(xdata))**2 for x in xdata])
    intercept_err.append(std_err * np.sqrt(1 / len(xdata) + np.mean(xdata)**2 / sx2))
    ts = np.linspace(0.9*Ts_plot[0], 1.1*Ts_plot[-1], 1000)
    
intercepts = np.array(intercepts)
fig2 = plt.figure()
ax = fig2.add_subplot(111)
ax.plot(np.log(Ns), intercepts, 'r+')
fig2.savefig("intercepts.pdf")


########################################
## midheight dependence on N for T=10000
########################################

print("Plotting asymptotic N-dependence ...")

Ns = np.arange(2,150)
midheight = []

for N in Ns:
    ts, alphas = np.genfromtxt(builddir+"alpha_mixed/alpha_mixed_N={}_T=10000.000.txt".format(N), unpack=True)
    szs = 0.5 * (1 - 2 * alphas)
    av_indices = [int(len(ts) / 2) + x for x in range(-int(len(ts) / (2 * N)), int(len(ts) / (2 * N)))]
    midheight.append(max([szs[a] for a in av_indices]))

midheight = np.array(midheight)
midheight -= 1/6
slope, intercept, r_value, l_value, std_err = linregress(np.log(Ns[10:]), np.log(midheight[10:]))
print(slope, intercept)

fig = plt.figure(figsize=(10,5))
ax = fig.add_subplot(111)
ns = np.linspace(1, 200, 1000)
ax.plot(ns, np.exp(intercept) * ns**slope, 'r-')
ax.plot(Ns, midheight, 'b.')
ax.set_xlim(1, 200)
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlabel("$N$")
ax.set_ylabel(r"$h - \frac{1}{6}$")
fig.tight_layout(pad=0.2)
fig.savefig(builddir+"asymptotic_T_power.pdf")
