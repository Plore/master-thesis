import sys
import matplotlib.pyplot as plt
import numpy as np

N = int(sys.argv[1])
T = sys.argv[2]

ts, alphas = np.genfromtxt("../../build/temperature/0.5/alpha_mixed/alpha_mixed_N={}_T={}.txt".format(N, T), unpack=True)
print("{} data points".format(len(ts)))
szs = 0.5 * (1 - 2 * alphas)
av_indices = [int(len(ts) / 2) + x for x in range(-int(len(ts) / N), int(len(ts) / N))]
midheight  = max([szs[a] for a in av_indices])
plt.xlim(0, np.pi)
plt.axhline(y=midheight, xmin=av_indices[0]/len(ts), xmax=av_indices[-1]/len(ts), color="green")
plt.axvline(3.14159 / 2, color="red")
plt.plot(ts, szs)
plt.savefig("../../build/temperature/0.5/specific_dynamics_N={}_T={}.pdf".format(N, T))
plt.clf()
