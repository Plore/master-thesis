import matplotlib.pyplot as plt
import numpy as np

Ns_odd = [3,7,15,31,63]
Ns_even = [4,8,16,32,64]
colors = plt.get_cmap("jet")(np.linspace(0,1,5))

###########################
# with bath spin (internal)
###########################

# rest of bath in Sb, Sbz eigenstates
print("internal entanglement eigenstates")
fig, ax = plt.subplots(figsize=(10,4.5))
ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_internal_Sb=9\\2_Sbz=9\\2.txt", unpack=True)
ax.plot(ts, ccs, 'b-', label="$S_b^z=9/2$")
ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_internal_Sb=9\\2_Sbz=7\\2.txt", unpack=True)
ax.plot(ts, ccs, 'r-', label="$S_b^z=7/2$")
ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_internal_Sb=9\\2_Sbz=1\\2.txt", unpack=True)
ax.plot(ts, ccs, 'g-', label="$S_b^z=1/2$")
ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_internal_Sb=9\\2_Sbz=-7\\2.txt", unpack=True)
ax.plot(ts, ccs, 'k--', label="$S_b^z=-7/2$")
ax.set_xlabel("$t$")
ax.set_ylabel(r"$\mathcal{C}$")
ax.set_xlim(0, 2 * np.pi)
ax.set_xticks([0, 0.5 * np.pi, np.pi, 1.5 * np.pi, 2 * np.pi])
ax.set_xticklabels(["$0$", r"$\frac{\pi}{2}$", r"$\pi$", r"$\frac{3 \pi}{2}$", r"$2 \pi$"])
ax.legend(bbox_to_anchor=(0, 1.1, 1, 0.1), mode="expand", ncol=4, borderaxespad=0, handlelength=2.3)
fig.tight_layout(pad=0.2, rect = (0,0,1,0.87))
fig.savefig("../../build/entanglement_loss/concurrences_internal_example_Sb=9_2.pdf")

fig, ax = plt.subplots(figsize=(10,4.5))
ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_internal_Sb=14\\2_Sbz=14\\2.txt", unpack=True)
ax.plot(ts, ccs, 'b-', label="$S_b^z=14/2$")
ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_internal_Sb=14\\2_Sbz=10\\2.txt", unpack=True)
ax.plot(ts, ccs, 'r-', label="$S_b^z=10/2$")
ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_internal_Sb=14\\2_Sbz=0\\2.txt", unpack=True)
ax.plot(ts, ccs, 'g-', label="$S_b^z=0$")
ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_internal_Sb=14\\2_Sbz=-10\\2.txt", unpack=True)
ax.plot(ts, ccs, 'k--', label="$S_b^z=-10/2$")
ax.set_xlabel("$t$")
ax.set_ylabel(r"$\mathcal{C}$")
ax.set_xlim(0, 2 * np.pi)
ax.set_xticks([0, 0.5 * np.pi, np.pi, 1.5 * np.pi, 2 * np.pi])
ax.set_xticklabels(["$0$", r"$\frac{\pi}{2}$", r"$\pi$", r"$\frac{3 \pi}{2}$", r"$2 \pi$"])
ax.legend(bbox_to_anchor=(0, 1.1, 1, 0.1), mode="expand", ncol=4, borderaxespad=0, handlelength=2.3)
fig.tight_layout(pad=0.2, rect = (0,0,1,0.87))
fig.savefig("../../build/entanglement_loss/concurrences_internal_example_Sb=14_2.pdf")

# rest of bath in maximally mixed state
print("internal entanglement mixed")
fig, ax = plt.subplots(figsize=(10,4.5))
for N in [2,7,14,29,48]:
    ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_internal_mixed_N={}.txt".format(N), unpack=True)
    ax.plot(ts, ccs, '-', label="$N={}$".format(N))

ax.set_xlabel("$t$")
ax.set_ylabel(r"$\mathcal{C}$")
ax.set_xlim(0, 2 * np.pi)
ax.set_xticks([0, 0.5 * np.pi, np.pi, 1.5 * np.pi, 2 * np.pi])
ax.set_xticklabels(["$0$", r"$\frac{\pi}{2}$", r"$\pi$", r"$\frac{3 \pi}{2}$", r"$2 \pi$"])
ax.legend(loc="center", bbox_to_anchor=(0.5, 0.6))
fig.tight_layout(pad=0.2, rect = (0,0,1,1))
fig.savefig("../../build/entanglement_loss/concurrences_internal_mixed.pdf")

################################
# with spectator spin (external)
################################

# mixed bath

# even N
print("external entanglement mixed")
fig, ax = plt.subplots(figsize=(10,4.5), nrows=1, ncols=1, sharey=True)
for i in range(len(Ns_even)):
    N = Ns_even[i]
    ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_external_mixed_N={}.txt".format(N), unpack=True)
    ax.plot(ts, ccs, '-', color=colors[i], label="$N={}$".format(N))

ax.set_xlabel("$t$")
ax.set_xlim(0, 2 * np.pi)
ax.set_xticks([0, 0.5 * np.pi, np.pi, 1.5 * np.pi, 2 * np.pi])
ax.set_xticklabels(["$0$", r"$\frac{\pi}{2}$", r"$\pi$", r"$\frac{3 \pi}{2}$", r"$2 \pi$"])
ax.set_ylabel(r"$\mathcal{C}$")
ax.legend(loc="upper center", bbox_to_anchor=(0.80, 1.0))
fig.tight_layout(pad=0.2, rect = (0,0,1,1))
fig.savefig("../../build/entanglement_loss/concurrences_external_mixed_even.pdf")

# odd N
fig, ax = plt.subplots(figsize=(10,4.5), nrows=1, ncols=1, sharey=True)
for i in range(len(Ns_odd)):
    N = Ns_odd[i]
    ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_external_mixed_N={}.txt".format(N), unpack=True)
    ax.plot(ts, ccs, '-', color=colors[i], label="$N={}$".format(N))

ax.set_xlabel("$t$")
ax.set_ylabel(r"$\mathcal{C}$")
ax.set_xlim(0, np.pi)
ax.set_xticks([0, 0.25 * np.pi, 0.5 * np.pi, 0.75 * np.pi, np.pi])
ax.set_xticklabels(["$0$", r"$\frac{\pi}{4}$", r"$\frac{\pi}{2}$", r"$\frac{3\pi}{4}$", r"$\pi$"])
ax.set_ylabel(r"$\mathcal{C}$")
ax.legend(loc="upper center")
fig.tight_layout(pad=0.2, rect = (0,0,1,1))
fig.savefig("../../build/entanglement_loss/concurrences_external_mixed_odd.pdf")


# bath eigenstates

print("external entanglement eigenstates")
fig, ax = plt.subplots(figsize=(10,4.5))
ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_external_Sb=9\\2_Sbz=9\\2.txt", unpack=True)
ax.plot(ts, ccs, 'b-', label="$S_b^z=9/2$")
ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_external_Sb=9\\2_Sbz=7\\2.txt", unpack=True)
ax.plot(ts, ccs, 'r-', label="$S_b^z=7/2$")
ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_external_Sb=9\\2_Sbz=1\\2.txt", unpack=True)
ax.plot(ts, ccs, 'g-', label="$S_b^z=1/2$")
ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_external_Sb=9\\2_Sbz=-7\\2.txt", unpack=True)
ax.plot(ts, ccs, 'k--', label="$S_b^z=-7/2$")
ax.set_xlim(0, 2 * np.pi)
ax.set_xlabel("$t$")  
ax.set_xticks([0, 0.5 * np.pi, np.pi, 1.5 * np.pi, 2 * np.pi])
ax.set_xticklabels(["$0$", r"$\frac{\pi}{2}$", r"$\pi$", r"$\frac{3 \pi}{2}$", r"$2 \pi$"])
ax.set_ylim(0,1)
ax.set_ylabel(r"$\mathcal{C}$")
ax.legend(bbox_to_anchor=(0, 1.1, 1, 0.1), mode="expand", ncol=4, borderaxespad=0)
fig.tight_layout(pad=0.2, rect = (0,0,1,0.87))
fig.savefig("../../build/entanglement_loss/concurrences_external_example_Sb=9_2.pdf")

fig, ax = plt.subplots(figsize=(10,4.5))
ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_external_Sb=14\\2_Sbz=14\\2.txt", unpack=True)
ax.plot(ts, ccs, 'b-', label="$S_b^z=14/2$")
ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_external_Sb=14\\2_Sbz=10\\2.txt", unpack=True)
ax.plot(ts, ccs, 'r-', label="$S_b^z=10/2$")
ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_external_Sb=14\\2_Sbz=0\\2.txt", unpack=True)
ax.plot(ts, ccs, 'g-', label="$S_b^z=0$")
ts, ccs = np.genfromtxt("../../build/entanglement_loss/concurrence/concurrence_external_Sb=14\\2_Sbz=-10\\2.txt", unpack=True)
ax.plot(ts, ccs, 'k--', label="$S_b^z=-10/2$")
ax.set_xlim(0, 2 * np.pi)
ax.set_xticks([0, 0.5 * np.pi, np.pi, 1.5 * np.pi, 2 * np.pi])
ax.set_xticklabels(["$0$", r"$\frac{\pi}{2}$", r"$\pi$", r"$\frac{3 \pi}{2}$", r"$2 \pi$"])
ax.set_xlabel("$t$")
ax.set_ylim(0, 1)
ax.set_ylabel(r"$\mathcal{C}$")
ax.legend(bbox_to_anchor=(0, 1.1, 1, 0.1), mode="expand", ncol=4, borderaxespad=0)
fig.tight_layout(pad=0.2, rect = (0,0,1,0.87))
fig.savefig("../../build/entanglement_loss/concurrences_external_example_Sb=14_2.pdf")
