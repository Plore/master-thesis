import numpy as np
import matplotlib.pyplot as plt

ts, sz_mixed, sz_eigenstate = np.genfromtxt("build/examples/S0z_examples_N=21.txt", unpack=True)
fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(10,5), sharey=True)
axes[0].plot(ts, sz_eigenstate, 'b-')
axes[0].set_xlabel("$t$")
axes[0].set_ylabel(r"$\langle S_0^z \rangle$")
axes[0].set_xlim(0,3.14)
axes[0].set_title("$S_b=21/2\:,\:S_b^z=17/2$")
axes[1].plot(ts, sz_mixed, 'b-')
axes[1].set_xlabel("$t$")
axes[1].set_xlim(0,3.14)
axes[1].set_title("$N=21$")
fig.tight_layout()
plt.savefig("build/examples/S0z_example_N=21.pdf")
