\documentclass[10pt]{beamer}
\include{header_physics}

\title{Correlations in the central spin model}
\subtitle{Progress report}
\author{Peter Lorenz}
\date{07.01.2015}

\begin{document}

{
\setbeamertemplate{headline}{}
\setbeamertemplate{footline}{}
  \begin{center}
    \titlepage
  \end{center}
}

\begin{frame}{Model}
  \begin{eqn}
    H = \v{S}_0 \sum_{i=1}^N A_i \v{S}_i + h S_0^z
  \end{eqn}
  Choose $h=0$, $A_i \equiv 2$ (physically unrealistic)
  \begin{eqns}
    H &=& \v{S}^2 - \v{S}_b^2 - \v{S}_0^2\:, \qquad \v{S}_b = \sum_i \v{S}_i \\
    E &=& \begin{cases}
      S_b\:,       & S = S_b + \frac{1}{2} \\
      -S_b - 1\:,  & S = S_b - \frac{1}{2}
    \end{cases}
  \end{eqns}
  \mbox{\rightarrow} periodic dynamics \par
  Examine $\avg{S_0^z(t)} = \Tr\left[\rho_0(t) S_0^z\right]$ and $E(t) = \Tr\left[\rho_0(t) \log \rho_0(t)\right]$, \\ $\rho_0(t) = \Tr_b\left[\rho(t)\right]$
\end{frame}

\begin{frame}{Exact solution \cite{bortz_stolze_2007} for Spin $\frac{1}{2}$}
  Two cases:
  \begin{enumerate}
    \item Bath state $\Ket{S_b, S_b^z}$, entanglement between bath spins
    \item Product state $\Ket{\uparrow \downarrow \downarrow \uparrow \downarrow \ldots}$, polarization $S_b^z = \frac{1}{2} (N - 2N_\t{down})$
  \end{enumerate}
  Strategy: decompose $\Ket{\Downarrow}\Ket{\t{Bath}}$ into eigenstates $\Ket{S, S_b}$\par
  \mbox{\rightarrow} Angular momemtum addition of central spin to bath \cite{schwabl}
  \begin{eqns}
    \Ket{S_b + \frac{1}{2}, S_b^z - \frac{1}{2}} &=& \frac{1}{\sqrt{2 S_b + 1}} \left(\sqrt{S_b + S_b^z} \Ket{\Uparrow}\Ket{S_b, S_b^z - 1}\right. \\
                                          & & + \left.\sqrt{S_b - S_b^z + 1} \Ket{\Downarrow}\Ket{S_b, S_b^z}\right) \\
    \Ket{S_b - \frac{1}{2}, S_b^z - \frac{1}{2}} &=& \frac{1}{\sqrt{2 S_b + 1}} \left(\sqrt{S_b - S_b^z + 1} \Ket{\Uparrow}\Ket{S_b, S_b^z - 1}\right. \\
                                            &  & - \left.\sqrt{S_b + S_b^z} \Ket{\Downarrow}\Ket{S_b, S_b^z}\right)  \\
  \end{eqns}
\end{frame}

\begin{frame}{Case $1$: Entangled bath}
  \begin{eqns}
    \rho_0(t) &=& \alpha(t) \Ket{\Uparrow}\Bra{\Uparrow} + (1 - \alpha(t)) \Ket{\Downarrow}\Bra{\Downarrow} \\
    \alpha(t) &=& \frac{2 (S_b - S_b^z + 1)(S_b + S_b^z)}{(2 S_b + 1)^2} \left(1 - \cos\left( (2 S_b + 1) t\right) \right)
    \label{eqn:spin_half_entangled}
  \end{eqns}
  \mbox{\rightarrow} dynamics characterized by one frequency
\end{frame}

\begin{frame}{Case $2$: Bath product state}
  \fontsize{9}{9}
  Decompose state $\Ket{M, N-M}$ with $N-M$ up-spins and $M$ down-spins into eigenstates $\Ket{S, S^z, S_b}$ by repeated addition of angular momenta, starting with $\Ket{\downarrow}\Ket{0,N-M}$
  \begin{eqn}
    \Ket{\Downarrow} |\underbrace{\downarrow \downarrow \ldots \downarrow}_{M-1},\underbrace{\uparrow \uparrow \ldots \uparrow}_{N-M} \rangle = \sum_{k=0}^{M-1} \frac{1}{\sqrt{N - 2k}} \sum_{j=1}^{M-1 \choose k} c_{jk}^{NM}\\ 
    \qquad \left(\sqrt{M - k}\Ket{\frac{N}{2} - k, \frac{N - 2M}{2}, \frac{N}{2} - \frac{1}{2} - k} \right. \\
    \qquad \left. - \sqrt{N - M - k}\Ket{\frac{N}{2} - 1 - k, \frac{N - 2M}{2}, \frac{N}{2} - \frac{1}{2} - k} \right)
    \label{eq:monster}
  \end{eqn}
  \begin{eqn}
    d_{kNM} := \sum_{j=1}^{\binom{M}{k}} \left(c_{jk}^{NM}\right)^2 = \frac{(M-1)!(N-M)!(N-2k)}{(N-k)!k!}
  \end{eqn}
\end{frame}

\begin{frame}{Case $2$: Bath product state}
  \begin{eqns}
    \rho_0(t) &=& \beta(t) \Ket{\Uparrow}\Bra{\Uparrow} + (1 - \beta(t)) \Ket{\Downarrow}\Bra{\Downarrow}  \\
    \beta(t) &=& 2 \sum_{k=0}^M \frac{d_{kNM}}{(N - 2 k)^2} (M - k) (N - M - k) \left(1 - \cos( (N - 2 k) t)\right)
  \end{eqns}
  \mbox{\rightarrow} dynamics characterized by multiple frequencies
\end{frame}

\begin{frame}{Extension to bath particles of spin $I$}
  \begin{itemize}
    \item Same strategy works for bath eigenstate; Hamiltonian doesn't distinguish between bath compositions
    \item Product state can be constructed in an analogous fashion, but more work required
    \item No simple closed expression for $\Ket{I,m_i} \otimes \Ket{S_b, S_b^z}$
    \item $(2I+1)^M$ different eigenstates after addition of $M$ spins
    \item Choice between different polarization configurations \\ \mbox{\rightarrow} here $m_i = -I$ for all $M$ added spins
  \end{itemize}
\end{frame}

\begin{frame}{Resulting dynamics}
  \vspace{-1.2cm}
  \fontsize{9}{9}
  \begin{eqns}
    \Ket{\Downarrow}|{M},{N-M}\rangle 
    &=& \sum_{k=0}^{2MI} \sum_{j=1}^{G_{M-k}^{M}} c_{jk}^{NM} \frac{1}{\sqrt{2(NI-k) + 1}} \\
    & & \left(\sqrt{2MI - k + 1} \Ket{NI - k + \frac{1}{2}, (N-2M)I - \frac{1}{2}, NI - k}\right. \\
    & & \left. - \sqrt{2NI - 2MI - k}\Ket{NI - k -\frac{1}{2}, (N-2M)I-\frac{1}{2}, NI-k}\right)
  \end{eqns}
  \vspace{0.2cm}
  $d_{kNM} := \sum_{j=1}^{G_{M-k}^{M}}\left(c_{jk}^{NM}\right)^2$ again determined by recursion
  \begin{columns}
    \column{0.5\textwidth}
    $G_{M-k}^M$: multinomial coefficients, for example if $I=\frac{3}{2}$
    \column{0.5\textwidth}
    \begin{eqn}
      \begin{array}{c c c c c c c c c c}
        1 &   &   &   &   &   &   &   &   &\\
        1 & 1 & 1 & 1 &   &   &   &   &   &\\
        1 & 2 & 3 & 4 & 3 & 2 & 1 &   &   &\\
        1 & 3 & 6 & 10& 12& 12& 10& 6 & 4 & 1 
      \end{array}
    \end{eqn}
  \end{columns}
\end{frame}

\begin{frame}{Resulting dynamics}
  \fontsize{9}{9}
  \begin{eqns}
    \rho_0(t) &=& \alpha(t) \Ket{\Uparrow}\Bra{\Uparrow} + (1-\alpha(t)) \Ket{\Downarrow}\Bra{\Downarrow} \\
    \alpha(t) &=& 2 \sum_{k=0}^{2MI} \frac{d_{kNM} (2MI - k + 1)}{(2(NI - k) + 1)^2} (2NI - 2MI - k) \left(1-\cos\left( (2 (NI - k) + 1) t \right)\right)
  \end{eqns}
  \begin{itemize}
    \item Basically the same as before with more frequencies
    \item Extension to more general product states and mixed states by superposition
  \end{itemize}
\end{frame}

\begin{frame}{Comparison with \cite{bortz_stolze_2007}}
  \begin{figure}[h]
      \centering
      \includegraphics[width=0.9\textwidth]{../Code/build/comparisons/comparison_stolze}
  \end{figure}
\end{frame}

\begin{frame}{Maximally mixed state}
  If the bath is in a maximally mixed state ($T \sim \SI{5}{\kelvin}$)
  \begin{eqn}
    \rho(t) = \begin{pmatrix}
      1 & 0 \\
      0 & 0 
    \end{pmatrix}
    \otimes \frac{1}{(2I+1)^N} \mathbb{1}
  \end{eqn}
  the dynamics are calculated by counting degeneracy of $\Ket{S_b, S_b^z}$ with respect to product basis
  \begin{eqn}
    D_{S_b, S_b^z} = G_{NI-S_b^z}^N - G_{NI-S_b^z-1}^N
  \end{eqn}
\end{frame}

\begin{frame}{Comparison with Daniel Meissner's Bachelor thesis \cite{meissner_2014}}
  \vspace{-0.5cm}
  \begin{columns}
    \column{0.5\textwidth}
    \begin{figure}[h]
      \centering
      \includegraphics[width=1.05\textwidth]{../Code/build/comparisons/comparison_meissner}
    \end{figure}
    \column{0.5\textwidth}
    \begin{figure}[h]
      \centering
      \includegraphics[width=1.05\textwidth]{../Code/build/comparisons/comparison_meissner_error}
    \end{figure}
  \end{columns}
\end{frame}

\begin{frame}
  \printbibliography
\end{frame}

\begin{frame}{Backup}
  \fontsize{8}{8}
  \begin{eqns}
    & & |{M},{N-M}\rangle \\
    &=& \sum_{k=0}^{2 (M-1) I} \sum_{j=1}^{G_{M-1-k}^{M-1}} c_{jk}^{NM} \sum_{i=0}^{2I} \Braket{NI-k-i, (N-2M)I | I, -I, (N-1)I-k, (N-2M+1)I}\\
    & & \Ket{NI-k-i, (N-2M)I, (N-1)I-k}
  \end{eqns}
\end{frame}
\end{document}
